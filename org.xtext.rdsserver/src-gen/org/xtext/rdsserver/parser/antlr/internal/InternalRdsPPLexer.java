package org.xtext.rdsserver.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRdsPPLexer extends Lexer {
    public static final int RULE_A=4;
    public static final int RULE_AAANNN=12;
    public static final int RULE_ANN=6;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=16;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_AANNN=9;
    public static final int RULE_AANN=10;
    public static final int RULE_ID=13;
    public static final int RULE_WS=17;
    public static final int RULE_AAANN=8;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_S=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_N=5;
    public static final int RULE_INT=14;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalRdsPPLexer() {;} 
    public InternalRdsPPLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalRdsPPLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalRdsPP.g"; }

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:11:7: ( '=' )
            // InternalRdsPP.g:11:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:12:7: ( '.' )
            // InternalRdsPP.g:12:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:13:7: ( '==' )
            // InternalRdsPP.g:13:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:14:7: ( '+' )
            // InternalRdsPP.g:14:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:15:7: ( '++' )
            // InternalRdsPP.g:15:9: '++'
            {
            match("++"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:16:7: ( '-' )
            // InternalRdsPP.g:16:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:17:7: ( ':' )
            // InternalRdsPP.g:17:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:18:7: ( '&' )
            // InternalRdsPP.g:18:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:19:7: ( ';' )
            // InternalRdsPP.g:19:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "RULE_A"
    public final void mRULE_A() throws RecognitionException {
        try {
            int _type = RULE_A;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1149:8: ( 'A' .. 'Z' )
            // InternalRdsPP.g:1149:10: 'A' .. 'Z'
            {
            matchRange('A','Z'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_A"

    // $ANTLR start "RULE_N"
    public final void mRULE_N() throws RecognitionException {
        try {
            int _type = RULE_N;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1151:8: ( '0' .. '9' )
            // InternalRdsPP.g:1151:10: '0' .. '9'
            {
            matchRange('0','9'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_N"

    // $ANTLR start "RULE_S"
    public final void mRULE_S() throws RecognitionException {
        try {
            int _type = RULE_S;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1153:8: ( ' ' )
            // InternalRdsPP.g:1153:10: ' '
            {
            match(' '); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_S"

    // $ANTLR start "RULE_ANN"
    public final void mRULE_ANN() throws RecognitionException {
        try {
            int _type = RULE_ANN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1155:10: ( RULE_A ( RULE_S )? RULE_N ( RULE_N )? )
            // InternalRdsPP.g:1155:12: RULE_A ( RULE_S )? RULE_N ( RULE_N )?
            {
            mRULE_A(); 
            // InternalRdsPP.g:1155:19: ( RULE_S )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==' ') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalRdsPP.g:1155:19: RULE_S
                    {
                    mRULE_S(); 

                    }
                    break;

            }

            mRULE_N(); 
            // InternalRdsPP.g:1155:34: ( RULE_N )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalRdsPP.g:1155:34: RULE_N
                    {
                    mRULE_N(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANN"

    // $ANTLR start "RULE_AAANN"
    public final void mRULE_AAANN() throws RecognitionException {
        try {
            int _type = RULE_AAANN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1157:12: ( RULE_A RULE_A RULE_A ( RULE_S )? RULE_N RULE_N )
            // InternalRdsPP.g:1157:14: RULE_A RULE_A RULE_A ( RULE_S )? RULE_N RULE_N
            {
            mRULE_A(); 
            mRULE_A(); 
            mRULE_A(); 
            // InternalRdsPP.g:1157:35: ( RULE_S )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==' ') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRdsPP.g:1157:35: RULE_S
                    {
                    mRULE_S(); 

                    }
                    break;

            }

            mRULE_N(); 
            mRULE_N(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AAANN"

    // $ANTLR start "RULE_AANNN"
    public final void mRULE_AANNN() throws RecognitionException {
        try {
            int _type = RULE_AANNN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1159:12: ( RULE_A RULE_A ( RULE_S )? RULE_N RULE_N RULE_N )
            // InternalRdsPP.g:1159:14: RULE_A RULE_A ( RULE_S )? RULE_N RULE_N RULE_N
            {
            mRULE_A(); 
            mRULE_A(); 
            // InternalRdsPP.g:1159:28: ( RULE_S )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==' ') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRdsPP.g:1159:28: RULE_S
                    {
                    mRULE_S(); 

                    }
                    break;

            }

            mRULE_N(); 
            mRULE_N(); 
            mRULE_N(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AANNN"

    // $ANTLR start "RULE_AANN"
    public final void mRULE_AANN() throws RecognitionException {
        try {
            int _type = RULE_AANN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1161:11: ( RULE_A RULE_A ( RULE_S )? RULE_N RULE_N )
            // InternalRdsPP.g:1161:13: RULE_A RULE_A ( RULE_S )? RULE_N RULE_N
            {
            mRULE_A(); 
            mRULE_A(); 
            // InternalRdsPP.g:1161:27: ( RULE_S )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==' ') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalRdsPP.g:1161:27: RULE_S
                    {
                    mRULE_S(); 

                    }
                    break;

            }

            mRULE_N(); 
            mRULE_N(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AANN"

    // $ANTLR start "RULE_AAANNN"
    public final void mRULE_AAANNN() throws RecognitionException {
        try {
            int _type = RULE_AAANNN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1163:13: ( RULE_A RULE_A RULE_A RULE_N RULE_N RULE_N )
            // InternalRdsPP.g:1163:15: RULE_A RULE_A RULE_A RULE_N RULE_N RULE_N
            {
            mRULE_A(); 
            mRULE_A(); 
            mRULE_A(); 
            mRULE_N(); 
            mRULE_N(); 
            mRULE_N(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AAANNN"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1165:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalRdsPP.g:1165:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalRdsPP.g:1165:11: ( '^' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='^') ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalRdsPP.g:1165:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalRdsPP.g:1165:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')||(LA7_0>='A' && LA7_0<='Z')||LA7_0=='_'||(LA7_0>='a' && LA7_0<='z')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalRdsPP.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1167:10: ( ( '0' .. '9' )+ )
            // InternalRdsPP.g:1167:12: ( '0' .. '9' )+
            {
            // InternalRdsPP.g:1167:12: ( '0' .. '9' )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalRdsPP.g:1167:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1169:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalRdsPP.g:1169:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalRdsPP.g:1169:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\"') ) {
                alt11=1;
            }
            else if ( (LA11_0=='\'') ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalRdsPP.g:1169:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalRdsPP.g:1169:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='!')||(LA9_0>='#' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalRdsPP.g:1169:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalRdsPP.g:1169:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:1169:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalRdsPP.g:1169:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop10:
                    do {
                        int alt10=3;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0=='\\') ) {
                            alt10=1;
                        }
                        else if ( ((LA10_0>='\u0000' && LA10_0<='&')||(LA10_0>='(' && LA10_0<='[')||(LA10_0>=']' && LA10_0<='\uFFFF')) ) {
                            alt10=2;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalRdsPP.g:1169:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalRdsPP.g:1169:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1171:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalRdsPP.g:1171:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalRdsPP.g:1171:24: ( options {greedy=false; } : . )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0=='*') ) {
                    int LA12_1 = input.LA(2);

                    if ( (LA12_1=='/') ) {
                        alt12=2;
                    }
                    else if ( ((LA12_1>='\u0000' && LA12_1<='.')||(LA12_1>='0' && LA12_1<='\uFFFF')) ) {
                        alt12=1;
                    }


                }
                else if ( ((LA12_0>='\u0000' && LA12_0<=')')||(LA12_0>='+' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalRdsPP.g:1171:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1173:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalRdsPP.g:1173:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalRdsPP.g:1173:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='\u0000' && LA13_0<='\t')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='\uFFFF')) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalRdsPP.g:1173:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            // InternalRdsPP.g:1173:40: ( ( '\\r' )? '\\n' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='\n'||LA15_0=='\r') ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRdsPP.g:1173:41: ( '\\r' )? '\\n'
                    {
                    // InternalRdsPP.g:1173:41: ( '\\r' )?
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0=='\r') ) {
                        alt14=1;
                    }
                    switch (alt14) {
                        case 1 :
                            // InternalRdsPP.g:1173:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1175:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalRdsPP.g:1175:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalRdsPP.g:1175:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>='\t' && LA16_0<='\n')||LA16_0=='\r'||LA16_0==' ') ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalRdsPP.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalRdsPP.g:1177:16: ( . )
            // InternalRdsPP.g:1177:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalRdsPP.g:1:8: ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | RULE_A | RULE_N | RULE_S | RULE_ANN | RULE_AAANN | RULE_AANNN | RULE_AANN | RULE_AAANNN | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt17=24;
        alt17 = dfa17.predict(input);
        switch (alt17) {
            case 1 :
                // InternalRdsPP.g:1:10: T__19
                {
                mT__19(); 

                }
                break;
            case 2 :
                // InternalRdsPP.g:1:16: T__20
                {
                mT__20(); 

                }
                break;
            case 3 :
                // InternalRdsPP.g:1:22: T__21
                {
                mT__21(); 

                }
                break;
            case 4 :
                // InternalRdsPP.g:1:28: T__22
                {
                mT__22(); 

                }
                break;
            case 5 :
                // InternalRdsPP.g:1:34: T__23
                {
                mT__23(); 

                }
                break;
            case 6 :
                // InternalRdsPP.g:1:40: T__24
                {
                mT__24(); 

                }
                break;
            case 7 :
                // InternalRdsPP.g:1:46: T__25
                {
                mT__25(); 

                }
                break;
            case 8 :
                // InternalRdsPP.g:1:52: T__26
                {
                mT__26(); 

                }
                break;
            case 9 :
                // InternalRdsPP.g:1:58: T__27
                {
                mT__27(); 

                }
                break;
            case 10 :
                // InternalRdsPP.g:1:64: RULE_A
                {
                mRULE_A(); 

                }
                break;
            case 11 :
                // InternalRdsPP.g:1:71: RULE_N
                {
                mRULE_N(); 

                }
                break;
            case 12 :
                // InternalRdsPP.g:1:78: RULE_S
                {
                mRULE_S(); 

                }
                break;
            case 13 :
                // InternalRdsPP.g:1:85: RULE_ANN
                {
                mRULE_ANN(); 

                }
                break;
            case 14 :
                // InternalRdsPP.g:1:94: RULE_AAANN
                {
                mRULE_AAANN(); 

                }
                break;
            case 15 :
                // InternalRdsPP.g:1:105: RULE_AANNN
                {
                mRULE_AANNN(); 

                }
                break;
            case 16 :
                // InternalRdsPP.g:1:116: RULE_AANN
                {
                mRULE_AANN(); 

                }
                break;
            case 17 :
                // InternalRdsPP.g:1:126: RULE_AAANNN
                {
                mRULE_AAANNN(); 

                }
                break;
            case 18 :
                // InternalRdsPP.g:1:138: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 19 :
                // InternalRdsPP.g:1:146: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 20 :
                // InternalRdsPP.g:1:155: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 21 :
                // InternalRdsPP.g:1:167: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 22 :
                // InternalRdsPP.g:1:183: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 23 :
                // InternalRdsPP.g:1:199: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 24 :
                // InternalRdsPP.g:1:207: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA17 dfa17 = new DFA17(this);
    static final String DFA17_eotS =
        "\1\uffff\1\23\1\uffff\1\26\4\uffff\1\33\1\40\1\42\1\21\1\uffff\3\21\14\uffff\1\37\1\36\11\uffff\1\37\1\uffff\1\37\1\36\1\uffff\1\37\1\uffff\1\61\1\53\1\61\1\uffff\1\64\1\65\2\uffff";
    static final String DFA17_eofS =
        "\66\uffff";
    static final String DFA17_minS =
        "\1\0\1\75\1\uffff\1\53\4\uffff\1\40\1\60\1\11\1\101\1\uffff\2\0\1\52\14\uffff\1\40\1\60\11\uffff\1\40\3\60\1\uffff\5\60\1\uffff\2\60\2\uffff";
    static final String DFA17_maxS =
        "\1\uffff\1\75\1\uffff\1\53\4\uffff\1\172\1\71\1\40\1\172\1\uffff\2\uffff\1\57\14\uffff\1\132\1\172\11\uffff\3\71\1\172\1\uffff\2\71\2\172\1\71\1\uffff\2\172\2\uffff";
    static final String DFA17_acceptS =
        "\2\uffff\1\2\1\uffff\1\6\1\7\1\10\1\11\4\uffff\1\22\3\uffff\1\27\1\30\1\3\1\1\1\2\1\5\1\4\1\6\1\7\1\10\1\11\1\12\2\uffff\1\15\1\22\1\13\1\23\1\14\1\27\1\24\1\25\1\26\4\uffff\1\16\5\uffff\1\20\2\uffff\1\17\1\21";
    static final String DFA17_specialS =
        "\1\2\14\uffff\1\0\1\1\47\uffff}>";
    static final String[] DFA17_transitionS = {
            "\11\21\2\20\2\21\1\20\22\21\1\12\1\21\1\15\3\21\1\6\1\16\3\21\1\3\1\21\1\4\1\2\1\17\12\11\1\5\1\7\1\21\1\1\3\21\32\10\3\21\1\13\1\14\1\21\32\14\uff85\21",
            "\1\22",
            "",
            "\1\25",
            "",
            "",
            "",
            "",
            "\1\36\17\uffff\12\35\7\uffff\32\34\4\uffff\1\37\1\uffff\32\37",
            "\12\41",
            "\2\43\2\uffff\1\43\22\uffff\1\43",
            "\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\0\44",
            "\0\44",
            "\1\45\4\uffff\1\46",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\50\17\uffff\12\51\7\uffff\32\47",
            "\12\52\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\53\17\uffff\12\54",
            "\12\55",
            "\12\56",
            "\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            "\12\57",
            "\12\60",
            "\12\62\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\12\63\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\12\64",
            "",
            "\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "\12\37\7\uffff\32\37\4\uffff\1\37\1\uffff\32\37",
            "",
            ""
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | RULE_A | RULE_N | RULE_S | RULE_ANN | RULE_AAANN | RULE_AANNN | RULE_AANN | RULE_AAANNN | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA17_13 = input.LA(1);

                        s = -1;
                        if ( ((LA17_13>='\u0000' && LA17_13<='\uFFFF')) ) {s = 36;}

                        else s = 17;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA17_14 = input.LA(1);

                        s = -1;
                        if ( ((LA17_14>='\u0000' && LA17_14<='\uFFFF')) ) {s = 36;}

                        else s = 17;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA17_0 = input.LA(1);

                        s = -1;
                        if ( (LA17_0=='=') ) {s = 1;}

                        else if ( (LA17_0=='.') ) {s = 2;}

                        else if ( (LA17_0=='+') ) {s = 3;}

                        else if ( (LA17_0=='-') ) {s = 4;}

                        else if ( (LA17_0==':') ) {s = 5;}

                        else if ( (LA17_0=='&') ) {s = 6;}

                        else if ( (LA17_0==';') ) {s = 7;}

                        else if ( ((LA17_0>='A' && LA17_0<='Z')) ) {s = 8;}

                        else if ( ((LA17_0>='0' && LA17_0<='9')) ) {s = 9;}

                        else if ( (LA17_0==' ') ) {s = 10;}

                        else if ( (LA17_0=='^') ) {s = 11;}

                        else if ( (LA17_0=='_'||(LA17_0>='a' && LA17_0<='z')) ) {s = 12;}

                        else if ( (LA17_0=='\"') ) {s = 13;}

                        else if ( (LA17_0=='\'') ) {s = 14;}

                        else if ( (LA17_0=='/') ) {s = 15;}

                        else if ( ((LA17_0>='\t' && LA17_0<='\n')||LA17_0=='\r') ) {s = 16;}

                        else if ( ((LA17_0>='\u0000' && LA17_0<='\b')||(LA17_0>='\u000B' && LA17_0<='\f')||(LA17_0>='\u000E' && LA17_0<='\u001F')||LA17_0=='!'||(LA17_0>='#' && LA17_0<='%')||(LA17_0>='(' && LA17_0<='*')||LA17_0==','||LA17_0=='<'||(LA17_0>='>' && LA17_0<='@')||(LA17_0>='[' && LA17_0<=']')||LA17_0=='`'||(LA17_0>='{' && LA17_0<='\uFFFF')) ) {s = 17;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 17, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}