package org.xtext.rdsserver.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.rdsserver.services.RdsPPGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRdsPPParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_A", "RULE_N", "RULE_ANN", "RULE_S", "RULE_AAANN", "RULE_AANNN", "RULE_AANN", "RULE_STRING", "RULE_AAANNN", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'='", "'.'", "'=='", "'+'", "'++'", "'-'", "':'", "'&'", "';'"
    };
    public static final int RULE_A=4;
    public static final int RULE_AAANNN=12;
    public static final int RULE_ANN=6;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=16;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_AANNN=9;
    public static final int RULE_AANN=10;
    public static final int RULE_ID=13;
    public static final int RULE_WS=17;
    public static final int RULE_AAANN=8;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_S=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_N=5;
    public static final int RULE_INT=14;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRdsPPParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRdsPPParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRdsPPParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRdsPP.g"; }



     	private RdsPPGrammarAccess grammarAccess;

        public InternalRdsPPParser(TokenStream input, RdsPPGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";
       	}

       	@Override
       	protected RdsPPGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDomainmodel"
    // InternalRdsPP.g:64:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // InternalRdsPP.g:64:52: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // InternalRdsPP.g:65:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalRdsPP.g:71:1: ruleDomainmodel returns [EObject current=null] : ( (lv_Kennzeichnung_0_0= ruleKennzeichnung ) ) ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_Kennzeichnung_0_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:77:2: ( ( (lv_Kennzeichnung_0_0= ruleKennzeichnung ) ) )
            // InternalRdsPP.g:78:2: ( (lv_Kennzeichnung_0_0= ruleKennzeichnung ) )
            {
            // InternalRdsPP.g:78:2: ( (lv_Kennzeichnung_0_0= ruleKennzeichnung ) )
            // InternalRdsPP.g:79:3: (lv_Kennzeichnung_0_0= ruleKennzeichnung )
            {
            // InternalRdsPP.g:79:3: (lv_Kennzeichnung_0_0= ruleKennzeichnung )
            // InternalRdsPP.g:80:4: lv_Kennzeichnung_0_0= ruleKennzeichnung
            {

            				newCompositeNode(grammarAccess.getDomainmodelAccess().getKennzeichnungKennzeichnungParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_Kennzeichnung_0_0=ruleKennzeichnung();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            				}
            				set(
            					current,
            					"Kennzeichnung",
            					lv_Kennzeichnung_0_0,
            					"org.xtext.rdsserver.RdsPP.Kennzeichnung");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleNA"
    // InternalRdsPP.g:100:1: entryRuleNA returns [String current=null] : iv_ruleNA= ruleNA EOF ;
    public final String entryRuleNA() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNA = null;


        try {
            // InternalRdsPP.g:100:42: (iv_ruleNA= ruleNA EOF )
            // InternalRdsPP.g:101:2: iv_ruleNA= ruleNA EOF
            {
             newCompositeNode(grammarAccess.getNARule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNA=ruleNA();

            state._fsp--;

             current =iv_ruleNA.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNA"


    // $ANTLR start "ruleNA"
    // InternalRdsPP.g:107:1: ruleNA returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_A_0= RULE_A | this_N_1= RULE_N )* ;
    public final AntlrDatatypeRuleToken ruleNA() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_A_0=null;
        Token this_N_1=null;


        	enterRule();

        try {
            // InternalRdsPP.g:113:2: ( (this_A_0= RULE_A | this_N_1= RULE_N )* )
            // InternalRdsPP.g:114:2: (this_A_0= RULE_A | this_N_1= RULE_N )*
            {
            // InternalRdsPP.g:114:2: (this_A_0= RULE_A | this_N_1= RULE_N )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_A) ) {
                    alt1=1;
                }
                else if ( (LA1_0==RULE_N) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalRdsPP.g:115:3: this_A_0= RULE_A
            	    {
            	    this_A_0=(Token)match(input,RULE_A,FOLLOW_3); 

            	    			current.merge(this_A_0);
            	    		

            	    			newLeafNode(this_A_0, grammarAccess.getNAAccess().getATerminalRuleCall_0());
            	    		

            	    }
            	    break;
            	case 2 :
            	    // InternalRdsPP.g:123:3: this_N_1= RULE_N
            	    {
            	    this_N_1=(Token)match(input,RULE_N,FOLLOW_3); 

            	    			current.merge(this_N_1);
            	    		

            	    			newLeafNode(this_N_1, grammarAccess.getNAAccess().getNTerminalRuleCall_1());
            	    		

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNA"


    // $ANTLR start "entryRuleKennzeichnung"
    // InternalRdsPP.g:134:1: entryRuleKennzeichnung returns [EObject current=null] : iv_ruleKennzeichnung= ruleKennzeichnung EOF ;
    public final EObject entryRuleKennzeichnung() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKennzeichnung = null;


        try {
            // InternalRdsPP.g:134:54: (iv_ruleKennzeichnung= ruleKennzeichnung EOF )
            // InternalRdsPP.g:135:2: iv_ruleKennzeichnung= ruleKennzeichnung EOF
            {
             newCompositeNode(grammarAccess.getKennzeichnungRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKennzeichnung=ruleKennzeichnung();

            state._fsp--;

             current =iv_ruleKennzeichnung; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKennzeichnung"


    // $ANTLR start "ruleKennzeichnung"
    // InternalRdsPP.g:141:1: ruleKennzeichnung returns [EObject current=null] : ( ( (lv_Funktion_0_0= ruleBFunktion ) ) | ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) ) | ( (lv_Produkt_2_0= ruleBProdukt ) ) | ( (lv_Einbauort_3_0= ruleBEinbauort ) ) | ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) ) ) ;
    public final EObject ruleKennzeichnung() throws RecognitionException {
        EObject current = null;

        EObject lv_Funktion_0_0 = null;

        EObject lv_FunkZuordnung_1_0 = null;

        EObject lv_Produkt_2_0 = null;

        EObject lv_Einbauort_3_0 = null;

        EObject lv_Aufstellungsort_4_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:147:2: ( ( ( (lv_Funktion_0_0= ruleBFunktion ) ) | ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) ) | ( (lv_Produkt_2_0= ruleBProdukt ) ) | ( (lv_Einbauort_3_0= ruleBEinbauort ) ) | ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) ) ) )
            // InternalRdsPP.g:148:2: ( ( (lv_Funktion_0_0= ruleBFunktion ) ) | ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) ) | ( (lv_Produkt_2_0= ruleBProdukt ) ) | ( (lv_Einbauort_3_0= ruleBEinbauort ) ) | ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) ) )
            {
            // InternalRdsPP.g:148:2: ( ( (lv_Funktion_0_0= ruleBFunktion ) ) | ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) ) | ( (lv_Produkt_2_0= ruleBProdukt ) ) | ( (lv_Einbauort_3_0= ruleBEinbauort ) ) | ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) ) )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt2=1;
                }
                break;
            case 21:
                {
                alt2=2;
                }
                break;
            case 24:
                {
                alt2=3;
                }
                break;
            case 22:
                {
                alt2=4;
                }
                break;
            case 23:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalRdsPP.g:149:3: ( (lv_Funktion_0_0= ruleBFunktion ) )
                    {
                    // InternalRdsPP.g:149:3: ( (lv_Funktion_0_0= ruleBFunktion ) )
                    // InternalRdsPP.g:150:4: (lv_Funktion_0_0= ruleBFunktion )
                    {
                    // InternalRdsPP.g:150:4: (lv_Funktion_0_0= ruleBFunktion )
                    // InternalRdsPP.g:151:5: lv_Funktion_0_0= ruleBFunktion
                    {

                    					newCompositeNode(grammarAccess.getKennzeichnungAccess().getFunktionBFunktionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_Funktion_0_0=ruleBFunktion();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getKennzeichnungRule());
                    					}
                    					set(
                    						current,
                    						"Funktion",
                    						lv_Funktion_0_0,
                    						"org.xtext.rdsserver.RdsPP.BFunktion");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:169:3: ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) )
                    {
                    // InternalRdsPP.g:169:3: ( (lv_FunkZuordnung_1_0= ruleBFunkZuordnung ) )
                    // InternalRdsPP.g:170:4: (lv_FunkZuordnung_1_0= ruleBFunkZuordnung )
                    {
                    // InternalRdsPP.g:170:4: (lv_FunkZuordnung_1_0= ruleBFunkZuordnung )
                    // InternalRdsPP.g:171:5: lv_FunkZuordnung_1_0= ruleBFunkZuordnung
                    {

                    					newCompositeNode(grammarAccess.getKennzeichnungAccess().getFunkZuordnungBFunkZuordnungParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_FunkZuordnung_1_0=ruleBFunkZuordnung();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getKennzeichnungRule());
                    					}
                    					set(
                    						current,
                    						"FunkZuordnung",
                    						lv_FunkZuordnung_1_0,
                    						"org.xtext.rdsserver.RdsPP.BFunkZuordnung");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalRdsPP.g:189:3: ( (lv_Produkt_2_0= ruleBProdukt ) )
                    {
                    // InternalRdsPP.g:189:3: ( (lv_Produkt_2_0= ruleBProdukt ) )
                    // InternalRdsPP.g:190:4: (lv_Produkt_2_0= ruleBProdukt )
                    {
                    // InternalRdsPP.g:190:4: (lv_Produkt_2_0= ruleBProdukt )
                    // InternalRdsPP.g:191:5: lv_Produkt_2_0= ruleBProdukt
                    {

                    					newCompositeNode(grammarAccess.getKennzeichnungAccess().getProduktBProduktParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_Produkt_2_0=ruleBProdukt();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getKennzeichnungRule());
                    					}
                    					set(
                    						current,
                    						"Produkt",
                    						lv_Produkt_2_0,
                    						"org.xtext.rdsserver.RdsPP.BProdukt");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalRdsPP.g:209:3: ( (lv_Einbauort_3_0= ruleBEinbauort ) )
                    {
                    // InternalRdsPP.g:209:3: ( (lv_Einbauort_3_0= ruleBEinbauort ) )
                    // InternalRdsPP.g:210:4: (lv_Einbauort_3_0= ruleBEinbauort )
                    {
                    // InternalRdsPP.g:210:4: (lv_Einbauort_3_0= ruleBEinbauort )
                    // InternalRdsPP.g:211:5: lv_Einbauort_3_0= ruleBEinbauort
                    {

                    					newCompositeNode(grammarAccess.getKennzeichnungAccess().getEinbauortBEinbauortParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_Einbauort_3_0=ruleBEinbauort();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getKennzeichnungRule());
                    					}
                    					set(
                    						current,
                    						"Einbauort",
                    						lv_Einbauort_3_0,
                    						"org.xtext.rdsserver.RdsPP.BEinbauort");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalRdsPP.g:229:3: ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) )
                    {
                    // InternalRdsPP.g:229:3: ( (lv_Aufstellungsort_4_0= ruleBAufstellungsort ) )
                    // InternalRdsPP.g:230:4: (lv_Aufstellungsort_4_0= ruleBAufstellungsort )
                    {
                    // InternalRdsPP.g:230:4: (lv_Aufstellungsort_4_0= ruleBAufstellungsort )
                    // InternalRdsPP.g:231:5: lv_Aufstellungsort_4_0= ruleBAufstellungsort
                    {

                    					newCompositeNode(grammarAccess.getKennzeichnungAccess().getAufstellungsortBAufstellungsortParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_Aufstellungsort_4_0=ruleBAufstellungsort();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getKennzeichnungRule());
                    					}
                    					set(
                    						current,
                    						"Aufstellungsort",
                    						lv_Aufstellungsort_4_0,
                    						"org.xtext.rdsserver.RdsPP.BAufstellungsort");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKennzeichnung"


    // $ANTLR start "entryRuleBFunktion"
    // InternalRdsPP.g:252:1: entryRuleBFunktion returns [EObject current=null] : iv_ruleBFunktion= ruleBFunktion EOF ;
    public final EObject entryRuleBFunktion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBFunktion = null;


        try {
            // InternalRdsPP.g:252:50: (iv_ruleBFunktion= ruleBFunktion EOF )
            // InternalRdsPP.g:253:2: iv_ruleBFunktion= ruleBFunktion EOF
            {
             newCompositeNode(grammarAccess.getBFunktionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBFunktion=ruleBFunktion();

            state._fsp--;

             current =iv_ruleBFunktion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBFunktion"


    // $ANTLR start "ruleBFunktion"
    // InternalRdsPP.g:259:1: ruleBFunktion returns [EObject current=null] : (otherlv_0= '=' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) (this_S_4= RULE_S )? ( (lv_Abschnitt2_5_0= RULE_AANNN ) ) (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )? ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )? ) ;
    public final EObject ruleBFunktion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Hauptsystem_1_0=null;
        Token this_S_2=null;
        Token lv_Abschnitt1_3_0=null;
        Token this_S_4=null;
        Token lv_Abschnitt2_5_0=null;
        Token otherlv_6=null;
        Token lv_Leitfunktion_7_0=null;
        Token this_S_8=null;
        Token this_S_10=null;
        Token this_S_12=null;
        Token this_S_14=null;
        Token this_S_16=null;
        EObject lv_Produkt_9_0 = null;

        EObject lv_Anschluss_11_0 = null;

        EObject lv_Dokumentation_13_0 = null;

        EObject lv_Signal_15_0 = null;

        EObject lv_Dokumentation_17_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:265:2: ( (otherlv_0= '=' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) (this_S_4= RULE_S )? ( (lv_Abschnitt2_5_0= RULE_AANNN ) ) (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )? ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )? ) )
            // InternalRdsPP.g:266:2: (otherlv_0= '=' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) (this_S_4= RULE_S )? ( (lv_Abschnitt2_5_0= RULE_AANNN ) ) (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )? ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )? )
            {
            // InternalRdsPP.g:266:2: (otherlv_0= '=' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) (this_S_4= RULE_S )? ( (lv_Abschnitt2_5_0= RULE_AANNN ) ) (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )? ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )? )
            // InternalRdsPP.g:267:3: otherlv_0= '=' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) (this_S_4= RULE_S )? ( (lv_Abschnitt2_5_0= RULE_AANNN ) ) (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )? ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )?
            {
            otherlv_0=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getBFunktionAccess().getEqualsSignKeyword_0());
            		
            // InternalRdsPP.g:271:3: ( (lv_Hauptsystem_1_0= RULE_ANN ) )
            // InternalRdsPP.g:272:4: (lv_Hauptsystem_1_0= RULE_ANN )
            {
            // InternalRdsPP.g:272:4: (lv_Hauptsystem_1_0= RULE_ANN )
            // InternalRdsPP.g:273:5: lv_Hauptsystem_1_0= RULE_ANN
            {
            lv_Hauptsystem_1_0=(Token)match(input,RULE_ANN,FOLLOW_5); 

            					newLeafNode(lv_Hauptsystem_1_0, grammarAccess.getBFunktionAccess().getHauptsystemANNTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunktionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Hauptsystem",
            						lv_Hauptsystem_1_0,
            						"org.xtext.rdsserver.RdsPP.ANN");
            				

            }


            }

            // InternalRdsPP.g:289:3: (this_S_2= RULE_S )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_S) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRdsPP.g:290:4: this_S_2= RULE_S
                    {
                    this_S_2=(Token)match(input,RULE_S,FOLLOW_6); 

                    				newLeafNode(this_S_2, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_2());
                    			

                    }
                    break;

            }

            // InternalRdsPP.g:295:3: ( (lv_Abschnitt1_3_0= RULE_AAANN ) )
            // InternalRdsPP.g:296:4: (lv_Abschnitt1_3_0= RULE_AAANN )
            {
            // InternalRdsPP.g:296:4: (lv_Abschnitt1_3_0= RULE_AAANN )
            // InternalRdsPP.g:297:5: lv_Abschnitt1_3_0= RULE_AAANN
            {
            lv_Abschnitt1_3_0=(Token)match(input,RULE_AAANN,FOLLOW_7); 

            					newLeafNode(lv_Abschnitt1_3_0, grammarAccess.getBFunktionAccess().getAbschnitt1AAANNTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunktionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Abschnitt1",
            						lv_Abschnitt1_3_0,
            						"org.xtext.rdsserver.RdsPP.AAANN");
            				

            }


            }

            // InternalRdsPP.g:313:3: (this_S_4= RULE_S )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_S) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRdsPP.g:314:4: this_S_4= RULE_S
                    {
                    this_S_4=(Token)match(input,RULE_S,FOLLOW_8); 

                    				newLeafNode(this_S_4, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_4());
                    			

                    }
                    break;

            }

            // InternalRdsPP.g:319:3: ( (lv_Abschnitt2_5_0= RULE_AANNN ) )
            // InternalRdsPP.g:320:4: (lv_Abschnitt2_5_0= RULE_AANNN )
            {
            // InternalRdsPP.g:320:4: (lv_Abschnitt2_5_0= RULE_AANNN )
            // InternalRdsPP.g:321:5: lv_Abschnitt2_5_0= RULE_AANNN
            {
            lv_Abschnitt2_5_0=(Token)match(input,RULE_AANNN,FOLLOW_9); 

            					newLeafNode(lv_Abschnitt2_5_0, grammarAccess.getBFunktionAccess().getAbschnitt2AANNNTerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunktionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Abschnitt2",
            						lv_Abschnitt2_5_0,
            						"org.xtext.rdsserver.RdsPP.AANNN");
            				

            }


            }

            // InternalRdsPP.g:337:3: (otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalRdsPP.g:338:4: otherlv_6= '.' ( (lv_Leitfunktion_7_0= RULE_AANN ) )
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_10); 

                    				newLeafNode(otherlv_6, grammarAccess.getBFunktionAccess().getFullStopKeyword_6_0());
                    			
                    // InternalRdsPP.g:342:4: ( (lv_Leitfunktion_7_0= RULE_AANN ) )
                    // InternalRdsPP.g:343:5: (lv_Leitfunktion_7_0= RULE_AANN )
                    {
                    // InternalRdsPP.g:343:5: (lv_Leitfunktion_7_0= RULE_AANN )
                    // InternalRdsPP.g:344:6: lv_Leitfunktion_7_0= RULE_AANN
                    {
                    lv_Leitfunktion_7_0=(Token)match(input,RULE_AANN,FOLLOW_11); 

                    						newLeafNode(lv_Leitfunktion_7_0, grammarAccess.getBFunktionAccess().getLeitfunktionAANNTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBFunktionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Leitfunktion",
                    							lv_Leitfunktion_7_0,
                    							"org.xtext.rdsserver.RdsPP.AANN");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRdsPP.g:361:3: ( (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? ) | (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) ) | (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) ) )?
            int alt7=4;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_S) ) {
                switch ( input.LA(2) ) {
                    case 26:
                        {
                        alt7=3;
                        }
                        break;
                    case 27:
                        {
                        alt7=2;
                        }
                        break;
                    case 24:
                        {
                        alt7=1;
                        }
                        break;
                }

            }
            switch (alt7) {
                case 1 :
                    // InternalRdsPP.g:362:4: (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? )
                    {
                    // InternalRdsPP.g:362:4: (this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )? )
                    // InternalRdsPP.g:363:5: this_S_8= RULE_S ( (lv_Produkt_9_0= ruleBProdukt ) ) ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )?
                    {
                    this_S_8=(Token)match(input,RULE_S,FOLLOW_12); 

                    					newLeafNode(this_S_8, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_0());
                    				
                    // InternalRdsPP.g:367:5: ( (lv_Produkt_9_0= ruleBProdukt ) )
                    // InternalRdsPP.g:368:6: (lv_Produkt_9_0= ruleBProdukt )
                    {
                    // InternalRdsPP.g:368:6: (lv_Produkt_9_0= ruleBProdukt )
                    // InternalRdsPP.g:369:7: lv_Produkt_9_0= ruleBProdukt
                    {

                    							newCompositeNode(grammarAccess.getBFunktionAccess().getProduktBProduktParserRuleCall_7_0_1_0());
                    						
                    pushFollow(FOLLOW_11);
                    lv_Produkt_9_0=ruleBProdukt();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getBFunktionRule());
                    							}
                    							set(
                    								current,
                    								"Produkt",
                    								lv_Produkt_9_0,
                    								"org.xtext.rdsserver.RdsPP.BProdukt");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalRdsPP.g:386:5: ( (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) ) | (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) ) )?
                    int alt6=3;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==RULE_S) ) {
                        int LA6_1 = input.LA(2);

                        if ( (LA6_1==26) ) {
                            alt6=2;
                        }
                        else if ( (LA6_1==25) ) {
                            alt6=1;
                        }
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalRdsPP.g:387:6: (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) )
                            {
                            // InternalRdsPP.g:387:6: (this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) ) )
                            // InternalRdsPP.g:388:7: this_S_10= RULE_S ( (lv_Anschluss_11_0= ruleBAnschluss ) )
                            {
                            this_S_10=(Token)match(input,RULE_S,FOLLOW_13); 

                            							newLeafNode(this_S_10, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_0_0());
                            						
                            // InternalRdsPP.g:392:7: ( (lv_Anschluss_11_0= ruleBAnschluss ) )
                            // InternalRdsPP.g:393:8: (lv_Anschluss_11_0= ruleBAnschluss )
                            {
                            // InternalRdsPP.g:393:8: (lv_Anschluss_11_0= ruleBAnschluss )
                            // InternalRdsPP.g:394:9: lv_Anschluss_11_0= ruleBAnschluss
                            {

                            									newCompositeNode(grammarAccess.getBFunktionAccess().getAnschlussBAnschlussParserRuleCall_7_0_2_0_1_0());
                            								
                            pushFollow(FOLLOW_2);
                            lv_Anschluss_11_0=ruleBAnschluss();

                            state._fsp--;


                            									if (current==null) {
                            										current = createModelElementForParent(grammarAccess.getBFunktionRule());
                            									}
                            									set(
                            										current,
                            										"Anschluss",
                            										lv_Anschluss_11_0,
                            										"org.xtext.rdsserver.RdsPP.BAnschluss");
                            									afterParserOrEnumRuleCall();
                            								

                            }


                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalRdsPP.g:413:6: (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) )
                            {
                            // InternalRdsPP.g:413:6: (this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) ) )
                            // InternalRdsPP.g:414:7: this_S_12= RULE_S ( (lv_Dokumentation_13_0= ruleBDoku ) )
                            {
                            this_S_12=(Token)match(input,RULE_S,FOLLOW_14); 

                            							newLeafNode(this_S_12, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_1_0());
                            						
                            // InternalRdsPP.g:418:7: ( (lv_Dokumentation_13_0= ruleBDoku ) )
                            // InternalRdsPP.g:419:8: (lv_Dokumentation_13_0= ruleBDoku )
                            {
                            // InternalRdsPP.g:419:8: (lv_Dokumentation_13_0= ruleBDoku )
                            // InternalRdsPP.g:420:9: lv_Dokumentation_13_0= ruleBDoku
                            {

                            									newCompositeNode(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_0_2_1_1_0());
                            								
                            pushFollow(FOLLOW_2);
                            lv_Dokumentation_13_0=ruleBDoku();

                            state._fsp--;


                            									if (current==null) {
                            										current = createModelElementForParent(grammarAccess.getBFunktionRule());
                            									}
                            									set(
                            										current,
                            										"Dokumentation",
                            										lv_Dokumentation_13_0,
                            										"org.xtext.rdsserver.RdsPP.BDoku");
                            									afterParserOrEnumRuleCall();
                            								

                            }


                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:441:4: (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) )
                    {
                    // InternalRdsPP.g:441:4: (this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) ) )
                    // InternalRdsPP.g:442:5: this_S_14= RULE_S ( (lv_Signal_15_0= ruleBSignal ) )
                    {
                    this_S_14=(Token)match(input,RULE_S,FOLLOW_15); 

                    					newLeafNode(this_S_14, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_1_0());
                    				
                    // InternalRdsPP.g:446:5: ( (lv_Signal_15_0= ruleBSignal ) )
                    // InternalRdsPP.g:447:6: (lv_Signal_15_0= ruleBSignal )
                    {
                    // InternalRdsPP.g:447:6: (lv_Signal_15_0= ruleBSignal )
                    // InternalRdsPP.g:448:7: lv_Signal_15_0= ruleBSignal
                    {

                    							newCompositeNode(grammarAccess.getBFunktionAccess().getSignalBSignalParserRuleCall_7_1_1_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_Signal_15_0=ruleBSignal();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getBFunktionRule());
                    							}
                    							set(
                    								current,
                    								"Signal",
                    								lv_Signal_15_0,
                    								"org.xtext.rdsserver.RdsPP.BSignal");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalRdsPP.g:467:4: (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) )
                    {
                    // InternalRdsPP.g:467:4: (this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) ) )
                    // InternalRdsPP.g:468:5: this_S_16= RULE_S ( (lv_Dokumentation_17_0= ruleBDoku ) )
                    {
                    this_S_16=(Token)match(input,RULE_S,FOLLOW_14); 

                    					newLeafNode(this_S_16, grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_2_0());
                    				
                    // InternalRdsPP.g:472:5: ( (lv_Dokumentation_17_0= ruleBDoku ) )
                    // InternalRdsPP.g:473:6: (lv_Dokumentation_17_0= ruleBDoku )
                    {
                    // InternalRdsPP.g:473:6: (lv_Dokumentation_17_0= ruleBDoku )
                    // InternalRdsPP.g:474:7: lv_Dokumentation_17_0= ruleBDoku
                    {

                    							newCompositeNode(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_2_1_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_Dokumentation_17_0=ruleBDoku();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getBFunktionRule());
                    							}
                    							set(
                    								current,
                    								"Dokumentation",
                    								lv_Dokumentation_17_0,
                    								"org.xtext.rdsserver.RdsPP.BDoku");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBFunktion"


    // $ANTLR start "entryRuleBFunkZuordnung"
    // InternalRdsPP.g:497:1: entryRuleBFunkZuordnung returns [EObject current=null] : iv_ruleBFunkZuordnung= ruleBFunkZuordnung EOF ;
    public final EObject entryRuleBFunkZuordnung() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBFunkZuordnung = null;


        try {
            // InternalRdsPP.g:497:55: (iv_ruleBFunkZuordnung= ruleBFunkZuordnung EOF )
            // InternalRdsPP.g:498:2: iv_ruleBFunkZuordnung= ruleBFunkZuordnung EOF
            {
             newCompositeNode(grammarAccess.getBFunkZuordnungRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBFunkZuordnung=ruleBFunkZuordnung();

            state._fsp--;

             current =iv_ruleBFunkZuordnung; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBFunkZuordnung"


    // $ANTLR start "ruleBFunkZuordnung"
    // InternalRdsPP.g:504:1: ruleBFunkZuordnung returns [EObject current=null] : (otherlv_0= '==' ( (lv_Funktionsbereiche_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Leitfunktion_5_0= RULE_AANN ) ) ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )? ) ;
    public final EObject ruleBFunkZuordnung() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Funktionsbereiche_1_0=null;
        Token this_S_2=null;
        Token lv_Abschnitt1_3_0=null;
        Token otherlv_4=null;
        Token lv_Leitfunktion_5_0=null;
        Token this_S_6=null;
        Token this_S_8=null;
        EObject lv_Signal_7_0 = null;

        EObject lv_Dokumentation_9_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:510:2: ( (otherlv_0= '==' ( (lv_Funktionsbereiche_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Leitfunktion_5_0= RULE_AANN ) ) ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )? ) )
            // InternalRdsPP.g:511:2: (otherlv_0= '==' ( (lv_Funktionsbereiche_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Leitfunktion_5_0= RULE_AANN ) ) ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )? )
            {
            // InternalRdsPP.g:511:2: (otherlv_0= '==' ( (lv_Funktionsbereiche_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Leitfunktion_5_0= RULE_AANN ) ) ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )? )
            // InternalRdsPP.g:512:3: otherlv_0= '==' ( (lv_Funktionsbereiche_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Abschnitt1_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Leitfunktion_5_0= RULE_AANN ) ) ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )?
            {
            otherlv_0=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getBFunkZuordnungAccess().getEqualsSignEqualsSignKeyword_0());
            		
            // InternalRdsPP.g:516:3: ( (lv_Funktionsbereiche_1_0= RULE_ANN ) )
            // InternalRdsPP.g:517:4: (lv_Funktionsbereiche_1_0= RULE_ANN )
            {
            // InternalRdsPP.g:517:4: (lv_Funktionsbereiche_1_0= RULE_ANN )
            // InternalRdsPP.g:518:5: lv_Funktionsbereiche_1_0= RULE_ANN
            {
            lv_Funktionsbereiche_1_0=(Token)match(input,RULE_ANN,FOLLOW_5); 

            					newLeafNode(lv_Funktionsbereiche_1_0, grammarAccess.getBFunkZuordnungAccess().getFunktionsbereicheANNTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunkZuordnungRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Funktionsbereiche",
            						lv_Funktionsbereiche_1_0,
            						"org.xtext.rdsserver.RdsPP.ANN");
            				

            }


            }

            // InternalRdsPP.g:534:3: (this_S_2= RULE_S )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_S) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalRdsPP.g:535:4: this_S_2= RULE_S
                    {
                    this_S_2=(Token)match(input,RULE_S,FOLLOW_6); 

                    				newLeafNode(this_S_2, grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_2());
                    			

                    }
                    break;

            }

            // InternalRdsPP.g:540:3: ( (lv_Abschnitt1_3_0= RULE_AAANN ) )
            // InternalRdsPP.g:541:4: (lv_Abschnitt1_3_0= RULE_AAANN )
            {
            // InternalRdsPP.g:541:4: (lv_Abschnitt1_3_0= RULE_AAANN )
            // InternalRdsPP.g:542:5: lv_Abschnitt1_3_0= RULE_AAANN
            {
            lv_Abschnitt1_3_0=(Token)match(input,RULE_AAANN,FOLLOW_16); 

            					newLeafNode(lv_Abschnitt1_3_0, grammarAccess.getBFunkZuordnungAccess().getAbschnitt1AAANNTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunkZuordnungRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Abschnitt1",
            						lv_Abschnitt1_3_0,
            						"org.xtext.rdsserver.RdsPP.AAANN");
            				

            }


            }

            otherlv_4=(Token)match(input,20,FOLLOW_10); 

            			newLeafNode(otherlv_4, grammarAccess.getBFunkZuordnungAccess().getFullStopKeyword_4());
            		
            // InternalRdsPP.g:562:3: ( (lv_Leitfunktion_5_0= RULE_AANN ) )
            // InternalRdsPP.g:563:4: (lv_Leitfunktion_5_0= RULE_AANN )
            {
            // InternalRdsPP.g:563:4: (lv_Leitfunktion_5_0= RULE_AANN )
            // InternalRdsPP.g:564:5: lv_Leitfunktion_5_0= RULE_AANN
            {
            lv_Leitfunktion_5_0=(Token)match(input,RULE_AANN,FOLLOW_11); 

            					newLeafNode(lv_Leitfunktion_5_0, grammarAccess.getBFunkZuordnungAccess().getLeitfunktionAANNTerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBFunkZuordnungRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Leitfunktion",
            						lv_Leitfunktion_5_0,
            						"org.xtext.rdsserver.RdsPP.AANN");
            				

            }


            }

            // InternalRdsPP.g:580:3: ( (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) ) | (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) ) )?
            int alt9=3;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_S) ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==26) ) {
                    alt9=2;
                }
                else if ( (LA9_1==27) ) {
                    alt9=1;
                }
            }
            switch (alt9) {
                case 1 :
                    // InternalRdsPP.g:581:4: (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) )
                    {
                    // InternalRdsPP.g:581:4: (this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) ) )
                    // InternalRdsPP.g:582:5: this_S_6= RULE_S ( (lv_Signal_7_0= ruleBSignal ) )
                    {
                    this_S_6=(Token)match(input,RULE_S,FOLLOW_15); 

                    					newLeafNode(this_S_6, grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_0_0());
                    				
                    // InternalRdsPP.g:586:5: ( (lv_Signal_7_0= ruleBSignal ) )
                    // InternalRdsPP.g:587:6: (lv_Signal_7_0= ruleBSignal )
                    {
                    // InternalRdsPP.g:587:6: (lv_Signal_7_0= ruleBSignal )
                    // InternalRdsPP.g:588:7: lv_Signal_7_0= ruleBSignal
                    {

                    							newCompositeNode(grammarAccess.getBFunkZuordnungAccess().getSignalBSignalParserRuleCall_6_0_1_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_Signal_7_0=ruleBSignal();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getBFunkZuordnungRule());
                    							}
                    							set(
                    								current,
                    								"Signal",
                    								lv_Signal_7_0,
                    								"org.xtext.rdsserver.RdsPP.BSignal");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:607:4: (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )
                    {
                    // InternalRdsPP.g:607:4: (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )
                    // InternalRdsPP.g:608:5: this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) )
                    {
                    this_S_8=(Token)match(input,RULE_S,FOLLOW_14); 

                    					newLeafNode(this_S_8, grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_1_0());
                    				
                    // InternalRdsPP.g:612:5: ( (lv_Dokumentation_9_0= ruleBDoku ) )
                    // InternalRdsPP.g:613:6: (lv_Dokumentation_9_0= ruleBDoku )
                    {
                    // InternalRdsPP.g:613:6: (lv_Dokumentation_9_0= ruleBDoku )
                    // InternalRdsPP.g:614:7: lv_Dokumentation_9_0= ruleBDoku
                    {

                    							newCompositeNode(grammarAccess.getBFunkZuordnungAccess().getDokumentationBDokuParserRuleCall_6_1_1_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_Dokumentation_9_0=ruleBDoku();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getBFunkZuordnungRule());
                    							}
                    							set(
                    								current,
                    								"Dokumentation",
                    								lv_Dokumentation_9_0,
                    								"org.xtext.rdsserver.RdsPP.BDoku");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBFunkZuordnung"


    // $ANTLR start "entryRuleBEinbauort"
    // InternalRdsPP.g:637:1: entryRuleBEinbauort returns [EObject current=null] : iv_ruleBEinbauort= ruleBEinbauort EOF ;
    public final EObject entryRuleBEinbauort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBEinbauort = null;


        try {
            // InternalRdsPP.g:637:51: (iv_ruleBEinbauort= ruleBEinbauort EOF )
            // InternalRdsPP.g:638:2: iv_ruleBEinbauort= ruleBEinbauort EOF
            {
             newCompositeNode(grammarAccess.getBEinbauortRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBEinbauort=ruleBEinbauort();

            state._fsp--;

             current =iv_ruleBEinbauort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBEinbauort"


    // $ANTLR start "ruleBEinbauort"
    // InternalRdsPP.g:644:1: ruleBEinbauort returns [EObject current=null] : (otherlv_0= '+' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Einbaueinheit_3_0= RULE_AAANN ) ) (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )? otherlv_6= '.' ( (lv_Einbauplatz_7_0= ruleNA ) ) (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )? ) ;
    public final EObject ruleBEinbauort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Hauptsystem_1_0=null;
        Token this_S_2=null;
        Token lv_Einbaueinheit_3_0=null;
        Token this_S_4=null;
        Token lv_Klassifizierung_5_0=null;
        Token otherlv_6=null;
        Token this_S_8=null;
        AntlrDatatypeRuleToken lv_Einbauplatz_7_0 = null;

        EObject lv_Dokumentation_9_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:650:2: ( (otherlv_0= '+' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Einbaueinheit_3_0= RULE_AAANN ) ) (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )? otherlv_6= '.' ( (lv_Einbauplatz_7_0= ruleNA ) ) (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )? ) )
            // InternalRdsPP.g:651:2: (otherlv_0= '+' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Einbaueinheit_3_0= RULE_AAANN ) ) (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )? otherlv_6= '.' ( (lv_Einbauplatz_7_0= ruleNA ) ) (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )? )
            {
            // InternalRdsPP.g:651:2: (otherlv_0= '+' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Einbaueinheit_3_0= RULE_AAANN ) ) (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )? otherlv_6= '.' ( (lv_Einbauplatz_7_0= ruleNA ) ) (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )? )
            // InternalRdsPP.g:652:3: otherlv_0= '+' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Einbaueinheit_3_0= RULE_AAANN ) ) (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )? otherlv_6= '.' ( (lv_Einbauplatz_7_0= ruleNA ) ) (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )?
            {
            otherlv_0=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getBEinbauortAccess().getPlusSignKeyword_0());
            		
            // InternalRdsPP.g:656:3: ( (lv_Hauptsystem_1_0= RULE_ANN ) )
            // InternalRdsPP.g:657:4: (lv_Hauptsystem_1_0= RULE_ANN )
            {
            // InternalRdsPP.g:657:4: (lv_Hauptsystem_1_0= RULE_ANN )
            // InternalRdsPP.g:658:5: lv_Hauptsystem_1_0= RULE_ANN
            {
            lv_Hauptsystem_1_0=(Token)match(input,RULE_ANN,FOLLOW_5); 

            					newLeafNode(lv_Hauptsystem_1_0, grammarAccess.getBEinbauortAccess().getHauptsystemANNTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBEinbauortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Hauptsystem",
            						lv_Hauptsystem_1_0,
            						"org.xtext.rdsserver.RdsPP.ANN");
            				

            }


            }

            // InternalRdsPP.g:674:3: (this_S_2= RULE_S )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_S) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalRdsPP.g:675:4: this_S_2= RULE_S
                    {
                    this_S_2=(Token)match(input,RULE_S,FOLLOW_6); 

                    				newLeafNode(this_S_2, grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_2());
                    			

                    }
                    break;

            }

            // InternalRdsPP.g:680:3: ( (lv_Einbaueinheit_3_0= RULE_AAANN ) )
            // InternalRdsPP.g:681:4: (lv_Einbaueinheit_3_0= RULE_AAANN )
            {
            // InternalRdsPP.g:681:4: (lv_Einbaueinheit_3_0= RULE_AAANN )
            // InternalRdsPP.g:682:5: lv_Einbaueinheit_3_0= RULE_AAANN
            {
            lv_Einbaueinheit_3_0=(Token)match(input,RULE_AAANN,FOLLOW_17); 

            					newLeafNode(lv_Einbaueinheit_3_0, grammarAccess.getBEinbauortAccess().getEinbaueinheitAAANNTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBEinbauortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Einbaueinheit",
            						lv_Einbaueinheit_3_0,
            						"org.xtext.rdsserver.RdsPP.AAANN");
            				

            }


            }

            // InternalRdsPP.g:698:3: (this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_S) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalRdsPP.g:699:4: this_S_4= RULE_S ( (lv_Klassifizierung_5_0= RULE_AANNN ) )
                    {
                    this_S_4=(Token)match(input,RULE_S,FOLLOW_8); 

                    				newLeafNode(this_S_4, grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_4_0());
                    			
                    // InternalRdsPP.g:703:4: ( (lv_Klassifizierung_5_0= RULE_AANNN ) )
                    // InternalRdsPP.g:704:5: (lv_Klassifizierung_5_0= RULE_AANNN )
                    {
                    // InternalRdsPP.g:704:5: (lv_Klassifizierung_5_0= RULE_AANNN )
                    // InternalRdsPP.g:705:6: lv_Klassifizierung_5_0= RULE_AANNN
                    {
                    lv_Klassifizierung_5_0=(Token)match(input,RULE_AANNN,FOLLOW_16); 

                    						newLeafNode(lv_Klassifizierung_5_0, grammarAccess.getBEinbauortAccess().getKlassifizierungAANNNTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBEinbauortRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Klassifizierung",
                    							lv_Klassifizierung_5_0,
                    							"org.xtext.rdsserver.RdsPP.AANNN");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,20,FOLLOW_18); 

            			newLeafNode(otherlv_6, grammarAccess.getBEinbauortAccess().getFullStopKeyword_5());
            		
            // InternalRdsPP.g:726:3: ( (lv_Einbauplatz_7_0= ruleNA ) )
            // InternalRdsPP.g:727:4: (lv_Einbauplatz_7_0= ruleNA )
            {
            // InternalRdsPP.g:727:4: (lv_Einbauplatz_7_0= ruleNA )
            // InternalRdsPP.g:728:5: lv_Einbauplatz_7_0= ruleNA
            {

            					newCompositeNode(grammarAccess.getBEinbauortAccess().getEinbauplatzNAParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_11);
            lv_Einbauplatz_7_0=ruleNA();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBEinbauortRule());
            					}
            					set(
            						current,
            						"Einbauplatz",
            						lv_Einbauplatz_7_0,
            						"org.xtext.rdsserver.RdsPP.NA");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRdsPP.g:745:3: (this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_S) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalRdsPP.g:746:4: this_S_8= RULE_S ( (lv_Dokumentation_9_0= ruleBDoku ) )
                    {
                    this_S_8=(Token)match(input,RULE_S,FOLLOW_14); 

                    				newLeafNode(this_S_8, grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_7_0());
                    			
                    // InternalRdsPP.g:750:4: ( (lv_Dokumentation_9_0= ruleBDoku ) )
                    // InternalRdsPP.g:751:5: (lv_Dokumentation_9_0= ruleBDoku )
                    {
                    // InternalRdsPP.g:751:5: (lv_Dokumentation_9_0= ruleBDoku )
                    // InternalRdsPP.g:752:6: lv_Dokumentation_9_0= ruleBDoku
                    {

                    						newCompositeNode(grammarAccess.getBEinbauortAccess().getDokumentationBDokuParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_Dokumentation_9_0=ruleBDoku();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBEinbauortRule());
                    						}
                    						set(
                    							current,
                    							"Dokumentation",
                    							lv_Dokumentation_9_0,
                    							"org.xtext.rdsserver.RdsPP.BDoku");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBEinbauort"


    // $ANTLR start "entryRuleBAufstellungsort"
    // InternalRdsPP.g:774:1: entryRuleBAufstellungsort returns [EObject current=null] : iv_ruleBAufstellungsort= ruleBAufstellungsort EOF ;
    public final EObject entryRuleBAufstellungsort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBAufstellungsort = null;


        try {
            // InternalRdsPP.g:774:57: (iv_ruleBAufstellungsort= ruleBAufstellungsort EOF )
            // InternalRdsPP.g:775:2: iv_ruleBAufstellungsort= ruleBAufstellungsort EOF
            {
             newCompositeNode(grammarAccess.getBAufstellungsortRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBAufstellungsort=ruleBAufstellungsort();

            state._fsp--;

             current =iv_ruleBAufstellungsort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBAufstellungsort"


    // $ANTLR start "ruleBAufstellungsort"
    // InternalRdsPP.g:781:1: ruleBAufstellungsort returns [EObject current=null] : (otherlv_0= '++' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Ort_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Bereich_5_0= ruleNA ) ) (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )? ) ;
    public final EObject ruleBAufstellungsort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Hauptsystem_1_0=null;
        Token this_S_2=null;
        Token lv_Ort_3_0=null;
        Token otherlv_4=null;
        Token this_S_6=null;
        AntlrDatatypeRuleToken lv_Bereich_5_0 = null;

        EObject lv_Dokumentation_7_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:787:2: ( (otherlv_0= '++' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Ort_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Bereich_5_0= ruleNA ) ) (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )? ) )
            // InternalRdsPP.g:788:2: (otherlv_0= '++' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Ort_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Bereich_5_0= ruleNA ) ) (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )? )
            {
            // InternalRdsPP.g:788:2: (otherlv_0= '++' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Ort_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Bereich_5_0= ruleNA ) ) (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )? )
            // InternalRdsPP.g:789:3: otherlv_0= '++' ( (lv_Hauptsystem_1_0= RULE_ANN ) ) (this_S_2= RULE_S )? ( (lv_Ort_3_0= RULE_AAANN ) ) otherlv_4= '.' ( (lv_Bereich_5_0= ruleNA ) ) (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )?
            {
            otherlv_0=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getBAufstellungsortAccess().getPlusSignPlusSignKeyword_0());
            		
            // InternalRdsPP.g:793:3: ( (lv_Hauptsystem_1_0= RULE_ANN ) )
            // InternalRdsPP.g:794:4: (lv_Hauptsystem_1_0= RULE_ANN )
            {
            // InternalRdsPP.g:794:4: (lv_Hauptsystem_1_0= RULE_ANN )
            // InternalRdsPP.g:795:5: lv_Hauptsystem_1_0= RULE_ANN
            {
            lv_Hauptsystem_1_0=(Token)match(input,RULE_ANN,FOLLOW_5); 

            					newLeafNode(lv_Hauptsystem_1_0, grammarAccess.getBAufstellungsortAccess().getHauptsystemANNTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBAufstellungsortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Hauptsystem",
            						lv_Hauptsystem_1_0,
            						"org.xtext.rdsserver.RdsPP.ANN");
            				

            }


            }

            // InternalRdsPP.g:811:3: (this_S_2= RULE_S )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_S) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRdsPP.g:812:4: this_S_2= RULE_S
                    {
                    this_S_2=(Token)match(input,RULE_S,FOLLOW_6); 

                    				newLeafNode(this_S_2, grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_2());
                    			

                    }
                    break;

            }

            // InternalRdsPP.g:817:3: ( (lv_Ort_3_0= RULE_AAANN ) )
            // InternalRdsPP.g:818:4: (lv_Ort_3_0= RULE_AAANN )
            {
            // InternalRdsPP.g:818:4: (lv_Ort_3_0= RULE_AAANN )
            // InternalRdsPP.g:819:5: lv_Ort_3_0= RULE_AAANN
            {
            lv_Ort_3_0=(Token)match(input,RULE_AAANN,FOLLOW_16); 

            					newLeafNode(lv_Ort_3_0, grammarAccess.getBAufstellungsortAccess().getOrtAAANNTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBAufstellungsortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Ort",
            						lv_Ort_3_0,
            						"org.xtext.rdsserver.RdsPP.AAANN");
            				

            }


            }

            otherlv_4=(Token)match(input,20,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getBAufstellungsortAccess().getFullStopKeyword_4());
            		
            // InternalRdsPP.g:839:3: ( (lv_Bereich_5_0= ruleNA ) )
            // InternalRdsPP.g:840:4: (lv_Bereich_5_0= ruleNA )
            {
            // InternalRdsPP.g:840:4: (lv_Bereich_5_0= ruleNA )
            // InternalRdsPP.g:841:5: lv_Bereich_5_0= ruleNA
            {

            					newCompositeNode(grammarAccess.getBAufstellungsortAccess().getBereichNAParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_11);
            lv_Bereich_5_0=ruleNA();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBAufstellungsortRule());
            					}
            					set(
            						current,
            						"Bereich",
            						lv_Bereich_5_0,
            						"org.xtext.rdsserver.RdsPP.NA");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRdsPP.g:858:3: (this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_S) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalRdsPP.g:859:4: this_S_6= RULE_S ( (lv_Dokumentation_7_0= ruleBDoku ) )
                    {
                    this_S_6=(Token)match(input,RULE_S,FOLLOW_14); 

                    				newLeafNode(this_S_6, grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_6_0());
                    			
                    // InternalRdsPP.g:863:4: ( (lv_Dokumentation_7_0= ruleBDoku ) )
                    // InternalRdsPP.g:864:5: (lv_Dokumentation_7_0= ruleBDoku )
                    {
                    // InternalRdsPP.g:864:5: (lv_Dokumentation_7_0= ruleBDoku )
                    // InternalRdsPP.g:865:6: lv_Dokumentation_7_0= ruleBDoku
                    {

                    						newCompositeNode(grammarAccess.getBAufstellungsortAccess().getDokumentationBDokuParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_Dokumentation_7_0=ruleBDoku();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBAufstellungsortRule());
                    						}
                    						set(
                    							current,
                    							"Dokumentation",
                    							lv_Dokumentation_7_0,
                    							"org.xtext.rdsserver.RdsPP.BDoku");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBAufstellungsort"


    // $ANTLR start "entryRuleBProdukt"
    // InternalRdsPP.g:887:1: entryRuleBProdukt returns [EObject current=null] : iv_ruleBProdukt= ruleBProdukt EOF ;
    public final EObject entryRuleBProdukt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBProdukt = null;


        try {
            // InternalRdsPP.g:887:49: (iv_ruleBProdukt= ruleBProdukt EOF )
            // InternalRdsPP.g:888:2: iv_ruleBProdukt= ruleBProdukt EOF
            {
             newCompositeNode(grammarAccess.getBProduktRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBProdukt=ruleBProdukt();

            state._fsp--;

             current =iv_ruleBProdukt; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBProdukt"


    // $ANTLR start "ruleBProdukt"
    // InternalRdsPP.g:894:1: ruleBProdukt returns [EObject current=null] : (otherlv_0= '-' ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) ) ) ;
    public final EObject ruleBProdukt() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Produkt_1_1=null;
        Token lv_Produkt_1_2=null;


        	enterRule();

        try {
            // InternalRdsPP.g:900:2: ( (otherlv_0= '-' ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) ) ) )
            // InternalRdsPP.g:901:2: (otherlv_0= '-' ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) ) )
            {
            // InternalRdsPP.g:901:2: (otherlv_0= '-' ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) ) )
            // InternalRdsPP.g:902:3: otherlv_0= '-' ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getBProduktAccess().getHyphenMinusKeyword_0());
            		
            // InternalRdsPP.g:906:3: ( ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) ) )
            // InternalRdsPP.g:907:4: ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) )
            {
            // InternalRdsPP.g:907:4: ( (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN ) )
            // InternalRdsPP.g:908:5: (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN )
            {
            // InternalRdsPP.g:908:5: (lv_Produkt_1_1= RULE_AANN | lv_Produkt_1_2= RULE_AANNN )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_AANN) ) {
                alt15=1;
            }
            else if ( (LA15_0==RULE_AANNN) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalRdsPP.g:909:6: lv_Produkt_1_1= RULE_AANN
                    {
                    lv_Produkt_1_1=(Token)match(input,RULE_AANN,FOLLOW_2); 

                    						newLeafNode(lv_Produkt_1_1, grammarAccess.getBProduktAccess().getProduktAANNTerminalRuleCall_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBProduktRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Produkt",
                    							lv_Produkt_1_1,
                    							"org.xtext.rdsserver.RdsPP.AANN");
                    					

                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:924:6: lv_Produkt_1_2= RULE_AANNN
                    {
                    lv_Produkt_1_2=(Token)match(input,RULE_AANNN,FOLLOW_2); 

                    						newLeafNode(lv_Produkt_1_2, grammarAccess.getBProduktAccess().getProduktAANNNTerminalRuleCall_1_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBProduktRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Produkt",
                    							lv_Produkt_1_2,
                    							"org.xtext.rdsserver.RdsPP.AANNN");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBProdukt"


    // $ANTLR start "entryRuleBAnschluss"
    // InternalRdsPP.g:945:1: entryRuleBAnschluss returns [EObject current=null] : iv_ruleBAnschluss= ruleBAnschluss EOF ;
    public final EObject entryRuleBAnschluss() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBAnschluss = null;


        try {
            // InternalRdsPP.g:945:51: (iv_ruleBAnschluss= ruleBAnschluss EOF )
            // InternalRdsPP.g:946:2: iv_ruleBAnschluss= ruleBAnschluss EOF
            {
             newCompositeNode(grammarAccess.getBAnschlussRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBAnschluss=ruleBAnschluss();

            state._fsp--;

             current =iv_ruleBAnschluss; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBAnschluss"


    // $ANTLR start "ruleBAnschluss"
    // InternalRdsPP.g:952:1: ruleBAnschluss returns [EObject current=null] : (otherlv_0= ':' ( (lv_Kennzeichnung1_1_0= RULE_STRING ) ) (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )? (this_S_4= RULE_S )? ) ;
    public final EObject ruleBAnschluss() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Kennzeichnung1_1_0=null;
        Token otherlv_2=null;
        Token lv_Kennzeichnung2_3_0=null;
        Token this_S_4=null;


        	enterRule();

        try {
            // InternalRdsPP.g:958:2: ( (otherlv_0= ':' ( (lv_Kennzeichnung1_1_0= RULE_STRING ) ) (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )? (this_S_4= RULE_S )? ) )
            // InternalRdsPP.g:959:2: (otherlv_0= ':' ( (lv_Kennzeichnung1_1_0= RULE_STRING ) ) (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )? (this_S_4= RULE_S )? )
            {
            // InternalRdsPP.g:959:2: (otherlv_0= ':' ( (lv_Kennzeichnung1_1_0= RULE_STRING ) ) (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )? (this_S_4= RULE_S )? )
            // InternalRdsPP.g:960:3: otherlv_0= ':' ( (lv_Kennzeichnung1_1_0= RULE_STRING ) ) (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )? (this_S_4= RULE_S )?
            {
            otherlv_0=(Token)match(input,25,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getBAnschlussAccess().getColonKeyword_0());
            		
            // InternalRdsPP.g:964:3: ( (lv_Kennzeichnung1_1_0= RULE_STRING ) )
            // InternalRdsPP.g:965:4: (lv_Kennzeichnung1_1_0= RULE_STRING )
            {
            // InternalRdsPP.g:965:4: (lv_Kennzeichnung1_1_0= RULE_STRING )
            // InternalRdsPP.g:966:5: lv_Kennzeichnung1_1_0= RULE_STRING
            {
            lv_Kennzeichnung1_1_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_Kennzeichnung1_1_0, grammarAccess.getBAnschlussAccess().getKennzeichnung1STRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBAnschlussRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Kennzeichnung1",
            						lv_Kennzeichnung1_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalRdsPP.g:982:3: (otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==20) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRdsPP.g:983:4: otherlv_2= '.' ( (lv_Kennzeichnung2_3_0= RULE_STRING ) )
                    {
                    otherlv_2=(Token)match(input,20,FOLLOW_20); 

                    				newLeafNode(otherlv_2, grammarAccess.getBAnschlussAccess().getFullStopKeyword_2_0());
                    			
                    // InternalRdsPP.g:987:4: ( (lv_Kennzeichnung2_3_0= RULE_STRING ) )
                    // InternalRdsPP.g:988:5: (lv_Kennzeichnung2_3_0= RULE_STRING )
                    {
                    // InternalRdsPP.g:988:5: (lv_Kennzeichnung2_3_0= RULE_STRING )
                    // InternalRdsPP.g:989:6: lv_Kennzeichnung2_3_0= RULE_STRING
                    {
                    lv_Kennzeichnung2_3_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

                    						newLeafNode(lv_Kennzeichnung2_3_0, grammarAccess.getBAnschlussAccess().getKennzeichnung2STRINGTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBAnschlussRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Kennzeichnung2",
                    							lv_Kennzeichnung2_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRdsPP.g:1006:3: (this_S_4= RULE_S )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_S) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRdsPP.g:1007:4: this_S_4= RULE_S
                    {
                    this_S_4=(Token)match(input,RULE_S,FOLLOW_2); 

                    				newLeafNode(this_S_4, grammarAccess.getBAnschlussAccess().getSTerminalRuleCall_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBAnschluss"


    // $ANTLR start "entryRuleBDoku"
    // InternalRdsPP.g:1016:1: entryRuleBDoku returns [EObject current=null] : iv_ruleBDoku= ruleBDoku EOF ;
    public final EObject entryRuleBDoku() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBDoku = null;


        try {
            // InternalRdsPP.g:1016:46: (iv_ruleBDoku= ruleBDoku EOF )
            // InternalRdsPP.g:1017:2: iv_ruleBDoku= ruleBDoku EOF
            {
             newCompositeNode(grammarAccess.getBDokuRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBDoku=ruleBDoku();

            state._fsp--;

             current =iv_ruleBDoku; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBDoku"


    // $ANTLR start "ruleBDoku"
    // InternalRdsPP.g:1023:1: ruleBDoku returns [EObject current=null] : (otherlv_0= '&' ( (lv_Doku_1_0= RULE_AAANNN ) ) (this_S_2= RULE_S )? ) ;
    public final EObject ruleBDoku() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Doku_1_0=null;
        Token this_S_2=null;


        	enterRule();

        try {
            // InternalRdsPP.g:1029:2: ( (otherlv_0= '&' ( (lv_Doku_1_0= RULE_AAANNN ) ) (this_S_2= RULE_S )? ) )
            // InternalRdsPP.g:1030:2: (otherlv_0= '&' ( (lv_Doku_1_0= RULE_AAANNN ) ) (this_S_2= RULE_S )? )
            {
            // InternalRdsPP.g:1030:2: (otherlv_0= '&' ( (lv_Doku_1_0= RULE_AAANNN ) ) (this_S_2= RULE_S )? )
            // InternalRdsPP.g:1031:3: otherlv_0= '&' ( (lv_Doku_1_0= RULE_AAANNN ) ) (this_S_2= RULE_S )?
            {
            otherlv_0=(Token)match(input,26,FOLLOW_21); 

            			newLeafNode(otherlv_0, grammarAccess.getBDokuAccess().getAmpersandKeyword_0());
            		
            // InternalRdsPP.g:1035:3: ( (lv_Doku_1_0= RULE_AAANNN ) )
            // InternalRdsPP.g:1036:4: (lv_Doku_1_0= RULE_AAANNN )
            {
            // InternalRdsPP.g:1036:4: (lv_Doku_1_0= RULE_AAANNN )
            // InternalRdsPP.g:1037:5: lv_Doku_1_0= RULE_AAANNN
            {
            lv_Doku_1_0=(Token)match(input,RULE_AAANNN,FOLLOW_11); 

            					newLeafNode(lv_Doku_1_0, grammarAccess.getBDokuAccess().getDokuAAANNNTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBDokuRule());
            					}
            					setWithLastConsumed(
            						current,
            						"Doku",
            						lv_Doku_1_0,
            						"org.xtext.rdsserver.RdsPP.AAANNN");
            				

            }


            }

            // InternalRdsPP.g:1053:3: (this_S_2= RULE_S )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_S) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalRdsPP.g:1054:4: this_S_2= RULE_S
                    {
                    this_S_2=(Token)match(input,RULE_S,FOLLOW_2); 

                    				newLeafNode(this_S_2, grammarAccess.getBDokuAccess().getSTerminalRuleCall_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBDoku"


    // $ANTLR start "entryRuleBSignal"
    // InternalRdsPP.g:1063:1: entryRuleBSignal returns [EObject current=null] : iv_ruleBSignal= ruleBSignal EOF ;
    public final EObject entryRuleBSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBSignal = null;


        try {
            // InternalRdsPP.g:1063:48: (iv_ruleBSignal= ruleBSignal EOF )
            // InternalRdsPP.g:1064:2: iv_ruleBSignal= ruleBSignal EOF
            {
             newCompositeNode(grammarAccess.getBSignalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBSignal=ruleBSignal();

            state._fsp--;

             current =iv_ruleBSignal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBSignal"


    // $ANTLR start "ruleBSignal"
    // InternalRdsPP.g:1070:1: ruleBSignal returns [EObject current=null] : (otherlv_0= ';' ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) ) otherlv_2= '.' ( (lv_Kennzeichnung_3_0= ruleNA ) ) (this_S_4= RULE_S )? ) ;
    public final EObject ruleBSignal() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_Klassifizierung_1_1=null;
        Token lv_Klassifizierung_1_2=null;
        Token otherlv_2=null;
        Token this_S_4=null;
        AntlrDatatypeRuleToken lv_Kennzeichnung_3_0 = null;



        	enterRule();

        try {
            // InternalRdsPP.g:1076:2: ( (otherlv_0= ';' ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) ) otherlv_2= '.' ( (lv_Kennzeichnung_3_0= ruleNA ) ) (this_S_4= RULE_S )? ) )
            // InternalRdsPP.g:1077:2: (otherlv_0= ';' ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) ) otherlv_2= '.' ( (lv_Kennzeichnung_3_0= ruleNA ) ) (this_S_4= RULE_S )? )
            {
            // InternalRdsPP.g:1077:2: (otherlv_0= ';' ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) ) otherlv_2= '.' ( (lv_Kennzeichnung_3_0= ruleNA ) ) (this_S_4= RULE_S )? )
            // InternalRdsPP.g:1078:3: otherlv_0= ';' ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) ) otherlv_2= '.' ( (lv_Kennzeichnung_3_0= ruleNA ) ) (this_S_4= RULE_S )?
            {
            otherlv_0=(Token)match(input,27,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getBSignalAccess().getSemicolonKeyword_0());
            		
            // InternalRdsPP.g:1082:3: ( ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) ) )
            // InternalRdsPP.g:1083:4: ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) )
            {
            // InternalRdsPP.g:1083:4: ( (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN ) )
            // InternalRdsPP.g:1084:5: (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN )
            {
            // InternalRdsPP.g:1084:5: (lv_Klassifizierung_1_1= RULE_AANN | lv_Klassifizierung_1_2= RULE_AANNN )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_AANN) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_AANNN) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalRdsPP.g:1085:6: lv_Klassifizierung_1_1= RULE_AANN
                    {
                    lv_Klassifizierung_1_1=(Token)match(input,RULE_AANN,FOLLOW_16); 

                    						newLeafNode(lv_Klassifizierung_1_1, grammarAccess.getBSignalAccess().getKlassifizierungAANNTerminalRuleCall_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBSignalRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Klassifizierung",
                    							lv_Klassifizierung_1_1,
                    							"org.xtext.rdsserver.RdsPP.AANN");
                    					

                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:1100:6: lv_Klassifizierung_1_2= RULE_AANNN
                    {
                    lv_Klassifizierung_1_2=(Token)match(input,RULE_AANNN,FOLLOW_16); 

                    						newLeafNode(lv_Klassifizierung_1_2, grammarAccess.getBSignalAccess().getKlassifizierungAANNNTerminalRuleCall_1_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBSignalRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"Klassifizierung",
                    							lv_Klassifizierung_1_2,
                    							"org.xtext.rdsserver.RdsPP.AANNN");
                    					

                    }
                    break;

            }


            }


            }

            otherlv_2=(Token)match(input,20,FOLLOW_18); 

            			newLeafNode(otherlv_2, grammarAccess.getBSignalAccess().getFullStopKeyword_2());
            		
            // InternalRdsPP.g:1121:3: ( (lv_Kennzeichnung_3_0= ruleNA ) )
            // InternalRdsPP.g:1122:4: (lv_Kennzeichnung_3_0= ruleNA )
            {
            // InternalRdsPP.g:1122:4: (lv_Kennzeichnung_3_0= ruleNA )
            // InternalRdsPP.g:1123:5: lv_Kennzeichnung_3_0= ruleNA
            {

            					newCompositeNode(grammarAccess.getBSignalAccess().getKennzeichnungNAParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_11);
            lv_Kennzeichnung_3_0=ruleNA();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBSignalRule());
            					}
            					set(
            						current,
            						"Kennzeichnung",
            						lv_Kennzeichnung_3_0,
            						"org.xtext.rdsserver.RdsPP.NA");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRdsPP.g:1140:3: (this_S_4= RULE_S )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_S) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalRdsPP.g:1141:4: this_S_4= RULE_S
                    {
                    this_S_4=(Token)match(input,RULE_S,FOLLOW_2); 

                    				newLeafNode(this_S_4, grammarAccess.getBSignalAccess().getSTerminalRuleCall_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBSignal"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000280L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000100082L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000100080L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000000000B0L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000600L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000001000L});

}