package org.xtext.rdsserver.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.rdsserver.services.RdsPPGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRdsPPParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_A", "RULE_N", "RULE_AANN", "RULE_AANNN", "RULE_S", "RULE_ANN", "RULE_AAANN", "RULE_STRING", "RULE_AAANNN", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'='", "'.'", "'=='", "'+'", "'++'", "'-'", "':'", "'&'", "';'"
    };
    public static final int RULE_A=4;
    public static final int RULE_AAANNN=12;
    public static final int RULE_ANN=9;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=16;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_AANNN=7;
    public static final int RULE_AANN=6;
    public static final int RULE_ID=13;
    public static final int RULE_WS=17;
    public static final int RULE_AAANN=10;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_S=8;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_N=5;
    public static final int RULE_INT=14;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=15;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRdsPPParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRdsPPParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRdsPPParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRdsPP.g"; }


    	private RdsPPGrammarAccess grammarAccess;

    	public void setGrammarAccess(RdsPPGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDomainmodel"
    // InternalRdsPP.g:53:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // InternalRdsPP.g:54:1: ( ruleDomainmodel EOF )
            // InternalRdsPP.g:55:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalRdsPP.g:62:1: ruleDomainmodel : ( ( rule__Domainmodel__KennzeichnungAssignment ) ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:66:2: ( ( ( rule__Domainmodel__KennzeichnungAssignment ) ) )
            // InternalRdsPP.g:67:2: ( ( rule__Domainmodel__KennzeichnungAssignment ) )
            {
            // InternalRdsPP.g:67:2: ( ( rule__Domainmodel__KennzeichnungAssignment ) )
            // InternalRdsPP.g:68:3: ( rule__Domainmodel__KennzeichnungAssignment )
            {
             before(grammarAccess.getDomainmodelAccess().getKennzeichnungAssignment()); 
            // InternalRdsPP.g:69:3: ( rule__Domainmodel__KennzeichnungAssignment )
            // InternalRdsPP.g:69:4: rule__Domainmodel__KennzeichnungAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Domainmodel__KennzeichnungAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDomainmodelAccess().getKennzeichnungAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleNA"
    // InternalRdsPP.g:78:1: entryRuleNA : ruleNA EOF ;
    public final void entryRuleNA() throws RecognitionException {
        try {
            // InternalRdsPP.g:79:1: ( ruleNA EOF )
            // InternalRdsPP.g:80:1: ruleNA EOF
            {
             before(grammarAccess.getNARule()); 
            pushFollow(FOLLOW_1);
            ruleNA();

            state._fsp--;

             after(grammarAccess.getNARule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNA"


    // $ANTLR start "ruleNA"
    // InternalRdsPP.g:87:1: ruleNA : ( ( rule__NA__Alternatives )* ) ;
    public final void ruleNA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:91:2: ( ( ( rule__NA__Alternatives )* ) )
            // InternalRdsPP.g:92:2: ( ( rule__NA__Alternatives )* )
            {
            // InternalRdsPP.g:92:2: ( ( rule__NA__Alternatives )* )
            // InternalRdsPP.g:93:3: ( rule__NA__Alternatives )*
            {
             before(grammarAccess.getNAAccess().getAlternatives()); 
            // InternalRdsPP.g:94:3: ( rule__NA__Alternatives )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_A && LA1_0<=RULE_N)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalRdsPP.g:94:4: rule__NA__Alternatives
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__NA__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getNAAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNA"


    // $ANTLR start "entryRuleKennzeichnung"
    // InternalRdsPP.g:103:1: entryRuleKennzeichnung : ruleKennzeichnung EOF ;
    public final void entryRuleKennzeichnung() throws RecognitionException {
        try {
            // InternalRdsPP.g:104:1: ( ruleKennzeichnung EOF )
            // InternalRdsPP.g:105:1: ruleKennzeichnung EOF
            {
             before(grammarAccess.getKennzeichnungRule()); 
            pushFollow(FOLLOW_1);
            ruleKennzeichnung();

            state._fsp--;

             after(grammarAccess.getKennzeichnungRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleKennzeichnung"


    // $ANTLR start "ruleKennzeichnung"
    // InternalRdsPP.g:112:1: ruleKennzeichnung : ( ( rule__Kennzeichnung__Alternatives ) ) ;
    public final void ruleKennzeichnung() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:116:2: ( ( ( rule__Kennzeichnung__Alternatives ) ) )
            // InternalRdsPP.g:117:2: ( ( rule__Kennzeichnung__Alternatives ) )
            {
            // InternalRdsPP.g:117:2: ( ( rule__Kennzeichnung__Alternatives ) )
            // InternalRdsPP.g:118:3: ( rule__Kennzeichnung__Alternatives )
            {
             before(grammarAccess.getKennzeichnungAccess().getAlternatives()); 
            // InternalRdsPP.g:119:3: ( rule__Kennzeichnung__Alternatives )
            // InternalRdsPP.g:119:4: rule__Kennzeichnung__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Kennzeichnung__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getKennzeichnungAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKennzeichnung"


    // $ANTLR start "entryRuleBFunktion"
    // InternalRdsPP.g:128:1: entryRuleBFunktion : ruleBFunktion EOF ;
    public final void entryRuleBFunktion() throws RecognitionException {
        try {
            // InternalRdsPP.g:129:1: ( ruleBFunktion EOF )
            // InternalRdsPP.g:130:1: ruleBFunktion EOF
            {
             before(grammarAccess.getBFunktionRule()); 
            pushFollow(FOLLOW_1);
            ruleBFunktion();

            state._fsp--;

             after(grammarAccess.getBFunktionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBFunktion"


    // $ANTLR start "ruleBFunktion"
    // InternalRdsPP.g:137:1: ruleBFunktion : ( ( rule__BFunktion__Group__0 ) ) ;
    public final void ruleBFunktion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:141:2: ( ( ( rule__BFunktion__Group__0 ) ) )
            // InternalRdsPP.g:142:2: ( ( rule__BFunktion__Group__0 ) )
            {
            // InternalRdsPP.g:142:2: ( ( rule__BFunktion__Group__0 ) )
            // InternalRdsPP.g:143:3: ( rule__BFunktion__Group__0 )
            {
             before(grammarAccess.getBFunktionAccess().getGroup()); 
            // InternalRdsPP.g:144:3: ( rule__BFunktion__Group__0 )
            // InternalRdsPP.g:144:4: rule__BFunktion__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBFunktion"


    // $ANTLR start "entryRuleBFunkZuordnung"
    // InternalRdsPP.g:153:1: entryRuleBFunkZuordnung : ruleBFunkZuordnung EOF ;
    public final void entryRuleBFunkZuordnung() throws RecognitionException {
        try {
            // InternalRdsPP.g:154:1: ( ruleBFunkZuordnung EOF )
            // InternalRdsPP.g:155:1: ruleBFunkZuordnung EOF
            {
             before(grammarAccess.getBFunkZuordnungRule()); 
            pushFollow(FOLLOW_1);
            ruleBFunkZuordnung();

            state._fsp--;

             after(grammarAccess.getBFunkZuordnungRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBFunkZuordnung"


    // $ANTLR start "ruleBFunkZuordnung"
    // InternalRdsPP.g:162:1: ruleBFunkZuordnung : ( ( rule__BFunkZuordnung__Group__0 ) ) ;
    public final void ruleBFunkZuordnung() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:166:2: ( ( ( rule__BFunkZuordnung__Group__0 ) ) )
            // InternalRdsPP.g:167:2: ( ( rule__BFunkZuordnung__Group__0 ) )
            {
            // InternalRdsPP.g:167:2: ( ( rule__BFunkZuordnung__Group__0 ) )
            // InternalRdsPP.g:168:3: ( rule__BFunkZuordnung__Group__0 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getGroup()); 
            // InternalRdsPP.g:169:3: ( rule__BFunkZuordnung__Group__0 )
            // InternalRdsPP.g:169:4: rule__BFunkZuordnung__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBFunkZuordnung"


    // $ANTLR start "entryRuleBEinbauort"
    // InternalRdsPP.g:178:1: entryRuleBEinbauort : ruleBEinbauort EOF ;
    public final void entryRuleBEinbauort() throws RecognitionException {
        try {
            // InternalRdsPP.g:179:1: ( ruleBEinbauort EOF )
            // InternalRdsPP.g:180:1: ruleBEinbauort EOF
            {
             before(grammarAccess.getBEinbauortRule()); 
            pushFollow(FOLLOW_1);
            ruleBEinbauort();

            state._fsp--;

             after(grammarAccess.getBEinbauortRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBEinbauort"


    // $ANTLR start "ruleBEinbauort"
    // InternalRdsPP.g:187:1: ruleBEinbauort : ( ( rule__BEinbauort__Group__0 ) ) ;
    public final void ruleBEinbauort() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:191:2: ( ( ( rule__BEinbauort__Group__0 ) ) )
            // InternalRdsPP.g:192:2: ( ( rule__BEinbauort__Group__0 ) )
            {
            // InternalRdsPP.g:192:2: ( ( rule__BEinbauort__Group__0 ) )
            // InternalRdsPP.g:193:3: ( rule__BEinbauort__Group__0 )
            {
             before(grammarAccess.getBEinbauortAccess().getGroup()); 
            // InternalRdsPP.g:194:3: ( rule__BEinbauort__Group__0 )
            // InternalRdsPP.g:194:4: rule__BEinbauort__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBEinbauort"


    // $ANTLR start "entryRuleBAufstellungsort"
    // InternalRdsPP.g:203:1: entryRuleBAufstellungsort : ruleBAufstellungsort EOF ;
    public final void entryRuleBAufstellungsort() throws RecognitionException {
        try {
            // InternalRdsPP.g:204:1: ( ruleBAufstellungsort EOF )
            // InternalRdsPP.g:205:1: ruleBAufstellungsort EOF
            {
             before(grammarAccess.getBAufstellungsortRule()); 
            pushFollow(FOLLOW_1);
            ruleBAufstellungsort();

            state._fsp--;

             after(grammarAccess.getBAufstellungsortRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBAufstellungsort"


    // $ANTLR start "ruleBAufstellungsort"
    // InternalRdsPP.g:212:1: ruleBAufstellungsort : ( ( rule__BAufstellungsort__Group__0 ) ) ;
    public final void ruleBAufstellungsort() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:216:2: ( ( ( rule__BAufstellungsort__Group__0 ) ) )
            // InternalRdsPP.g:217:2: ( ( rule__BAufstellungsort__Group__0 ) )
            {
            // InternalRdsPP.g:217:2: ( ( rule__BAufstellungsort__Group__0 ) )
            // InternalRdsPP.g:218:3: ( rule__BAufstellungsort__Group__0 )
            {
             before(grammarAccess.getBAufstellungsortAccess().getGroup()); 
            // InternalRdsPP.g:219:3: ( rule__BAufstellungsort__Group__0 )
            // InternalRdsPP.g:219:4: rule__BAufstellungsort__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBAufstellungsortAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBAufstellungsort"


    // $ANTLR start "entryRuleBProdukt"
    // InternalRdsPP.g:228:1: entryRuleBProdukt : ruleBProdukt EOF ;
    public final void entryRuleBProdukt() throws RecognitionException {
        try {
            // InternalRdsPP.g:229:1: ( ruleBProdukt EOF )
            // InternalRdsPP.g:230:1: ruleBProdukt EOF
            {
             before(grammarAccess.getBProduktRule()); 
            pushFollow(FOLLOW_1);
            ruleBProdukt();

            state._fsp--;

             after(grammarAccess.getBProduktRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBProdukt"


    // $ANTLR start "ruleBProdukt"
    // InternalRdsPP.g:237:1: ruleBProdukt : ( ( rule__BProdukt__Group__0 ) ) ;
    public final void ruleBProdukt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:241:2: ( ( ( rule__BProdukt__Group__0 ) ) )
            // InternalRdsPP.g:242:2: ( ( rule__BProdukt__Group__0 ) )
            {
            // InternalRdsPP.g:242:2: ( ( rule__BProdukt__Group__0 ) )
            // InternalRdsPP.g:243:3: ( rule__BProdukt__Group__0 )
            {
             before(grammarAccess.getBProduktAccess().getGroup()); 
            // InternalRdsPP.g:244:3: ( rule__BProdukt__Group__0 )
            // InternalRdsPP.g:244:4: rule__BProdukt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BProdukt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBProduktAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBProdukt"


    // $ANTLR start "entryRuleBAnschluss"
    // InternalRdsPP.g:253:1: entryRuleBAnschluss : ruleBAnschluss EOF ;
    public final void entryRuleBAnschluss() throws RecognitionException {
        try {
            // InternalRdsPP.g:254:1: ( ruleBAnschluss EOF )
            // InternalRdsPP.g:255:1: ruleBAnschluss EOF
            {
             before(grammarAccess.getBAnschlussRule()); 
            pushFollow(FOLLOW_1);
            ruleBAnschluss();

            state._fsp--;

             after(grammarAccess.getBAnschlussRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBAnschluss"


    // $ANTLR start "ruleBAnschluss"
    // InternalRdsPP.g:262:1: ruleBAnschluss : ( ( rule__BAnschluss__Group__0 ) ) ;
    public final void ruleBAnschluss() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:266:2: ( ( ( rule__BAnschluss__Group__0 ) ) )
            // InternalRdsPP.g:267:2: ( ( rule__BAnschluss__Group__0 ) )
            {
            // InternalRdsPP.g:267:2: ( ( rule__BAnschluss__Group__0 ) )
            // InternalRdsPP.g:268:3: ( rule__BAnschluss__Group__0 )
            {
             before(grammarAccess.getBAnschlussAccess().getGroup()); 
            // InternalRdsPP.g:269:3: ( rule__BAnschluss__Group__0 )
            // InternalRdsPP.g:269:4: rule__BAnschluss__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBAnschlussAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBAnschluss"


    // $ANTLR start "entryRuleBDoku"
    // InternalRdsPP.g:278:1: entryRuleBDoku : ruleBDoku EOF ;
    public final void entryRuleBDoku() throws RecognitionException {
        try {
            // InternalRdsPP.g:279:1: ( ruleBDoku EOF )
            // InternalRdsPP.g:280:1: ruleBDoku EOF
            {
             before(grammarAccess.getBDokuRule()); 
            pushFollow(FOLLOW_1);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBDokuRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBDoku"


    // $ANTLR start "ruleBDoku"
    // InternalRdsPP.g:287:1: ruleBDoku : ( ( rule__BDoku__Group__0 ) ) ;
    public final void ruleBDoku() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:291:2: ( ( ( rule__BDoku__Group__0 ) ) )
            // InternalRdsPP.g:292:2: ( ( rule__BDoku__Group__0 ) )
            {
            // InternalRdsPP.g:292:2: ( ( rule__BDoku__Group__0 ) )
            // InternalRdsPP.g:293:3: ( rule__BDoku__Group__0 )
            {
             before(grammarAccess.getBDokuAccess().getGroup()); 
            // InternalRdsPP.g:294:3: ( rule__BDoku__Group__0 )
            // InternalRdsPP.g:294:4: rule__BDoku__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BDoku__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBDokuAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBDoku"


    // $ANTLR start "entryRuleBSignal"
    // InternalRdsPP.g:303:1: entryRuleBSignal : ruleBSignal EOF ;
    public final void entryRuleBSignal() throws RecognitionException {
        try {
            // InternalRdsPP.g:304:1: ( ruleBSignal EOF )
            // InternalRdsPP.g:305:1: ruleBSignal EOF
            {
             before(grammarAccess.getBSignalRule()); 
            pushFollow(FOLLOW_1);
            ruleBSignal();

            state._fsp--;

             after(grammarAccess.getBSignalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBSignal"


    // $ANTLR start "ruleBSignal"
    // InternalRdsPP.g:312:1: ruleBSignal : ( ( rule__BSignal__Group__0 ) ) ;
    public final void ruleBSignal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:316:2: ( ( ( rule__BSignal__Group__0 ) ) )
            // InternalRdsPP.g:317:2: ( ( rule__BSignal__Group__0 ) )
            {
            // InternalRdsPP.g:317:2: ( ( rule__BSignal__Group__0 ) )
            // InternalRdsPP.g:318:3: ( rule__BSignal__Group__0 )
            {
             before(grammarAccess.getBSignalAccess().getGroup()); 
            // InternalRdsPP.g:319:3: ( rule__BSignal__Group__0 )
            // InternalRdsPP.g:319:4: rule__BSignal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BSignal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBSignalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBSignal"


    // $ANTLR start "rule__NA__Alternatives"
    // InternalRdsPP.g:327:1: rule__NA__Alternatives : ( ( RULE_A ) | ( RULE_N ) );
    public final void rule__NA__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:331:1: ( ( RULE_A ) | ( RULE_N ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_A) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_N) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalRdsPP.g:332:2: ( RULE_A )
                    {
                    // InternalRdsPP.g:332:2: ( RULE_A )
                    // InternalRdsPP.g:333:3: RULE_A
                    {
                     before(grammarAccess.getNAAccess().getATerminalRuleCall_0()); 
                    match(input,RULE_A,FOLLOW_2); 
                     after(grammarAccess.getNAAccess().getATerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:338:2: ( RULE_N )
                    {
                    // InternalRdsPP.g:338:2: ( RULE_N )
                    // InternalRdsPP.g:339:3: RULE_N
                    {
                     before(grammarAccess.getNAAccess().getNTerminalRuleCall_1()); 
                    match(input,RULE_N,FOLLOW_2); 
                     after(grammarAccess.getNAAccess().getNTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NA__Alternatives"


    // $ANTLR start "rule__Kennzeichnung__Alternatives"
    // InternalRdsPP.g:348:1: rule__Kennzeichnung__Alternatives : ( ( ( rule__Kennzeichnung__FunktionAssignment_0 ) ) | ( ( rule__Kennzeichnung__FunkZuordnungAssignment_1 ) ) | ( ( rule__Kennzeichnung__ProduktAssignment_2 ) ) | ( ( rule__Kennzeichnung__EinbauortAssignment_3 ) ) | ( ( rule__Kennzeichnung__AufstellungsortAssignment_4 ) ) );
    public final void rule__Kennzeichnung__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:352:1: ( ( ( rule__Kennzeichnung__FunktionAssignment_0 ) ) | ( ( rule__Kennzeichnung__FunkZuordnungAssignment_1 ) ) | ( ( rule__Kennzeichnung__ProduktAssignment_2 ) ) | ( ( rule__Kennzeichnung__EinbauortAssignment_3 ) ) | ( ( rule__Kennzeichnung__AufstellungsortAssignment_4 ) ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt3=1;
                }
                break;
            case 21:
                {
                alt3=2;
                }
                break;
            case 24:
                {
                alt3=3;
                }
                break;
            case 22:
                {
                alt3=4;
                }
                break;
            case 23:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalRdsPP.g:353:2: ( ( rule__Kennzeichnung__FunktionAssignment_0 ) )
                    {
                    // InternalRdsPP.g:353:2: ( ( rule__Kennzeichnung__FunktionAssignment_0 ) )
                    // InternalRdsPP.g:354:3: ( rule__Kennzeichnung__FunktionAssignment_0 )
                    {
                     before(grammarAccess.getKennzeichnungAccess().getFunktionAssignment_0()); 
                    // InternalRdsPP.g:355:3: ( rule__Kennzeichnung__FunktionAssignment_0 )
                    // InternalRdsPP.g:355:4: rule__Kennzeichnung__FunktionAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kennzeichnung__FunktionAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getKennzeichnungAccess().getFunktionAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:359:2: ( ( rule__Kennzeichnung__FunkZuordnungAssignment_1 ) )
                    {
                    // InternalRdsPP.g:359:2: ( ( rule__Kennzeichnung__FunkZuordnungAssignment_1 ) )
                    // InternalRdsPP.g:360:3: ( rule__Kennzeichnung__FunkZuordnungAssignment_1 )
                    {
                     before(grammarAccess.getKennzeichnungAccess().getFunkZuordnungAssignment_1()); 
                    // InternalRdsPP.g:361:3: ( rule__Kennzeichnung__FunkZuordnungAssignment_1 )
                    // InternalRdsPP.g:361:4: rule__Kennzeichnung__FunkZuordnungAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kennzeichnung__FunkZuordnungAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getKennzeichnungAccess().getFunkZuordnungAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalRdsPP.g:365:2: ( ( rule__Kennzeichnung__ProduktAssignment_2 ) )
                    {
                    // InternalRdsPP.g:365:2: ( ( rule__Kennzeichnung__ProduktAssignment_2 ) )
                    // InternalRdsPP.g:366:3: ( rule__Kennzeichnung__ProduktAssignment_2 )
                    {
                     before(grammarAccess.getKennzeichnungAccess().getProduktAssignment_2()); 
                    // InternalRdsPP.g:367:3: ( rule__Kennzeichnung__ProduktAssignment_2 )
                    // InternalRdsPP.g:367:4: rule__Kennzeichnung__ProduktAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kennzeichnung__ProduktAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getKennzeichnungAccess().getProduktAssignment_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalRdsPP.g:371:2: ( ( rule__Kennzeichnung__EinbauortAssignment_3 ) )
                    {
                    // InternalRdsPP.g:371:2: ( ( rule__Kennzeichnung__EinbauortAssignment_3 ) )
                    // InternalRdsPP.g:372:3: ( rule__Kennzeichnung__EinbauortAssignment_3 )
                    {
                     before(grammarAccess.getKennzeichnungAccess().getEinbauortAssignment_3()); 
                    // InternalRdsPP.g:373:3: ( rule__Kennzeichnung__EinbauortAssignment_3 )
                    // InternalRdsPP.g:373:4: rule__Kennzeichnung__EinbauortAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kennzeichnung__EinbauortAssignment_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getKennzeichnungAccess().getEinbauortAssignment_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalRdsPP.g:377:2: ( ( rule__Kennzeichnung__AufstellungsortAssignment_4 ) )
                    {
                    // InternalRdsPP.g:377:2: ( ( rule__Kennzeichnung__AufstellungsortAssignment_4 ) )
                    // InternalRdsPP.g:378:3: ( rule__Kennzeichnung__AufstellungsortAssignment_4 )
                    {
                     before(grammarAccess.getKennzeichnungAccess().getAufstellungsortAssignment_4()); 
                    // InternalRdsPP.g:379:3: ( rule__Kennzeichnung__AufstellungsortAssignment_4 )
                    // InternalRdsPP.g:379:4: rule__Kennzeichnung__AufstellungsortAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kennzeichnung__AufstellungsortAssignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getKennzeichnungAccess().getAufstellungsortAssignment_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__Alternatives"


    // $ANTLR start "rule__BFunktion__Alternatives_7"
    // InternalRdsPP.g:387:1: rule__BFunktion__Alternatives_7 : ( ( ( rule__BFunktion__Group_7_0__0 ) ) | ( ( rule__BFunktion__Group_7_1__0 ) ) | ( ( rule__BFunktion__Group_7_2__0 ) ) );
    public final void rule__BFunktion__Alternatives_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:391:1: ( ( ( rule__BFunktion__Group_7_0__0 ) ) | ( ( rule__BFunktion__Group_7_1__0 ) ) | ( ( rule__BFunktion__Group_7_2__0 ) ) )
            int alt4=3;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_S) ) {
                switch ( input.LA(2) ) {
                case 27:
                    {
                    alt4=2;
                    }
                    break;
                case 24:
                    {
                    alt4=1;
                    }
                    break;
                case 26:
                    {
                    alt4=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalRdsPP.g:392:2: ( ( rule__BFunktion__Group_7_0__0 ) )
                    {
                    // InternalRdsPP.g:392:2: ( ( rule__BFunktion__Group_7_0__0 ) )
                    // InternalRdsPP.g:393:3: ( rule__BFunktion__Group_7_0__0 )
                    {
                     before(grammarAccess.getBFunktionAccess().getGroup_7_0()); 
                    // InternalRdsPP.g:394:3: ( rule__BFunktion__Group_7_0__0 )
                    // InternalRdsPP.g:394:4: rule__BFunktion__Group_7_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_7_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunktionAccess().getGroup_7_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:398:2: ( ( rule__BFunktion__Group_7_1__0 ) )
                    {
                    // InternalRdsPP.g:398:2: ( ( rule__BFunktion__Group_7_1__0 ) )
                    // InternalRdsPP.g:399:3: ( rule__BFunktion__Group_7_1__0 )
                    {
                     before(grammarAccess.getBFunktionAccess().getGroup_7_1()); 
                    // InternalRdsPP.g:400:3: ( rule__BFunktion__Group_7_1__0 )
                    // InternalRdsPP.g:400:4: rule__BFunktion__Group_7_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_7_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunktionAccess().getGroup_7_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalRdsPP.g:404:2: ( ( rule__BFunktion__Group_7_2__0 ) )
                    {
                    // InternalRdsPP.g:404:2: ( ( rule__BFunktion__Group_7_2__0 ) )
                    // InternalRdsPP.g:405:3: ( rule__BFunktion__Group_7_2__0 )
                    {
                     before(grammarAccess.getBFunktionAccess().getGroup_7_2()); 
                    // InternalRdsPP.g:406:3: ( rule__BFunktion__Group_7_2__0 )
                    // InternalRdsPP.g:406:4: rule__BFunktion__Group_7_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_7_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunktionAccess().getGroup_7_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Alternatives_7"


    // $ANTLR start "rule__BFunktion__Alternatives_7_0_2"
    // InternalRdsPP.g:414:1: rule__BFunktion__Alternatives_7_0_2 : ( ( ( rule__BFunktion__Group_7_0_2_0__0 ) ) | ( ( rule__BFunktion__Group_7_0_2_1__0 ) ) );
    public final void rule__BFunktion__Alternatives_7_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:418:1: ( ( ( rule__BFunktion__Group_7_0_2_0__0 ) ) | ( ( rule__BFunktion__Group_7_0_2_1__0 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_S) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==25) ) {
                    alt5=1;
                }
                else if ( (LA5_1==26) ) {
                    alt5=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalRdsPP.g:419:2: ( ( rule__BFunktion__Group_7_0_2_0__0 ) )
                    {
                    // InternalRdsPP.g:419:2: ( ( rule__BFunktion__Group_7_0_2_0__0 ) )
                    // InternalRdsPP.g:420:3: ( rule__BFunktion__Group_7_0_2_0__0 )
                    {
                     before(grammarAccess.getBFunktionAccess().getGroup_7_0_2_0()); 
                    // InternalRdsPP.g:421:3: ( rule__BFunktion__Group_7_0_2_0__0 )
                    // InternalRdsPP.g:421:4: rule__BFunktion__Group_7_0_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_7_0_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunktionAccess().getGroup_7_0_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:425:2: ( ( rule__BFunktion__Group_7_0_2_1__0 ) )
                    {
                    // InternalRdsPP.g:425:2: ( ( rule__BFunktion__Group_7_0_2_1__0 ) )
                    // InternalRdsPP.g:426:3: ( rule__BFunktion__Group_7_0_2_1__0 )
                    {
                     before(grammarAccess.getBFunktionAccess().getGroup_7_0_2_1()); 
                    // InternalRdsPP.g:427:3: ( rule__BFunktion__Group_7_0_2_1__0 )
                    // InternalRdsPP.g:427:4: rule__BFunktion__Group_7_0_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_7_0_2_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunktionAccess().getGroup_7_0_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Alternatives_7_0_2"


    // $ANTLR start "rule__BFunkZuordnung__Alternatives_6"
    // InternalRdsPP.g:435:1: rule__BFunkZuordnung__Alternatives_6 : ( ( ( rule__BFunkZuordnung__Group_6_0__0 ) ) | ( ( rule__BFunkZuordnung__Group_6_1__0 ) ) );
    public final void rule__BFunkZuordnung__Alternatives_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:439:1: ( ( ( rule__BFunkZuordnung__Group_6_0__0 ) ) | ( ( rule__BFunkZuordnung__Group_6_1__0 ) ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_S) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==26) ) {
                    alt6=2;
                }
                else if ( (LA6_1==27) ) {
                    alt6=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalRdsPP.g:440:2: ( ( rule__BFunkZuordnung__Group_6_0__0 ) )
                    {
                    // InternalRdsPP.g:440:2: ( ( rule__BFunkZuordnung__Group_6_0__0 ) )
                    // InternalRdsPP.g:441:3: ( rule__BFunkZuordnung__Group_6_0__0 )
                    {
                     before(grammarAccess.getBFunkZuordnungAccess().getGroup_6_0()); 
                    // InternalRdsPP.g:442:3: ( rule__BFunkZuordnung__Group_6_0__0 )
                    // InternalRdsPP.g:442:4: rule__BFunkZuordnung__Group_6_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunkZuordnung__Group_6_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunkZuordnungAccess().getGroup_6_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:446:2: ( ( rule__BFunkZuordnung__Group_6_1__0 ) )
                    {
                    // InternalRdsPP.g:446:2: ( ( rule__BFunkZuordnung__Group_6_1__0 ) )
                    // InternalRdsPP.g:447:3: ( rule__BFunkZuordnung__Group_6_1__0 )
                    {
                     before(grammarAccess.getBFunkZuordnungAccess().getGroup_6_1()); 
                    // InternalRdsPP.g:448:3: ( rule__BFunkZuordnung__Group_6_1__0 )
                    // InternalRdsPP.g:448:4: rule__BFunkZuordnung__Group_6_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunkZuordnung__Group_6_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBFunkZuordnungAccess().getGroup_6_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Alternatives_6"


    // $ANTLR start "rule__BProdukt__ProduktAlternatives_1_0"
    // InternalRdsPP.g:456:1: rule__BProdukt__ProduktAlternatives_1_0 : ( ( RULE_AANN ) | ( RULE_AANNN ) );
    public final void rule__BProdukt__ProduktAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:460:1: ( ( RULE_AANN ) | ( RULE_AANNN ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_AANN) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_AANNN) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalRdsPP.g:461:2: ( RULE_AANN )
                    {
                    // InternalRdsPP.g:461:2: ( RULE_AANN )
                    // InternalRdsPP.g:462:3: RULE_AANN
                    {
                     before(grammarAccess.getBProduktAccess().getProduktAANNTerminalRuleCall_1_0_0()); 
                    match(input,RULE_AANN,FOLLOW_2); 
                     after(grammarAccess.getBProduktAccess().getProduktAANNTerminalRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:467:2: ( RULE_AANNN )
                    {
                    // InternalRdsPP.g:467:2: ( RULE_AANNN )
                    // InternalRdsPP.g:468:3: RULE_AANNN
                    {
                     before(grammarAccess.getBProduktAccess().getProduktAANNNTerminalRuleCall_1_0_1()); 
                    match(input,RULE_AANNN,FOLLOW_2); 
                     after(grammarAccess.getBProduktAccess().getProduktAANNNTerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__ProduktAlternatives_1_0"


    // $ANTLR start "rule__BSignal__KlassifizierungAlternatives_1_0"
    // InternalRdsPP.g:477:1: rule__BSignal__KlassifizierungAlternatives_1_0 : ( ( RULE_AANN ) | ( RULE_AANNN ) );
    public final void rule__BSignal__KlassifizierungAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:481:1: ( ( RULE_AANN ) | ( RULE_AANNN ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_AANN) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_AANNN) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalRdsPP.g:482:2: ( RULE_AANN )
                    {
                    // InternalRdsPP.g:482:2: ( RULE_AANN )
                    // InternalRdsPP.g:483:3: RULE_AANN
                    {
                     before(grammarAccess.getBSignalAccess().getKlassifizierungAANNTerminalRuleCall_1_0_0()); 
                    match(input,RULE_AANN,FOLLOW_2); 
                     after(grammarAccess.getBSignalAccess().getKlassifizierungAANNTerminalRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRdsPP.g:488:2: ( RULE_AANNN )
                    {
                    // InternalRdsPP.g:488:2: ( RULE_AANNN )
                    // InternalRdsPP.g:489:3: RULE_AANNN
                    {
                     before(grammarAccess.getBSignalAccess().getKlassifizierungAANNNTerminalRuleCall_1_0_1()); 
                    match(input,RULE_AANNN,FOLLOW_2); 
                     after(grammarAccess.getBSignalAccess().getKlassifizierungAANNNTerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__KlassifizierungAlternatives_1_0"


    // $ANTLR start "rule__BFunktion__Group__0"
    // InternalRdsPP.g:498:1: rule__BFunktion__Group__0 : rule__BFunktion__Group__0__Impl rule__BFunktion__Group__1 ;
    public final void rule__BFunktion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:502:1: ( rule__BFunktion__Group__0__Impl rule__BFunktion__Group__1 )
            // InternalRdsPP.g:503:2: rule__BFunktion__Group__0__Impl rule__BFunktion__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__BFunktion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__0"


    // $ANTLR start "rule__BFunktion__Group__0__Impl"
    // InternalRdsPP.g:510:1: rule__BFunktion__Group__0__Impl : ( '=' ) ;
    public final void rule__BFunktion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:514:1: ( ( '=' ) )
            // InternalRdsPP.g:515:1: ( '=' )
            {
            // InternalRdsPP.g:515:1: ( '=' )
            // InternalRdsPP.g:516:2: '='
            {
             before(grammarAccess.getBFunktionAccess().getEqualsSignKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getEqualsSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__0__Impl"


    // $ANTLR start "rule__BFunktion__Group__1"
    // InternalRdsPP.g:525:1: rule__BFunktion__Group__1 : rule__BFunktion__Group__1__Impl rule__BFunktion__Group__2 ;
    public final void rule__BFunktion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:529:1: ( rule__BFunktion__Group__1__Impl rule__BFunktion__Group__2 )
            // InternalRdsPP.g:530:2: rule__BFunktion__Group__1__Impl rule__BFunktion__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__BFunktion__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__1"


    // $ANTLR start "rule__BFunktion__Group__1__Impl"
    // InternalRdsPP.g:537:1: rule__BFunktion__Group__1__Impl : ( ( rule__BFunktion__HauptsystemAssignment_1 ) ) ;
    public final void rule__BFunktion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:541:1: ( ( ( rule__BFunktion__HauptsystemAssignment_1 ) ) )
            // InternalRdsPP.g:542:1: ( ( rule__BFunktion__HauptsystemAssignment_1 ) )
            {
            // InternalRdsPP.g:542:1: ( ( rule__BFunktion__HauptsystemAssignment_1 ) )
            // InternalRdsPP.g:543:2: ( rule__BFunktion__HauptsystemAssignment_1 )
            {
             before(grammarAccess.getBFunktionAccess().getHauptsystemAssignment_1()); 
            // InternalRdsPP.g:544:2: ( rule__BFunktion__HauptsystemAssignment_1 )
            // InternalRdsPP.g:544:3: rule__BFunktion__HauptsystemAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__HauptsystemAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getHauptsystemAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__1__Impl"


    // $ANTLR start "rule__BFunktion__Group__2"
    // InternalRdsPP.g:552:1: rule__BFunktion__Group__2 : rule__BFunktion__Group__2__Impl rule__BFunktion__Group__3 ;
    public final void rule__BFunktion__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:556:1: ( rule__BFunktion__Group__2__Impl rule__BFunktion__Group__3 )
            // InternalRdsPP.g:557:2: rule__BFunktion__Group__2__Impl rule__BFunktion__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__BFunktion__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__2"


    // $ANTLR start "rule__BFunktion__Group__2__Impl"
    // InternalRdsPP.g:564:1: rule__BFunktion__Group__2__Impl : ( ( RULE_S )? ) ;
    public final void rule__BFunktion__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:568:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:569:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:569:1: ( ( RULE_S )? )
            // InternalRdsPP.g:570:2: ( RULE_S )?
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_2()); 
            // InternalRdsPP.g:571:2: ( RULE_S )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_S) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalRdsPP.g:571:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__2__Impl"


    // $ANTLR start "rule__BFunktion__Group__3"
    // InternalRdsPP.g:579:1: rule__BFunktion__Group__3 : rule__BFunktion__Group__3__Impl rule__BFunktion__Group__4 ;
    public final void rule__BFunktion__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:583:1: ( rule__BFunktion__Group__3__Impl rule__BFunktion__Group__4 )
            // InternalRdsPP.g:584:2: rule__BFunktion__Group__3__Impl rule__BFunktion__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__BFunktion__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__3"


    // $ANTLR start "rule__BFunktion__Group__3__Impl"
    // InternalRdsPP.g:591:1: rule__BFunktion__Group__3__Impl : ( ( rule__BFunktion__Abschnitt1Assignment_3 ) ) ;
    public final void rule__BFunktion__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:595:1: ( ( ( rule__BFunktion__Abschnitt1Assignment_3 ) ) )
            // InternalRdsPP.g:596:1: ( ( rule__BFunktion__Abschnitt1Assignment_3 ) )
            {
            // InternalRdsPP.g:596:1: ( ( rule__BFunktion__Abschnitt1Assignment_3 ) )
            // InternalRdsPP.g:597:2: ( rule__BFunktion__Abschnitt1Assignment_3 )
            {
             before(grammarAccess.getBFunktionAccess().getAbschnitt1Assignment_3()); 
            // InternalRdsPP.g:598:2: ( rule__BFunktion__Abschnitt1Assignment_3 )
            // InternalRdsPP.g:598:3: rule__BFunktion__Abschnitt1Assignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Abschnitt1Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getAbschnitt1Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__3__Impl"


    // $ANTLR start "rule__BFunktion__Group__4"
    // InternalRdsPP.g:606:1: rule__BFunktion__Group__4 : rule__BFunktion__Group__4__Impl rule__BFunktion__Group__5 ;
    public final void rule__BFunktion__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:610:1: ( rule__BFunktion__Group__4__Impl rule__BFunktion__Group__5 )
            // InternalRdsPP.g:611:2: rule__BFunktion__Group__4__Impl rule__BFunktion__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__BFunktion__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__4"


    // $ANTLR start "rule__BFunktion__Group__4__Impl"
    // InternalRdsPP.g:618:1: rule__BFunktion__Group__4__Impl : ( ( RULE_S )? ) ;
    public final void rule__BFunktion__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:622:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:623:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:623:1: ( ( RULE_S )? )
            // InternalRdsPP.g:624:2: ( RULE_S )?
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_4()); 
            // InternalRdsPP.g:625:2: ( RULE_S )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_S) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalRdsPP.g:625:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__4__Impl"


    // $ANTLR start "rule__BFunktion__Group__5"
    // InternalRdsPP.g:633:1: rule__BFunktion__Group__5 : rule__BFunktion__Group__5__Impl rule__BFunktion__Group__6 ;
    public final void rule__BFunktion__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:637:1: ( rule__BFunktion__Group__5__Impl rule__BFunktion__Group__6 )
            // InternalRdsPP.g:638:2: rule__BFunktion__Group__5__Impl rule__BFunktion__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__BFunktion__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__5"


    // $ANTLR start "rule__BFunktion__Group__5__Impl"
    // InternalRdsPP.g:645:1: rule__BFunktion__Group__5__Impl : ( ( rule__BFunktion__Abschnitt2Assignment_5 ) ) ;
    public final void rule__BFunktion__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:649:1: ( ( ( rule__BFunktion__Abschnitt2Assignment_5 ) ) )
            // InternalRdsPP.g:650:1: ( ( rule__BFunktion__Abschnitt2Assignment_5 ) )
            {
            // InternalRdsPP.g:650:1: ( ( rule__BFunktion__Abschnitt2Assignment_5 ) )
            // InternalRdsPP.g:651:2: ( rule__BFunktion__Abschnitt2Assignment_5 )
            {
             before(grammarAccess.getBFunktionAccess().getAbschnitt2Assignment_5()); 
            // InternalRdsPP.g:652:2: ( rule__BFunktion__Abschnitt2Assignment_5 )
            // InternalRdsPP.g:652:3: rule__BFunktion__Abschnitt2Assignment_5
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Abschnitt2Assignment_5();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getAbschnitt2Assignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__5__Impl"


    // $ANTLR start "rule__BFunktion__Group__6"
    // InternalRdsPP.g:660:1: rule__BFunktion__Group__6 : rule__BFunktion__Group__6__Impl rule__BFunktion__Group__7 ;
    public final void rule__BFunktion__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:664:1: ( rule__BFunktion__Group__6__Impl rule__BFunktion__Group__7 )
            // InternalRdsPP.g:665:2: rule__BFunktion__Group__6__Impl rule__BFunktion__Group__7
            {
            pushFollow(FOLLOW_7);
            rule__BFunktion__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__6"


    // $ANTLR start "rule__BFunktion__Group__6__Impl"
    // InternalRdsPP.g:672:1: rule__BFunktion__Group__6__Impl : ( ( rule__BFunktion__Group_6__0 )? ) ;
    public final void rule__BFunktion__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:676:1: ( ( ( rule__BFunktion__Group_6__0 )? ) )
            // InternalRdsPP.g:677:1: ( ( rule__BFunktion__Group_6__0 )? )
            {
            // InternalRdsPP.g:677:1: ( ( rule__BFunktion__Group_6__0 )? )
            // InternalRdsPP.g:678:2: ( rule__BFunktion__Group_6__0 )?
            {
             before(grammarAccess.getBFunktionAccess().getGroup_6()); 
            // InternalRdsPP.g:679:2: ( rule__BFunktion__Group_6__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==20) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalRdsPP.g:679:3: rule__BFunktion__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBFunktionAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__6__Impl"


    // $ANTLR start "rule__BFunktion__Group__7"
    // InternalRdsPP.g:687:1: rule__BFunktion__Group__7 : rule__BFunktion__Group__7__Impl ;
    public final void rule__BFunktion__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:691:1: ( rule__BFunktion__Group__7__Impl )
            // InternalRdsPP.g:692:2: rule__BFunktion__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__7"


    // $ANTLR start "rule__BFunktion__Group__7__Impl"
    // InternalRdsPP.g:698:1: rule__BFunktion__Group__7__Impl : ( ( rule__BFunktion__Alternatives_7 )? ) ;
    public final void rule__BFunktion__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:702:1: ( ( ( rule__BFunktion__Alternatives_7 )? ) )
            // InternalRdsPP.g:703:1: ( ( rule__BFunktion__Alternatives_7 )? )
            {
            // InternalRdsPP.g:703:1: ( ( rule__BFunktion__Alternatives_7 )? )
            // InternalRdsPP.g:704:2: ( rule__BFunktion__Alternatives_7 )?
            {
             before(grammarAccess.getBFunktionAccess().getAlternatives_7()); 
            // InternalRdsPP.g:705:2: ( rule__BFunktion__Alternatives_7 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_S) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalRdsPP.g:705:3: rule__BFunktion__Alternatives_7
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Alternatives_7();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBFunktionAccess().getAlternatives_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group__7__Impl"


    // $ANTLR start "rule__BFunktion__Group_6__0"
    // InternalRdsPP.g:714:1: rule__BFunktion__Group_6__0 : rule__BFunktion__Group_6__0__Impl rule__BFunktion__Group_6__1 ;
    public final void rule__BFunktion__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:718:1: ( rule__BFunktion__Group_6__0__Impl rule__BFunktion__Group_6__1 )
            // InternalRdsPP.g:719:2: rule__BFunktion__Group_6__0__Impl rule__BFunktion__Group_6__1
            {
            pushFollow(FOLLOW_8);
            rule__BFunktion__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_6__0"


    // $ANTLR start "rule__BFunktion__Group_6__0__Impl"
    // InternalRdsPP.g:726:1: rule__BFunktion__Group_6__0__Impl : ( '.' ) ;
    public final void rule__BFunktion__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:730:1: ( ( '.' ) )
            // InternalRdsPP.g:731:1: ( '.' )
            {
            // InternalRdsPP.g:731:1: ( '.' )
            // InternalRdsPP.g:732:2: '.'
            {
             before(grammarAccess.getBFunktionAccess().getFullStopKeyword_6_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getFullStopKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_6__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_6__1"
    // InternalRdsPP.g:741:1: rule__BFunktion__Group_6__1 : rule__BFunktion__Group_6__1__Impl ;
    public final void rule__BFunktion__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:745:1: ( rule__BFunktion__Group_6__1__Impl )
            // InternalRdsPP.g:746:2: rule__BFunktion__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_6__1"


    // $ANTLR start "rule__BFunktion__Group_6__1__Impl"
    // InternalRdsPP.g:752:1: rule__BFunktion__Group_6__1__Impl : ( ( rule__BFunktion__LeitfunktionAssignment_6_1 ) ) ;
    public final void rule__BFunktion__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:756:1: ( ( ( rule__BFunktion__LeitfunktionAssignment_6_1 ) ) )
            // InternalRdsPP.g:757:1: ( ( rule__BFunktion__LeitfunktionAssignment_6_1 ) )
            {
            // InternalRdsPP.g:757:1: ( ( rule__BFunktion__LeitfunktionAssignment_6_1 ) )
            // InternalRdsPP.g:758:2: ( rule__BFunktion__LeitfunktionAssignment_6_1 )
            {
             before(grammarAccess.getBFunktionAccess().getLeitfunktionAssignment_6_1()); 
            // InternalRdsPP.g:759:2: ( rule__BFunktion__LeitfunktionAssignment_6_1 )
            // InternalRdsPP.g:759:3: rule__BFunktion__LeitfunktionAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__LeitfunktionAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getLeitfunktionAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_6__1__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0__0"
    // InternalRdsPP.g:768:1: rule__BFunktion__Group_7_0__0 : rule__BFunktion__Group_7_0__0__Impl rule__BFunktion__Group_7_0__1 ;
    public final void rule__BFunktion__Group_7_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:772:1: ( rule__BFunktion__Group_7_0__0__Impl rule__BFunktion__Group_7_0__1 )
            // InternalRdsPP.g:773:2: rule__BFunktion__Group_7_0__0__Impl rule__BFunktion__Group_7_0__1
            {
            pushFollow(FOLLOW_9);
            rule__BFunktion__Group_7_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__0"


    // $ANTLR start "rule__BFunktion__Group_7_0__0__Impl"
    // InternalRdsPP.g:780:1: rule__BFunktion__Group_7_0__0__Impl : ( RULE_S ) ;
    public final void rule__BFunktion__Group_7_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:784:1: ( ( RULE_S ) )
            // InternalRdsPP.g:785:1: ( RULE_S )
            {
            // InternalRdsPP.g:785:1: ( RULE_S )
            // InternalRdsPP.g:786:2: RULE_S
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0__1"
    // InternalRdsPP.g:795:1: rule__BFunktion__Group_7_0__1 : rule__BFunktion__Group_7_0__1__Impl rule__BFunktion__Group_7_0__2 ;
    public final void rule__BFunktion__Group_7_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:799:1: ( rule__BFunktion__Group_7_0__1__Impl rule__BFunktion__Group_7_0__2 )
            // InternalRdsPP.g:800:2: rule__BFunktion__Group_7_0__1__Impl rule__BFunktion__Group_7_0__2
            {
            pushFollow(FOLLOW_10);
            rule__BFunktion__Group_7_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__1"


    // $ANTLR start "rule__BFunktion__Group_7_0__1__Impl"
    // InternalRdsPP.g:807:1: rule__BFunktion__Group_7_0__1__Impl : ( ( rule__BFunktion__ProduktAssignment_7_0_1 ) ) ;
    public final void rule__BFunktion__Group_7_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:811:1: ( ( ( rule__BFunktion__ProduktAssignment_7_0_1 ) ) )
            // InternalRdsPP.g:812:1: ( ( rule__BFunktion__ProduktAssignment_7_0_1 ) )
            {
            // InternalRdsPP.g:812:1: ( ( rule__BFunktion__ProduktAssignment_7_0_1 ) )
            // InternalRdsPP.g:813:2: ( rule__BFunktion__ProduktAssignment_7_0_1 )
            {
             before(grammarAccess.getBFunktionAccess().getProduktAssignment_7_0_1()); 
            // InternalRdsPP.g:814:2: ( rule__BFunktion__ProduktAssignment_7_0_1 )
            // InternalRdsPP.g:814:3: rule__BFunktion__ProduktAssignment_7_0_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__ProduktAssignment_7_0_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getProduktAssignment_7_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__1__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0__2"
    // InternalRdsPP.g:822:1: rule__BFunktion__Group_7_0__2 : rule__BFunktion__Group_7_0__2__Impl ;
    public final void rule__BFunktion__Group_7_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:826:1: ( rule__BFunktion__Group_7_0__2__Impl )
            // InternalRdsPP.g:827:2: rule__BFunktion__Group_7_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__2"


    // $ANTLR start "rule__BFunktion__Group_7_0__2__Impl"
    // InternalRdsPP.g:833:1: rule__BFunktion__Group_7_0__2__Impl : ( ( rule__BFunktion__Alternatives_7_0_2 )? ) ;
    public final void rule__BFunktion__Group_7_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:837:1: ( ( ( rule__BFunktion__Alternatives_7_0_2 )? ) )
            // InternalRdsPP.g:838:1: ( ( rule__BFunktion__Alternatives_7_0_2 )? )
            {
            // InternalRdsPP.g:838:1: ( ( rule__BFunktion__Alternatives_7_0_2 )? )
            // InternalRdsPP.g:839:2: ( rule__BFunktion__Alternatives_7_0_2 )?
            {
             before(grammarAccess.getBFunktionAccess().getAlternatives_7_0_2()); 
            // InternalRdsPP.g:840:2: ( rule__BFunktion__Alternatives_7_0_2 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_S) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRdsPP.g:840:3: rule__BFunktion__Alternatives_7_0_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunktion__Alternatives_7_0_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBFunktionAccess().getAlternatives_7_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0__2__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_0__0"
    // InternalRdsPP.g:849:1: rule__BFunktion__Group_7_0_2_0__0 : rule__BFunktion__Group_7_0_2_0__0__Impl rule__BFunktion__Group_7_0_2_0__1 ;
    public final void rule__BFunktion__Group_7_0_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:853:1: ( rule__BFunktion__Group_7_0_2_0__0__Impl rule__BFunktion__Group_7_0_2_0__1 )
            // InternalRdsPP.g:854:2: rule__BFunktion__Group_7_0_2_0__0__Impl rule__BFunktion__Group_7_0_2_0__1
            {
            pushFollow(FOLLOW_11);
            rule__BFunktion__Group_7_0_2_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_0__0"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_0__0__Impl"
    // InternalRdsPP.g:861:1: rule__BFunktion__Group_7_0_2_0__0__Impl : ( RULE_S ) ;
    public final void rule__BFunktion__Group_7_0_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:865:1: ( ( RULE_S ) )
            // InternalRdsPP.g:866:1: ( RULE_S )
            {
            // InternalRdsPP.g:866:1: ( RULE_S )
            // InternalRdsPP.g:867:2: RULE_S
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_0_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_0__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_0__1"
    // InternalRdsPP.g:876:1: rule__BFunktion__Group_7_0_2_0__1 : rule__BFunktion__Group_7_0_2_0__1__Impl ;
    public final void rule__BFunktion__Group_7_0_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:880:1: ( rule__BFunktion__Group_7_0_2_0__1__Impl )
            // InternalRdsPP.g:881:2: rule__BFunktion__Group_7_0_2_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0_2_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_0__1"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_0__1__Impl"
    // InternalRdsPP.g:887:1: rule__BFunktion__Group_7_0_2_0__1__Impl : ( ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 ) ) ;
    public final void rule__BFunktion__Group_7_0_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:891:1: ( ( ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 ) ) )
            // InternalRdsPP.g:892:1: ( ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 ) )
            {
            // InternalRdsPP.g:892:1: ( ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 ) )
            // InternalRdsPP.g:893:2: ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 )
            {
             before(grammarAccess.getBFunktionAccess().getAnschlussAssignment_7_0_2_0_1()); 
            // InternalRdsPP.g:894:2: ( rule__BFunktion__AnschlussAssignment_7_0_2_0_1 )
            // InternalRdsPP.g:894:3: rule__BFunktion__AnschlussAssignment_7_0_2_0_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__AnschlussAssignment_7_0_2_0_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getAnschlussAssignment_7_0_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_0__1__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_1__0"
    // InternalRdsPP.g:903:1: rule__BFunktion__Group_7_0_2_1__0 : rule__BFunktion__Group_7_0_2_1__0__Impl rule__BFunktion__Group_7_0_2_1__1 ;
    public final void rule__BFunktion__Group_7_0_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:907:1: ( rule__BFunktion__Group_7_0_2_1__0__Impl rule__BFunktion__Group_7_0_2_1__1 )
            // InternalRdsPP.g:908:2: rule__BFunktion__Group_7_0_2_1__0__Impl rule__BFunktion__Group_7_0_2_1__1
            {
            pushFollow(FOLLOW_12);
            rule__BFunktion__Group_7_0_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_1__0"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_1__0__Impl"
    // InternalRdsPP.g:915:1: rule__BFunktion__Group_7_0_2_1__0__Impl : ( RULE_S ) ;
    public final void rule__BFunktion__Group_7_0_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:919:1: ( ( RULE_S ) )
            // InternalRdsPP.g:920:1: ( RULE_S )
            {
            // InternalRdsPP.g:920:1: ( RULE_S )
            // InternalRdsPP.g:921:2: RULE_S
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_1_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_0_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_1__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_1__1"
    // InternalRdsPP.g:930:1: rule__BFunktion__Group_7_0_2_1__1 : rule__BFunktion__Group_7_0_2_1__1__Impl ;
    public final void rule__BFunktion__Group_7_0_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:934:1: ( rule__BFunktion__Group_7_0_2_1__1__Impl )
            // InternalRdsPP.g:935:2: rule__BFunktion__Group_7_0_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_0_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_1__1"


    // $ANTLR start "rule__BFunktion__Group_7_0_2_1__1__Impl"
    // InternalRdsPP.g:941:1: rule__BFunktion__Group_7_0_2_1__1__Impl : ( ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 ) ) ;
    public final void rule__BFunktion__Group_7_0_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:945:1: ( ( ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 ) ) )
            // InternalRdsPP.g:946:1: ( ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 ) )
            {
            // InternalRdsPP.g:946:1: ( ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 ) )
            // InternalRdsPP.g:947:2: ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 )
            {
             before(grammarAccess.getBFunktionAccess().getDokumentationAssignment_7_0_2_1_1()); 
            // InternalRdsPP.g:948:2: ( rule__BFunktion__DokumentationAssignment_7_0_2_1_1 )
            // InternalRdsPP.g:948:3: rule__BFunktion__DokumentationAssignment_7_0_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__DokumentationAssignment_7_0_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getDokumentationAssignment_7_0_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_0_2_1__1__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_1__0"
    // InternalRdsPP.g:957:1: rule__BFunktion__Group_7_1__0 : rule__BFunktion__Group_7_1__0__Impl rule__BFunktion__Group_7_1__1 ;
    public final void rule__BFunktion__Group_7_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:961:1: ( rule__BFunktion__Group_7_1__0__Impl rule__BFunktion__Group_7_1__1 )
            // InternalRdsPP.g:962:2: rule__BFunktion__Group_7_1__0__Impl rule__BFunktion__Group_7_1__1
            {
            pushFollow(FOLLOW_13);
            rule__BFunktion__Group_7_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_1__0"


    // $ANTLR start "rule__BFunktion__Group_7_1__0__Impl"
    // InternalRdsPP.g:969:1: rule__BFunktion__Group_7_1__0__Impl : ( RULE_S ) ;
    public final void rule__BFunktion__Group_7_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:973:1: ( ( RULE_S ) )
            // InternalRdsPP.g:974:1: ( RULE_S )
            {
            // InternalRdsPP.g:974:1: ( RULE_S )
            // InternalRdsPP.g:975:2: RULE_S
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_1_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_1__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_1__1"
    // InternalRdsPP.g:984:1: rule__BFunktion__Group_7_1__1 : rule__BFunktion__Group_7_1__1__Impl ;
    public final void rule__BFunktion__Group_7_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:988:1: ( rule__BFunktion__Group_7_1__1__Impl )
            // InternalRdsPP.g:989:2: rule__BFunktion__Group_7_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_1__1"


    // $ANTLR start "rule__BFunktion__Group_7_1__1__Impl"
    // InternalRdsPP.g:995:1: rule__BFunktion__Group_7_1__1__Impl : ( ( rule__BFunktion__SignalAssignment_7_1_1 ) ) ;
    public final void rule__BFunktion__Group_7_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:999:1: ( ( ( rule__BFunktion__SignalAssignment_7_1_1 ) ) )
            // InternalRdsPP.g:1000:1: ( ( rule__BFunktion__SignalAssignment_7_1_1 ) )
            {
            // InternalRdsPP.g:1000:1: ( ( rule__BFunktion__SignalAssignment_7_1_1 ) )
            // InternalRdsPP.g:1001:2: ( rule__BFunktion__SignalAssignment_7_1_1 )
            {
             before(grammarAccess.getBFunktionAccess().getSignalAssignment_7_1_1()); 
            // InternalRdsPP.g:1002:2: ( rule__BFunktion__SignalAssignment_7_1_1 )
            // InternalRdsPP.g:1002:3: rule__BFunktion__SignalAssignment_7_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__SignalAssignment_7_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getSignalAssignment_7_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_1__1__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_2__0"
    // InternalRdsPP.g:1011:1: rule__BFunktion__Group_7_2__0 : rule__BFunktion__Group_7_2__0__Impl rule__BFunktion__Group_7_2__1 ;
    public final void rule__BFunktion__Group_7_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1015:1: ( rule__BFunktion__Group_7_2__0__Impl rule__BFunktion__Group_7_2__1 )
            // InternalRdsPP.g:1016:2: rule__BFunktion__Group_7_2__0__Impl rule__BFunktion__Group_7_2__1
            {
            pushFollow(FOLLOW_12);
            rule__BFunktion__Group_7_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_2__0"


    // $ANTLR start "rule__BFunktion__Group_7_2__0__Impl"
    // InternalRdsPP.g:1023:1: rule__BFunktion__Group_7_2__0__Impl : ( RULE_S ) ;
    public final void rule__BFunktion__Group_7_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1027:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1028:1: ( RULE_S )
            {
            // InternalRdsPP.g:1028:1: ( RULE_S )
            // InternalRdsPP.g:1029:2: RULE_S
            {
             before(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_2_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getSTerminalRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_2__0__Impl"


    // $ANTLR start "rule__BFunktion__Group_7_2__1"
    // InternalRdsPP.g:1038:1: rule__BFunktion__Group_7_2__1 : rule__BFunktion__Group_7_2__1__Impl ;
    public final void rule__BFunktion__Group_7_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1042:1: ( rule__BFunktion__Group_7_2__1__Impl )
            // InternalRdsPP.g:1043:2: rule__BFunktion__Group_7_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__Group_7_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_2__1"


    // $ANTLR start "rule__BFunktion__Group_7_2__1__Impl"
    // InternalRdsPP.g:1049:1: rule__BFunktion__Group_7_2__1__Impl : ( ( rule__BFunktion__DokumentationAssignment_7_2_1 ) ) ;
    public final void rule__BFunktion__Group_7_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1053:1: ( ( ( rule__BFunktion__DokumentationAssignment_7_2_1 ) ) )
            // InternalRdsPP.g:1054:1: ( ( rule__BFunktion__DokumentationAssignment_7_2_1 ) )
            {
            // InternalRdsPP.g:1054:1: ( ( rule__BFunktion__DokumentationAssignment_7_2_1 ) )
            // InternalRdsPP.g:1055:2: ( rule__BFunktion__DokumentationAssignment_7_2_1 )
            {
             before(grammarAccess.getBFunktionAccess().getDokumentationAssignment_7_2_1()); 
            // InternalRdsPP.g:1056:2: ( rule__BFunktion__DokumentationAssignment_7_2_1 )
            // InternalRdsPP.g:1056:3: rule__BFunktion__DokumentationAssignment_7_2_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunktion__DokumentationAssignment_7_2_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunktionAccess().getDokumentationAssignment_7_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Group_7_2__1__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__0"
    // InternalRdsPP.g:1065:1: rule__BFunkZuordnung__Group__0 : rule__BFunkZuordnung__Group__0__Impl rule__BFunkZuordnung__Group__1 ;
    public final void rule__BFunkZuordnung__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1069:1: ( rule__BFunkZuordnung__Group__0__Impl rule__BFunkZuordnung__Group__1 )
            // InternalRdsPP.g:1070:2: rule__BFunkZuordnung__Group__0__Impl rule__BFunkZuordnung__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__BFunkZuordnung__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__0"


    // $ANTLR start "rule__BFunkZuordnung__Group__0__Impl"
    // InternalRdsPP.g:1077:1: rule__BFunkZuordnung__Group__0__Impl : ( '==' ) ;
    public final void rule__BFunkZuordnung__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1081:1: ( ( '==' ) )
            // InternalRdsPP.g:1082:1: ( '==' )
            {
            // InternalRdsPP.g:1082:1: ( '==' )
            // InternalRdsPP.g:1083:2: '=='
            {
             before(grammarAccess.getBFunkZuordnungAccess().getEqualsSignEqualsSignKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getEqualsSignEqualsSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__0__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__1"
    // InternalRdsPP.g:1092:1: rule__BFunkZuordnung__Group__1 : rule__BFunkZuordnung__Group__1__Impl rule__BFunkZuordnung__Group__2 ;
    public final void rule__BFunkZuordnung__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1096:1: ( rule__BFunkZuordnung__Group__1__Impl rule__BFunkZuordnung__Group__2 )
            // InternalRdsPP.g:1097:2: rule__BFunkZuordnung__Group__1__Impl rule__BFunkZuordnung__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__BFunkZuordnung__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__1"


    // $ANTLR start "rule__BFunkZuordnung__Group__1__Impl"
    // InternalRdsPP.g:1104:1: rule__BFunkZuordnung__Group__1__Impl : ( ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 ) ) ;
    public final void rule__BFunkZuordnung__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1108:1: ( ( ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 ) ) )
            // InternalRdsPP.g:1109:1: ( ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 ) )
            {
            // InternalRdsPP.g:1109:1: ( ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 ) )
            // InternalRdsPP.g:1110:2: ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getFunktionsbereicheAssignment_1()); 
            // InternalRdsPP.g:1111:2: ( rule__BFunkZuordnung__FunktionsbereicheAssignment_1 )
            // InternalRdsPP.g:1111:3: rule__BFunkZuordnung__FunktionsbereicheAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__FunktionsbereicheAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getFunktionsbereicheAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__1__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__2"
    // InternalRdsPP.g:1119:1: rule__BFunkZuordnung__Group__2 : rule__BFunkZuordnung__Group__2__Impl rule__BFunkZuordnung__Group__3 ;
    public final void rule__BFunkZuordnung__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1123:1: ( rule__BFunkZuordnung__Group__2__Impl rule__BFunkZuordnung__Group__3 )
            // InternalRdsPP.g:1124:2: rule__BFunkZuordnung__Group__2__Impl rule__BFunkZuordnung__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__BFunkZuordnung__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__2"


    // $ANTLR start "rule__BFunkZuordnung__Group__2__Impl"
    // InternalRdsPP.g:1131:1: rule__BFunkZuordnung__Group__2__Impl : ( ( RULE_S )? ) ;
    public final void rule__BFunkZuordnung__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1135:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:1136:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:1136:1: ( ( RULE_S )? )
            // InternalRdsPP.g:1137:2: ( RULE_S )?
            {
             before(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_2()); 
            // InternalRdsPP.g:1138:2: ( RULE_S )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_S) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalRdsPP.g:1138:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__2__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__3"
    // InternalRdsPP.g:1146:1: rule__BFunkZuordnung__Group__3 : rule__BFunkZuordnung__Group__3__Impl rule__BFunkZuordnung__Group__4 ;
    public final void rule__BFunkZuordnung__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1150:1: ( rule__BFunkZuordnung__Group__3__Impl rule__BFunkZuordnung__Group__4 )
            // InternalRdsPP.g:1151:2: rule__BFunkZuordnung__Group__3__Impl rule__BFunkZuordnung__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__BFunkZuordnung__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__3"


    // $ANTLR start "rule__BFunkZuordnung__Group__3__Impl"
    // InternalRdsPP.g:1158:1: rule__BFunkZuordnung__Group__3__Impl : ( ( rule__BFunkZuordnung__Abschnitt1Assignment_3 ) ) ;
    public final void rule__BFunkZuordnung__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1162:1: ( ( ( rule__BFunkZuordnung__Abschnitt1Assignment_3 ) ) )
            // InternalRdsPP.g:1163:1: ( ( rule__BFunkZuordnung__Abschnitt1Assignment_3 ) )
            {
            // InternalRdsPP.g:1163:1: ( ( rule__BFunkZuordnung__Abschnitt1Assignment_3 ) )
            // InternalRdsPP.g:1164:2: ( rule__BFunkZuordnung__Abschnitt1Assignment_3 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getAbschnitt1Assignment_3()); 
            // InternalRdsPP.g:1165:2: ( rule__BFunkZuordnung__Abschnitt1Assignment_3 )
            // InternalRdsPP.g:1165:3: rule__BFunkZuordnung__Abschnitt1Assignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Abschnitt1Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getAbschnitt1Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__3__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__4"
    // InternalRdsPP.g:1173:1: rule__BFunkZuordnung__Group__4 : rule__BFunkZuordnung__Group__4__Impl rule__BFunkZuordnung__Group__5 ;
    public final void rule__BFunkZuordnung__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1177:1: ( rule__BFunkZuordnung__Group__4__Impl rule__BFunkZuordnung__Group__5 )
            // InternalRdsPP.g:1178:2: rule__BFunkZuordnung__Group__4__Impl rule__BFunkZuordnung__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__BFunkZuordnung__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__4"


    // $ANTLR start "rule__BFunkZuordnung__Group__4__Impl"
    // InternalRdsPP.g:1185:1: rule__BFunkZuordnung__Group__4__Impl : ( '.' ) ;
    public final void rule__BFunkZuordnung__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1189:1: ( ( '.' ) )
            // InternalRdsPP.g:1190:1: ( '.' )
            {
            // InternalRdsPP.g:1190:1: ( '.' )
            // InternalRdsPP.g:1191:2: '.'
            {
             before(grammarAccess.getBFunkZuordnungAccess().getFullStopKeyword_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getFullStopKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__4__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__5"
    // InternalRdsPP.g:1200:1: rule__BFunkZuordnung__Group__5 : rule__BFunkZuordnung__Group__5__Impl rule__BFunkZuordnung__Group__6 ;
    public final void rule__BFunkZuordnung__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1204:1: ( rule__BFunkZuordnung__Group__5__Impl rule__BFunkZuordnung__Group__6 )
            // InternalRdsPP.g:1205:2: rule__BFunkZuordnung__Group__5__Impl rule__BFunkZuordnung__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__BFunkZuordnung__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__5"


    // $ANTLR start "rule__BFunkZuordnung__Group__5__Impl"
    // InternalRdsPP.g:1212:1: rule__BFunkZuordnung__Group__5__Impl : ( ( rule__BFunkZuordnung__LeitfunktionAssignment_5 ) ) ;
    public final void rule__BFunkZuordnung__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1216:1: ( ( ( rule__BFunkZuordnung__LeitfunktionAssignment_5 ) ) )
            // InternalRdsPP.g:1217:1: ( ( rule__BFunkZuordnung__LeitfunktionAssignment_5 ) )
            {
            // InternalRdsPP.g:1217:1: ( ( rule__BFunkZuordnung__LeitfunktionAssignment_5 ) )
            // InternalRdsPP.g:1218:2: ( rule__BFunkZuordnung__LeitfunktionAssignment_5 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getLeitfunktionAssignment_5()); 
            // InternalRdsPP.g:1219:2: ( rule__BFunkZuordnung__LeitfunktionAssignment_5 )
            // InternalRdsPP.g:1219:3: rule__BFunkZuordnung__LeitfunktionAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__LeitfunktionAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getLeitfunktionAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__5__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group__6"
    // InternalRdsPP.g:1227:1: rule__BFunkZuordnung__Group__6 : rule__BFunkZuordnung__Group__6__Impl ;
    public final void rule__BFunkZuordnung__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1231:1: ( rule__BFunkZuordnung__Group__6__Impl )
            // InternalRdsPP.g:1232:2: rule__BFunkZuordnung__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__6"


    // $ANTLR start "rule__BFunkZuordnung__Group__6__Impl"
    // InternalRdsPP.g:1238:1: rule__BFunkZuordnung__Group__6__Impl : ( ( rule__BFunkZuordnung__Alternatives_6 )? ) ;
    public final void rule__BFunkZuordnung__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1242:1: ( ( ( rule__BFunkZuordnung__Alternatives_6 )? ) )
            // InternalRdsPP.g:1243:1: ( ( rule__BFunkZuordnung__Alternatives_6 )? )
            {
            // InternalRdsPP.g:1243:1: ( ( rule__BFunkZuordnung__Alternatives_6 )? )
            // InternalRdsPP.g:1244:2: ( rule__BFunkZuordnung__Alternatives_6 )?
            {
             before(grammarAccess.getBFunkZuordnungAccess().getAlternatives_6()); 
            // InternalRdsPP.g:1245:2: ( rule__BFunkZuordnung__Alternatives_6 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_S) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRdsPP.g:1245:3: rule__BFunkZuordnung__Alternatives_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__BFunkZuordnung__Alternatives_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBFunkZuordnungAccess().getAlternatives_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group__6__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_0__0"
    // InternalRdsPP.g:1254:1: rule__BFunkZuordnung__Group_6_0__0 : rule__BFunkZuordnung__Group_6_0__0__Impl rule__BFunkZuordnung__Group_6_0__1 ;
    public final void rule__BFunkZuordnung__Group_6_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1258:1: ( rule__BFunkZuordnung__Group_6_0__0__Impl rule__BFunkZuordnung__Group_6_0__1 )
            // InternalRdsPP.g:1259:2: rule__BFunkZuordnung__Group_6_0__0__Impl rule__BFunkZuordnung__Group_6_0__1
            {
            pushFollow(FOLLOW_13);
            rule__BFunkZuordnung__Group_6_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group_6_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_0__0"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_0__0__Impl"
    // InternalRdsPP.g:1266:1: rule__BFunkZuordnung__Group_6_0__0__Impl : ( RULE_S ) ;
    public final void rule__BFunkZuordnung__Group_6_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1270:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1271:1: ( RULE_S )
            {
            // InternalRdsPP.g:1271:1: ( RULE_S )
            // InternalRdsPP.g:1272:2: RULE_S
            {
             before(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_0_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_0__0__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_0__1"
    // InternalRdsPP.g:1281:1: rule__BFunkZuordnung__Group_6_0__1 : rule__BFunkZuordnung__Group_6_0__1__Impl ;
    public final void rule__BFunkZuordnung__Group_6_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1285:1: ( rule__BFunkZuordnung__Group_6_0__1__Impl )
            // InternalRdsPP.g:1286:2: rule__BFunkZuordnung__Group_6_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group_6_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_0__1"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_0__1__Impl"
    // InternalRdsPP.g:1292:1: rule__BFunkZuordnung__Group_6_0__1__Impl : ( ( rule__BFunkZuordnung__SignalAssignment_6_0_1 ) ) ;
    public final void rule__BFunkZuordnung__Group_6_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1296:1: ( ( ( rule__BFunkZuordnung__SignalAssignment_6_0_1 ) ) )
            // InternalRdsPP.g:1297:1: ( ( rule__BFunkZuordnung__SignalAssignment_6_0_1 ) )
            {
            // InternalRdsPP.g:1297:1: ( ( rule__BFunkZuordnung__SignalAssignment_6_0_1 ) )
            // InternalRdsPP.g:1298:2: ( rule__BFunkZuordnung__SignalAssignment_6_0_1 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getSignalAssignment_6_0_1()); 
            // InternalRdsPP.g:1299:2: ( rule__BFunkZuordnung__SignalAssignment_6_0_1 )
            // InternalRdsPP.g:1299:3: rule__BFunkZuordnung__SignalAssignment_6_0_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__SignalAssignment_6_0_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getSignalAssignment_6_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_0__1__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_1__0"
    // InternalRdsPP.g:1308:1: rule__BFunkZuordnung__Group_6_1__0 : rule__BFunkZuordnung__Group_6_1__0__Impl rule__BFunkZuordnung__Group_6_1__1 ;
    public final void rule__BFunkZuordnung__Group_6_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1312:1: ( rule__BFunkZuordnung__Group_6_1__0__Impl rule__BFunkZuordnung__Group_6_1__1 )
            // InternalRdsPP.g:1313:2: rule__BFunkZuordnung__Group_6_1__0__Impl rule__BFunkZuordnung__Group_6_1__1
            {
            pushFollow(FOLLOW_12);
            rule__BFunkZuordnung__Group_6_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group_6_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_1__0"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_1__0__Impl"
    // InternalRdsPP.g:1320:1: rule__BFunkZuordnung__Group_6_1__0__Impl : ( RULE_S ) ;
    public final void rule__BFunkZuordnung__Group_6_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1324:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1325:1: ( RULE_S )
            {
            // InternalRdsPP.g:1325:1: ( RULE_S )
            // InternalRdsPP.g:1326:2: RULE_S
            {
             before(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_1_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getSTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_1__0__Impl"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_1__1"
    // InternalRdsPP.g:1335:1: rule__BFunkZuordnung__Group_6_1__1 : rule__BFunkZuordnung__Group_6_1__1__Impl ;
    public final void rule__BFunkZuordnung__Group_6_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1339:1: ( rule__BFunkZuordnung__Group_6_1__1__Impl )
            // InternalRdsPP.g:1340:2: rule__BFunkZuordnung__Group_6_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__Group_6_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_1__1"


    // $ANTLR start "rule__BFunkZuordnung__Group_6_1__1__Impl"
    // InternalRdsPP.g:1346:1: rule__BFunkZuordnung__Group_6_1__1__Impl : ( ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 ) ) ;
    public final void rule__BFunkZuordnung__Group_6_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1350:1: ( ( ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 ) ) )
            // InternalRdsPP.g:1351:1: ( ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 ) )
            {
            // InternalRdsPP.g:1351:1: ( ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 ) )
            // InternalRdsPP.g:1352:2: ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 )
            {
             before(grammarAccess.getBFunkZuordnungAccess().getDokumentationAssignment_6_1_1()); 
            // InternalRdsPP.g:1353:2: ( rule__BFunkZuordnung__DokumentationAssignment_6_1_1 )
            // InternalRdsPP.g:1353:3: rule__BFunkZuordnung__DokumentationAssignment_6_1_1
            {
            pushFollow(FOLLOW_2);
            rule__BFunkZuordnung__DokumentationAssignment_6_1_1();

            state._fsp--;


            }

             after(grammarAccess.getBFunkZuordnungAccess().getDokumentationAssignment_6_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Group_6_1__1__Impl"


    // $ANTLR start "rule__BEinbauort__Group__0"
    // InternalRdsPP.g:1362:1: rule__BEinbauort__Group__0 : rule__BEinbauort__Group__0__Impl rule__BEinbauort__Group__1 ;
    public final void rule__BEinbauort__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1366:1: ( rule__BEinbauort__Group__0__Impl rule__BEinbauort__Group__1 )
            // InternalRdsPP.g:1367:2: rule__BEinbauort__Group__0__Impl rule__BEinbauort__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__BEinbauort__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__0"


    // $ANTLR start "rule__BEinbauort__Group__0__Impl"
    // InternalRdsPP.g:1374:1: rule__BEinbauort__Group__0__Impl : ( '+' ) ;
    public final void rule__BEinbauort__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1378:1: ( ( '+' ) )
            // InternalRdsPP.g:1379:1: ( '+' )
            {
            // InternalRdsPP.g:1379:1: ( '+' )
            // InternalRdsPP.g:1380:2: '+'
            {
             before(grammarAccess.getBEinbauortAccess().getPlusSignKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getPlusSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__0__Impl"


    // $ANTLR start "rule__BEinbauort__Group__1"
    // InternalRdsPP.g:1389:1: rule__BEinbauort__Group__1 : rule__BEinbauort__Group__1__Impl rule__BEinbauort__Group__2 ;
    public final void rule__BEinbauort__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1393:1: ( rule__BEinbauort__Group__1__Impl rule__BEinbauort__Group__2 )
            // InternalRdsPP.g:1394:2: rule__BEinbauort__Group__1__Impl rule__BEinbauort__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__BEinbauort__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__1"


    // $ANTLR start "rule__BEinbauort__Group__1__Impl"
    // InternalRdsPP.g:1401:1: rule__BEinbauort__Group__1__Impl : ( ( rule__BEinbauort__HauptsystemAssignment_1 ) ) ;
    public final void rule__BEinbauort__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1405:1: ( ( ( rule__BEinbauort__HauptsystemAssignment_1 ) ) )
            // InternalRdsPP.g:1406:1: ( ( rule__BEinbauort__HauptsystemAssignment_1 ) )
            {
            // InternalRdsPP.g:1406:1: ( ( rule__BEinbauort__HauptsystemAssignment_1 ) )
            // InternalRdsPP.g:1407:2: ( rule__BEinbauort__HauptsystemAssignment_1 )
            {
             before(grammarAccess.getBEinbauortAccess().getHauptsystemAssignment_1()); 
            // InternalRdsPP.g:1408:2: ( rule__BEinbauort__HauptsystemAssignment_1 )
            // InternalRdsPP.g:1408:3: rule__BEinbauort__HauptsystemAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__HauptsystemAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getHauptsystemAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__1__Impl"


    // $ANTLR start "rule__BEinbauort__Group__2"
    // InternalRdsPP.g:1416:1: rule__BEinbauort__Group__2 : rule__BEinbauort__Group__2__Impl rule__BEinbauort__Group__3 ;
    public final void rule__BEinbauort__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1420:1: ( rule__BEinbauort__Group__2__Impl rule__BEinbauort__Group__3 )
            // InternalRdsPP.g:1421:2: rule__BEinbauort__Group__2__Impl rule__BEinbauort__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__BEinbauort__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__2"


    // $ANTLR start "rule__BEinbauort__Group__2__Impl"
    // InternalRdsPP.g:1428:1: rule__BEinbauort__Group__2__Impl : ( ( RULE_S )? ) ;
    public final void rule__BEinbauort__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1432:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:1433:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:1433:1: ( ( RULE_S )? )
            // InternalRdsPP.g:1434:2: ( RULE_S )?
            {
             before(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_2()); 
            // InternalRdsPP.g:1435:2: ( RULE_S )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_S) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRdsPP.g:1435:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__2__Impl"


    // $ANTLR start "rule__BEinbauort__Group__3"
    // InternalRdsPP.g:1443:1: rule__BEinbauort__Group__3 : rule__BEinbauort__Group__3__Impl rule__BEinbauort__Group__4 ;
    public final void rule__BEinbauort__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1447:1: ( rule__BEinbauort__Group__3__Impl rule__BEinbauort__Group__4 )
            // InternalRdsPP.g:1448:2: rule__BEinbauort__Group__3__Impl rule__BEinbauort__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__BEinbauort__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__3"


    // $ANTLR start "rule__BEinbauort__Group__3__Impl"
    // InternalRdsPP.g:1455:1: rule__BEinbauort__Group__3__Impl : ( ( rule__BEinbauort__EinbaueinheitAssignment_3 ) ) ;
    public final void rule__BEinbauort__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1459:1: ( ( ( rule__BEinbauort__EinbaueinheitAssignment_3 ) ) )
            // InternalRdsPP.g:1460:1: ( ( rule__BEinbauort__EinbaueinheitAssignment_3 ) )
            {
            // InternalRdsPP.g:1460:1: ( ( rule__BEinbauort__EinbaueinheitAssignment_3 ) )
            // InternalRdsPP.g:1461:2: ( rule__BEinbauort__EinbaueinheitAssignment_3 )
            {
             before(grammarAccess.getBEinbauortAccess().getEinbaueinheitAssignment_3()); 
            // InternalRdsPP.g:1462:2: ( rule__BEinbauort__EinbaueinheitAssignment_3 )
            // InternalRdsPP.g:1462:3: rule__BEinbauort__EinbaueinheitAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__EinbaueinheitAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getEinbaueinheitAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__3__Impl"


    // $ANTLR start "rule__BEinbauort__Group__4"
    // InternalRdsPP.g:1470:1: rule__BEinbauort__Group__4 : rule__BEinbauort__Group__4__Impl rule__BEinbauort__Group__5 ;
    public final void rule__BEinbauort__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1474:1: ( rule__BEinbauort__Group__4__Impl rule__BEinbauort__Group__5 )
            // InternalRdsPP.g:1475:2: rule__BEinbauort__Group__4__Impl rule__BEinbauort__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__BEinbauort__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__4"


    // $ANTLR start "rule__BEinbauort__Group__4__Impl"
    // InternalRdsPP.g:1482:1: rule__BEinbauort__Group__4__Impl : ( ( rule__BEinbauort__Group_4__0 )? ) ;
    public final void rule__BEinbauort__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1486:1: ( ( ( rule__BEinbauort__Group_4__0 )? ) )
            // InternalRdsPP.g:1487:1: ( ( rule__BEinbauort__Group_4__0 )? )
            {
            // InternalRdsPP.g:1487:1: ( ( rule__BEinbauort__Group_4__0 )? )
            // InternalRdsPP.g:1488:2: ( rule__BEinbauort__Group_4__0 )?
            {
             before(grammarAccess.getBEinbauortAccess().getGroup_4()); 
            // InternalRdsPP.g:1489:2: ( rule__BEinbauort__Group_4__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_S) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRdsPP.g:1489:3: rule__BEinbauort__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BEinbauort__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBEinbauortAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__4__Impl"


    // $ANTLR start "rule__BEinbauort__Group__5"
    // InternalRdsPP.g:1497:1: rule__BEinbauort__Group__5 : rule__BEinbauort__Group__5__Impl rule__BEinbauort__Group__6 ;
    public final void rule__BEinbauort__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1501:1: ( rule__BEinbauort__Group__5__Impl rule__BEinbauort__Group__6 )
            // InternalRdsPP.g:1502:2: rule__BEinbauort__Group__5__Impl rule__BEinbauort__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__BEinbauort__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__5"


    // $ANTLR start "rule__BEinbauort__Group__5__Impl"
    // InternalRdsPP.g:1509:1: rule__BEinbauort__Group__5__Impl : ( '.' ) ;
    public final void rule__BEinbauort__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1513:1: ( ( '.' ) )
            // InternalRdsPP.g:1514:1: ( '.' )
            {
            // InternalRdsPP.g:1514:1: ( '.' )
            // InternalRdsPP.g:1515:2: '.'
            {
             before(grammarAccess.getBEinbauortAccess().getFullStopKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getFullStopKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__5__Impl"


    // $ANTLR start "rule__BEinbauort__Group__6"
    // InternalRdsPP.g:1524:1: rule__BEinbauort__Group__6 : rule__BEinbauort__Group__6__Impl rule__BEinbauort__Group__7 ;
    public final void rule__BEinbauort__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1528:1: ( rule__BEinbauort__Group__6__Impl rule__BEinbauort__Group__7 )
            // InternalRdsPP.g:1529:2: rule__BEinbauort__Group__6__Impl rule__BEinbauort__Group__7
            {
            pushFollow(FOLLOW_10);
            rule__BEinbauort__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__6"


    // $ANTLR start "rule__BEinbauort__Group__6__Impl"
    // InternalRdsPP.g:1536:1: rule__BEinbauort__Group__6__Impl : ( ( rule__BEinbauort__EinbauplatzAssignment_6 ) ) ;
    public final void rule__BEinbauort__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1540:1: ( ( ( rule__BEinbauort__EinbauplatzAssignment_6 ) ) )
            // InternalRdsPP.g:1541:1: ( ( rule__BEinbauort__EinbauplatzAssignment_6 ) )
            {
            // InternalRdsPP.g:1541:1: ( ( rule__BEinbauort__EinbauplatzAssignment_6 ) )
            // InternalRdsPP.g:1542:2: ( rule__BEinbauort__EinbauplatzAssignment_6 )
            {
             before(grammarAccess.getBEinbauortAccess().getEinbauplatzAssignment_6()); 
            // InternalRdsPP.g:1543:2: ( rule__BEinbauort__EinbauplatzAssignment_6 )
            // InternalRdsPP.g:1543:3: rule__BEinbauort__EinbauplatzAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__EinbauplatzAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getEinbauplatzAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__6__Impl"


    // $ANTLR start "rule__BEinbauort__Group__7"
    // InternalRdsPP.g:1551:1: rule__BEinbauort__Group__7 : rule__BEinbauort__Group__7__Impl ;
    public final void rule__BEinbauort__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1555:1: ( rule__BEinbauort__Group__7__Impl )
            // InternalRdsPP.g:1556:2: rule__BEinbauort__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__7"


    // $ANTLR start "rule__BEinbauort__Group__7__Impl"
    // InternalRdsPP.g:1562:1: rule__BEinbauort__Group__7__Impl : ( ( rule__BEinbauort__Group_7__0 )? ) ;
    public final void rule__BEinbauort__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1566:1: ( ( ( rule__BEinbauort__Group_7__0 )? ) )
            // InternalRdsPP.g:1567:1: ( ( rule__BEinbauort__Group_7__0 )? )
            {
            // InternalRdsPP.g:1567:1: ( ( rule__BEinbauort__Group_7__0 )? )
            // InternalRdsPP.g:1568:2: ( rule__BEinbauort__Group_7__0 )?
            {
             before(grammarAccess.getBEinbauortAccess().getGroup_7()); 
            // InternalRdsPP.g:1569:2: ( rule__BEinbauort__Group_7__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_S) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalRdsPP.g:1569:3: rule__BEinbauort__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BEinbauort__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBEinbauortAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group__7__Impl"


    // $ANTLR start "rule__BEinbauort__Group_4__0"
    // InternalRdsPP.g:1578:1: rule__BEinbauort__Group_4__0 : rule__BEinbauort__Group_4__0__Impl rule__BEinbauort__Group_4__1 ;
    public final void rule__BEinbauort__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1582:1: ( rule__BEinbauort__Group_4__0__Impl rule__BEinbauort__Group_4__1 )
            // InternalRdsPP.g:1583:2: rule__BEinbauort__Group_4__0__Impl rule__BEinbauort__Group_4__1
            {
            pushFollow(FOLLOW_16);
            rule__BEinbauort__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_4__0"


    // $ANTLR start "rule__BEinbauort__Group_4__0__Impl"
    // InternalRdsPP.g:1590:1: rule__BEinbauort__Group_4__0__Impl : ( RULE_S ) ;
    public final void rule__BEinbauort__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1594:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1595:1: ( RULE_S )
            {
            // InternalRdsPP.g:1595:1: ( RULE_S )
            // InternalRdsPP.g:1596:2: RULE_S
            {
             before(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_4_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_4__0__Impl"


    // $ANTLR start "rule__BEinbauort__Group_4__1"
    // InternalRdsPP.g:1605:1: rule__BEinbauort__Group_4__1 : rule__BEinbauort__Group_4__1__Impl ;
    public final void rule__BEinbauort__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1609:1: ( rule__BEinbauort__Group_4__1__Impl )
            // InternalRdsPP.g:1610:2: rule__BEinbauort__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_4__1"


    // $ANTLR start "rule__BEinbauort__Group_4__1__Impl"
    // InternalRdsPP.g:1616:1: rule__BEinbauort__Group_4__1__Impl : ( ( rule__BEinbauort__KlassifizierungAssignment_4_1 ) ) ;
    public final void rule__BEinbauort__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1620:1: ( ( ( rule__BEinbauort__KlassifizierungAssignment_4_1 ) ) )
            // InternalRdsPP.g:1621:1: ( ( rule__BEinbauort__KlassifizierungAssignment_4_1 ) )
            {
            // InternalRdsPP.g:1621:1: ( ( rule__BEinbauort__KlassifizierungAssignment_4_1 ) )
            // InternalRdsPP.g:1622:2: ( rule__BEinbauort__KlassifizierungAssignment_4_1 )
            {
             before(grammarAccess.getBEinbauortAccess().getKlassifizierungAssignment_4_1()); 
            // InternalRdsPP.g:1623:2: ( rule__BEinbauort__KlassifizierungAssignment_4_1 )
            // InternalRdsPP.g:1623:3: rule__BEinbauort__KlassifizierungAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__KlassifizierungAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getKlassifizierungAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_4__1__Impl"


    // $ANTLR start "rule__BEinbauort__Group_7__0"
    // InternalRdsPP.g:1632:1: rule__BEinbauort__Group_7__0 : rule__BEinbauort__Group_7__0__Impl rule__BEinbauort__Group_7__1 ;
    public final void rule__BEinbauort__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1636:1: ( rule__BEinbauort__Group_7__0__Impl rule__BEinbauort__Group_7__1 )
            // InternalRdsPP.g:1637:2: rule__BEinbauort__Group_7__0__Impl rule__BEinbauort__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__BEinbauort__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_7__0"


    // $ANTLR start "rule__BEinbauort__Group_7__0__Impl"
    // InternalRdsPP.g:1644:1: rule__BEinbauort__Group_7__0__Impl : ( RULE_S ) ;
    public final void rule__BEinbauort__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1648:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1649:1: ( RULE_S )
            {
            // InternalRdsPP.g:1649:1: ( RULE_S )
            // InternalRdsPP.g:1650:2: RULE_S
            {
             before(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_7_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getSTerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_7__0__Impl"


    // $ANTLR start "rule__BEinbauort__Group_7__1"
    // InternalRdsPP.g:1659:1: rule__BEinbauort__Group_7__1 : rule__BEinbauort__Group_7__1__Impl ;
    public final void rule__BEinbauort__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1663:1: ( rule__BEinbauort__Group_7__1__Impl )
            // InternalRdsPP.g:1664:2: rule__BEinbauort__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_7__1"


    // $ANTLR start "rule__BEinbauort__Group_7__1__Impl"
    // InternalRdsPP.g:1670:1: rule__BEinbauort__Group_7__1__Impl : ( ( rule__BEinbauort__DokumentationAssignment_7_1 ) ) ;
    public final void rule__BEinbauort__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1674:1: ( ( ( rule__BEinbauort__DokumentationAssignment_7_1 ) ) )
            // InternalRdsPP.g:1675:1: ( ( rule__BEinbauort__DokumentationAssignment_7_1 ) )
            {
            // InternalRdsPP.g:1675:1: ( ( rule__BEinbauort__DokumentationAssignment_7_1 ) )
            // InternalRdsPP.g:1676:2: ( rule__BEinbauort__DokumentationAssignment_7_1 )
            {
             before(grammarAccess.getBEinbauortAccess().getDokumentationAssignment_7_1()); 
            // InternalRdsPP.g:1677:2: ( rule__BEinbauort__DokumentationAssignment_7_1 )
            // InternalRdsPP.g:1677:3: rule__BEinbauort__DokumentationAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__BEinbauort__DokumentationAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getBEinbauortAccess().getDokumentationAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__Group_7__1__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__0"
    // InternalRdsPP.g:1686:1: rule__BAufstellungsort__Group__0 : rule__BAufstellungsort__Group__0__Impl rule__BAufstellungsort__Group__1 ;
    public final void rule__BAufstellungsort__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1690:1: ( rule__BAufstellungsort__Group__0__Impl rule__BAufstellungsort__Group__1 )
            // InternalRdsPP.g:1691:2: rule__BAufstellungsort__Group__0__Impl rule__BAufstellungsort__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__BAufstellungsort__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__0"


    // $ANTLR start "rule__BAufstellungsort__Group__0__Impl"
    // InternalRdsPP.g:1698:1: rule__BAufstellungsort__Group__0__Impl : ( '++' ) ;
    public final void rule__BAufstellungsort__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1702:1: ( ( '++' ) )
            // InternalRdsPP.g:1703:1: ( '++' )
            {
            // InternalRdsPP.g:1703:1: ( '++' )
            // InternalRdsPP.g:1704:2: '++'
            {
             before(grammarAccess.getBAufstellungsortAccess().getPlusSignPlusSignKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getBAufstellungsortAccess().getPlusSignPlusSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__0__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__1"
    // InternalRdsPP.g:1713:1: rule__BAufstellungsort__Group__1 : rule__BAufstellungsort__Group__1__Impl rule__BAufstellungsort__Group__2 ;
    public final void rule__BAufstellungsort__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1717:1: ( rule__BAufstellungsort__Group__1__Impl rule__BAufstellungsort__Group__2 )
            // InternalRdsPP.g:1718:2: rule__BAufstellungsort__Group__1__Impl rule__BAufstellungsort__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__BAufstellungsort__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__1"


    // $ANTLR start "rule__BAufstellungsort__Group__1__Impl"
    // InternalRdsPP.g:1725:1: rule__BAufstellungsort__Group__1__Impl : ( ( rule__BAufstellungsort__HauptsystemAssignment_1 ) ) ;
    public final void rule__BAufstellungsort__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1729:1: ( ( ( rule__BAufstellungsort__HauptsystemAssignment_1 ) ) )
            // InternalRdsPP.g:1730:1: ( ( rule__BAufstellungsort__HauptsystemAssignment_1 ) )
            {
            // InternalRdsPP.g:1730:1: ( ( rule__BAufstellungsort__HauptsystemAssignment_1 ) )
            // InternalRdsPP.g:1731:2: ( rule__BAufstellungsort__HauptsystemAssignment_1 )
            {
             before(grammarAccess.getBAufstellungsortAccess().getHauptsystemAssignment_1()); 
            // InternalRdsPP.g:1732:2: ( rule__BAufstellungsort__HauptsystemAssignment_1 )
            // InternalRdsPP.g:1732:3: rule__BAufstellungsort__HauptsystemAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__HauptsystemAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBAufstellungsortAccess().getHauptsystemAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__1__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__2"
    // InternalRdsPP.g:1740:1: rule__BAufstellungsort__Group__2 : rule__BAufstellungsort__Group__2__Impl rule__BAufstellungsort__Group__3 ;
    public final void rule__BAufstellungsort__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1744:1: ( rule__BAufstellungsort__Group__2__Impl rule__BAufstellungsort__Group__3 )
            // InternalRdsPP.g:1745:2: rule__BAufstellungsort__Group__2__Impl rule__BAufstellungsort__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__BAufstellungsort__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__2"


    // $ANTLR start "rule__BAufstellungsort__Group__2__Impl"
    // InternalRdsPP.g:1752:1: rule__BAufstellungsort__Group__2__Impl : ( ( RULE_S )? ) ;
    public final void rule__BAufstellungsort__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1756:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:1757:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:1757:1: ( ( RULE_S )? )
            // InternalRdsPP.g:1758:2: ( RULE_S )?
            {
             before(grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_2()); 
            // InternalRdsPP.g:1759:2: ( RULE_S )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_S) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalRdsPP.g:1759:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__2__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__3"
    // InternalRdsPP.g:1767:1: rule__BAufstellungsort__Group__3 : rule__BAufstellungsort__Group__3__Impl rule__BAufstellungsort__Group__4 ;
    public final void rule__BAufstellungsort__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1771:1: ( rule__BAufstellungsort__Group__3__Impl rule__BAufstellungsort__Group__4 )
            // InternalRdsPP.g:1772:2: rule__BAufstellungsort__Group__3__Impl rule__BAufstellungsort__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__BAufstellungsort__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__3"


    // $ANTLR start "rule__BAufstellungsort__Group__3__Impl"
    // InternalRdsPP.g:1779:1: rule__BAufstellungsort__Group__3__Impl : ( ( rule__BAufstellungsort__OrtAssignment_3 ) ) ;
    public final void rule__BAufstellungsort__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1783:1: ( ( ( rule__BAufstellungsort__OrtAssignment_3 ) ) )
            // InternalRdsPP.g:1784:1: ( ( rule__BAufstellungsort__OrtAssignment_3 ) )
            {
            // InternalRdsPP.g:1784:1: ( ( rule__BAufstellungsort__OrtAssignment_3 ) )
            // InternalRdsPP.g:1785:2: ( rule__BAufstellungsort__OrtAssignment_3 )
            {
             before(grammarAccess.getBAufstellungsortAccess().getOrtAssignment_3()); 
            // InternalRdsPP.g:1786:2: ( rule__BAufstellungsort__OrtAssignment_3 )
            // InternalRdsPP.g:1786:3: rule__BAufstellungsort__OrtAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__OrtAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBAufstellungsortAccess().getOrtAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__3__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__4"
    // InternalRdsPP.g:1794:1: rule__BAufstellungsort__Group__4 : rule__BAufstellungsort__Group__4__Impl rule__BAufstellungsort__Group__5 ;
    public final void rule__BAufstellungsort__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1798:1: ( rule__BAufstellungsort__Group__4__Impl rule__BAufstellungsort__Group__5 )
            // InternalRdsPP.g:1799:2: rule__BAufstellungsort__Group__4__Impl rule__BAufstellungsort__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__BAufstellungsort__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__4"


    // $ANTLR start "rule__BAufstellungsort__Group__4__Impl"
    // InternalRdsPP.g:1806:1: rule__BAufstellungsort__Group__4__Impl : ( '.' ) ;
    public final void rule__BAufstellungsort__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1810:1: ( ( '.' ) )
            // InternalRdsPP.g:1811:1: ( '.' )
            {
            // InternalRdsPP.g:1811:1: ( '.' )
            // InternalRdsPP.g:1812:2: '.'
            {
             before(grammarAccess.getBAufstellungsortAccess().getFullStopKeyword_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBAufstellungsortAccess().getFullStopKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__4__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__5"
    // InternalRdsPP.g:1821:1: rule__BAufstellungsort__Group__5 : rule__BAufstellungsort__Group__5__Impl rule__BAufstellungsort__Group__6 ;
    public final void rule__BAufstellungsort__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1825:1: ( rule__BAufstellungsort__Group__5__Impl rule__BAufstellungsort__Group__6 )
            // InternalRdsPP.g:1826:2: rule__BAufstellungsort__Group__5__Impl rule__BAufstellungsort__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__BAufstellungsort__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__5"


    // $ANTLR start "rule__BAufstellungsort__Group__5__Impl"
    // InternalRdsPP.g:1833:1: rule__BAufstellungsort__Group__5__Impl : ( ( rule__BAufstellungsort__BereichAssignment_5 ) ) ;
    public final void rule__BAufstellungsort__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1837:1: ( ( ( rule__BAufstellungsort__BereichAssignment_5 ) ) )
            // InternalRdsPP.g:1838:1: ( ( rule__BAufstellungsort__BereichAssignment_5 ) )
            {
            // InternalRdsPP.g:1838:1: ( ( rule__BAufstellungsort__BereichAssignment_5 ) )
            // InternalRdsPP.g:1839:2: ( rule__BAufstellungsort__BereichAssignment_5 )
            {
             before(grammarAccess.getBAufstellungsortAccess().getBereichAssignment_5()); 
            // InternalRdsPP.g:1840:2: ( rule__BAufstellungsort__BereichAssignment_5 )
            // InternalRdsPP.g:1840:3: rule__BAufstellungsort__BereichAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__BereichAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getBAufstellungsortAccess().getBereichAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__5__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group__6"
    // InternalRdsPP.g:1848:1: rule__BAufstellungsort__Group__6 : rule__BAufstellungsort__Group__6__Impl ;
    public final void rule__BAufstellungsort__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1852:1: ( rule__BAufstellungsort__Group__6__Impl )
            // InternalRdsPP.g:1853:2: rule__BAufstellungsort__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__6"


    // $ANTLR start "rule__BAufstellungsort__Group__6__Impl"
    // InternalRdsPP.g:1859:1: rule__BAufstellungsort__Group__6__Impl : ( ( rule__BAufstellungsort__Group_6__0 )? ) ;
    public final void rule__BAufstellungsort__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1863:1: ( ( ( rule__BAufstellungsort__Group_6__0 )? ) )
            // InternalRdsPP.g:1864:1: ( ( rule__BAufstellungsort__Group_6__0 )? )
            {
            // InternalRdsPP.g:1864:1: ( ( rule__BAufstellungsort__Group_6__0 )? )
            // InternalRdsPP.g:1865:2: ( rule__BAufstellungsort__Group_6__0 )?
            {
             before(grammarAccess.getBAufstellungsortAccess().getGroup_6()); 
            // InternalRdsPP.g:1866:2: ( rule__BAufstellungsort__Group_6__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_S) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalRdsPP.g:1866:3: rule__BAufstellungsort__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BAufstellungsort__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBAufstellungsortAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group__6__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group_6__0"
    // InternalRdsPP.g:1875:1: rule__BAufstellungsort__Group_6__0 : rule__BAufstellungsort__Group_6__0__Impl rule__BAufstellungsort__Group_6__1 ;
    public final void rule__BAufstellungsort__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1879:1: ( rule__BAufstellungsort__Group_6__0__Impl rule__BAufstellungsort__Group_6__1 )
            // InternalRdsPP.g:1880:2: rule__BAufstellungsort__Group_6__0__Impl rule__BAufstellungsort__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__BAufstellungsort__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group_6__0"


    // $ANTLR start "rule__BAufstellungsort__Group_6__0__Impl"
    // InternalRdsPP.g:1887:1: rule__BAufstellungsort__Group_6__0__Impl : ( RULE_S ) ;
    public final void rule__BAufstellungsort__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1891:1: ( ( RULE_S ) )
            // InternalRdsPP.g:1892:1: ( RULE_S )
            {
            // InternalRdsPP.g:1892:1: ( RULE_S )
            // InternalRdsPP.g:1893:2: RULE_S
            {
             before(grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_6_0()); 
            match(input,RULE_S,FOLLOW_2); 
             after(grammarAccess.getBAufstellungsortAccess().getSTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group_6__0__Impl"


    // $ANTLR start "rule__BAufstellungsort__Group_6__1"
    // InternalRdsPP.g:1902:1: rule__BAufstellungsort__Group_6__1 : rule__BAufstellungsort__Group_6__1__Impl ;
    public final void rule__BAufstellungsort__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1906:1: ( rule__BAufstellungsort__Group_6__1__Impl )
            // InternalRdsPP.g:1907:2: rule__BAufstellungsort__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group_6__1"


    // $ANTLR start "rule__BAufstellungsort__Group_6__1__Impl"
    // InternalRdsPP.g:1913:1: rule__BAufstellungsort__Group_6__1__Impl : ( ( rule__BAufstellungsort__DokumentationAssignment_6_1 ) ) ;
    public final void rule__BAufstellungsort__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1917:1: ( ( ( rule__BAufstellungsort__DokumentationAssignment_6_1 ) ) )
            // InternalRdsPP.g:1918:1: ( ( rule__BAufstellungsort__DokumentationAssignment_6_1 ) )
            {
            // InternalRdsPP.g:1918:1: ( ( rule__BAufstellungsort__DokumentationAssignment_6_1 ) )
            // InternalRdsPP.g:1919:2: ( rule__BAufstellungsort__DokumentationAssignment_6_1 )
            {
             before(grammarAccess.getBAufstellungsortAccess().getDokumentationAssignment_6_1()); 
            // InternalRdsPP.g:1920:2: ( rule__BAufstellungsort__DokumentationAssignment_6_1 )
            // InternalRdsPP.g:1920:3: rule__BAufstellungsort__DokumentationAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__BAufstellungsort__DokumentationAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getBAufstellungsortAccess().getDokumentationAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__Group_6__1__Impl"


    // $ANTLR start "rule__BProdukt__Group__0"
    // InternalRdsPP.g:1929:1: rule__BProdukt__Group__0 : rule__BProdukt__Group__0__Impl rule__BProdukt__Group__1 ;
    public final void rule__BProdukt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1933:1: ( rule__BProdukt__Group__0__Impl rule__BProdukt__Group__1 )
            // InternalRdsPP.g:1934:2: rule__BProdukt__Group__0__Impl rule__BProdukt__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__BProdukt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BProdukt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__Group__0"


    // $ANTLR start "rule__BProdukt__Group__0__Impl"
    // InternalRdsPP.g:1941:1: rule__BProdukt__Group__0__Impl : ( '-' ) ;
    public final void rule__BProdukt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1945:1: ( ( '-' ) )
            // InternalRdsPP.g:1946:1: ( '-' )
            {
            // InternalRdsPP.g:1946:1: ( '-' )
            // InternalRdsPP.g:1947:2: '-'
            {
             before(grammarAccess.getBProduktAccess().getHyphenMinusKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getBProduktAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__Group__0__Impl"


    // $ANTLR start "rule__BProdukt__Group__1"
    // InternalRdsPP.g:1956:1: rule__BProdukt__Group__1 : rule__BProdukt__Group__1__Impl ;
    public final void rule__BProdukt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1960:1: ( rule__BProdukt__Group__1__Impl )
            // InternalRdsPP.g:1961:2: rule__BProdukt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BProdukt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__Group__1"


    // $ANTLR start "rule__BProdukt__Group__1__Impl"
    // InternalRdsPP.g:1967:1: rule__BProdukt__Group__1__Impl : ( ( rule__BProdukt__ProduktAssignment_1 ) ) ;
    public final void rule__BProdukt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1971:1: ( ( ( rule__BProdukt__ProduktAssignment_1 ) ) )
            // InternalRdsPP.g:1972:1: ( ( rule__BProdukt__ProduktAssignment_1 ) )
            {
            // InternalRdsPP.g:1972:1: ( ( rule__BProdukt__ProduktAssignment_1 ) )
            // InternalRdsPP.g:1973:2: ( rule__BProdukt__ProduktAssignment_1 )
            {
             before(grammarAccess.getBProduktAccess().getProduktAssignment_1()); 
            // InternalRdsPP.g:1974:2: ( rule__BProdukt__ProduktAssignment_1 )
            // InternalRdsPP.g:1974:3: rule__BProdukt__ProduktAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BProdukt__ProduktAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBProduktAccess().getProduktAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__Group__1__Impl"


    // $ANTLR start "rule__BAnschluss__Group__0"
    // InternalRdsPP.g:1983:1: rule__BAnschluss__Group__0 : rule__BAnschluss__Group__0__Impl rule__BAnschluss__Group__1 ;
    public final void rule__BAnschluss__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1987:1: ( rule__BAnschluss__Group__0__Impl rule__BAnschluss__Group__1 )
            // InternalRdsPP.g:1988:2: rule__BAnschluss__Group__0__Impl rule__BAnschluss__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__BAnschluss__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__0"


    // $ANTLR start "rule__BAnschluss__Group__0__Impl"
    // InternalRdsPP.g:1995:1: rule__BAnschluss__Group__0__Impl : ( ':' ) ;
    public final void rule__BAnschluss__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:1999:1: ( ( ':' ) )
            // InternalRdsPP.g:2000:1: ( ':' )
            {
            // InternalRdsPP.g:2000:1: ( ':' )
            // InternalRdsPP.g:2001:2: ':'
            {
             before(grammarAccess.getBAnschlussAccess().getColonKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getBAnschlussAccess().getColonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__0__Impl"


    // $ANTLR start "rule__BAnschluss__Group__1"
    // InternalRdsPP.g:2010:1: rule__BAnschluss__Group__1 : rule__BAnschluss__Group__1__Impl rule__BAnschluss__Group__2 ;
    public final void rule__BAnschluss__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2014:1: ( rule__BAnschluss__Group__1__Impl rule__BAnschluss__Group__2 )
            // InternalRdsPP.g:2015:2: rule__BAnschluss__Group__1__Impl rule__BAnschluss__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__BAnschluss__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__1"


    // $ANTLR start "rule__BAnschluss__Group__1__Impl"
    // InternalRdsPP.g:2022:1: rule__BAnschluss__Group__1__Impl : ( ( rule__BAnschluss__Kennzeichnung1Assignment_1 ) ) ;
    public final void rule__BAnschluss__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2026:1: ( ( ( rule__BAnschluss__Kennzeichnung1Assignment_1 ) ) )
            // InternalRdsPP.g:2027:1: ( ( rule__BAnschluss__Kennzeichnung1Assignment_1 ) )
            {
            // InternalRdsPP.g:2027:1: ( ( rule__BAnschluss__Kennzeichnung1Assignment_1 ) )
            // InternalRdsPP.g:2028:2: ( rule__BAnschluss__Kennzeichnung1Assignment_1 )
            {
             before(grammarAccess.getBAnschlussAccess().getKennzeichnung1Assignment_1()); 
            // InternalRdsPP.g:2029:2: ( rule__BAnschluss__Kennzeichnung1Assignment_1 )
            // InternalRdsPP.g:2029:3: rule__BAnschluss__Kennzeichnung1Assignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BAnschluss__Kennzeichnung1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBAnschlussAccess().getKennzeichnung1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__1__Impl"


    // $ANTLR start "rule__BAnschluss__Group__2"
    // InternalRdsPP.g:2037:1: rule__BAnschluss__Group__2 : rule__BAnschluss__Group__2__Impl rule__BAnschluss__Group__3 ;
    public final void rule__BAnschluss__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2041:1: ( rule__BAnschluss__Group__2__Impl rule__BAnschluss__Group__3 )
            // InternalRdsPP.g:2042:2: rule__BAnschluss__Group__2__Impl rule__BAnschluss__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__BAnschluss__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__2"


    // $ANTLR start "rule__BAnschluss__Group__2__Impl"
    // InternalRdsPP.g:2049:1: rule__BAnschluss__Group__2__Impl : ( ( rule__BAnschluss__Group_2__0 )? ) ;
    public final void rule__BAnschluss__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2053:1: ( ( ( rule__BAnschluss__Group_2__0 )? ) )
            // InternalRdsPP.g:2054:1: ( ( rule__BAnschluss__Group_2__0 )? )
            {
            // InternalRdsPP.g:2054:1: ( ( rule__BAnschluss__Group_2__0 )? )
            // InternalRdsPP.g:2055:2: ( rule__BAnschluss__Group_2__0 )?
            {
             before(grammarAccess.getBAnschlussAccess().getGroup_2()); 
            // InternalRdsPP.g:2056:2: ( rule__BAnschluss__Group_2__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==20) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalRdsPP.g:2056:3: rule__BAnschluss__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BAnschluss__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBAnschlussAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__2__Impl"


    // $ANTLR start "rule__BAnschluss__Group__3"
    // InternalRdsPP.g:2064:1: rule__BAnschluss__Group__3 : rule__BAnschluss__Group__3__Impl ;
    public final void rule__BAnschluss__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2068:1: ( rule__BAnschluss__Group__3__Impl )
            // InternalRdsPP.g:2069:2: rule__BAnschluss__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__3"


    // $ANTLR start "rule__BAnschluss__Group__3__Impl"
    // InternalRdsPP.g:2075:1: rule__BAnschluss__Group__3__Impl : ( ( RULE_S )? ) ;
    public final void rule__BAnschluss__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2079:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:2080:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:2080:1: ( ( RULE_S )? )
            // InternalRdsPP.g:2081:2: ( RULE_S )?
            {
             before(grammarAccess.getBAnschlussAccess().getSTerminalRuleCall_3()); 
            // InternalRdsPP.g:2082:2: ( RULE_S )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==RULE_S) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalRdsPP.g:2082:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBAnschlussAccess().getSTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group__3__Impl"


    // $ANTLR start "rule__BAnschluss__Group_2__0"
    // InternalRdsPP.g:2091:1: rule__BAnschluss__Group_2__0 : rule__BAnschluss__Group_2__0__Impl rule__BAnschluss__Group_2__1 ;
    public final void rule__BAnschluss__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2095:1: ( rule__BAnschluss__Group_2__0__Impl rule__BAnschluss__Group_2__1 )
            // InternalRdsPP.g:2096:2: rule__BAnschluss__Group_2__0__Impl rule__BAnschluss__Group_2__1
            {
            pushFollow(FOLLOW_18);
            rule__BAnschluss__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group_2__0"


    // $ANTLR start "rule__BAnschluss__Group_2__0__Impl"
    // InternalRdsPP.g:2103:1: rule__BAnschluss__Group_2__0__Impl : ( '.' ) ;
    public final void rule__BAnschluss__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2107:1: ( ( '.' ) )
            // InternalRdsPP.g:2108:1: ( '.' )
            {
            // InternalRdsPP.g:2108:1: ( '.' )
            // InternalRdsPP.g:2109:2: '.'
            {
             before(grammarAccess.getBAnschlussAccess().getFullStopKeyword_2_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBAnschlussAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group_2__0__Impl"


    // $ANTLR start "rule__BAnschluss__Group_2__1"
    // InternalRdsPP.g:2118:1: rule__BAnschluss__Group_2__1 : rule__BAnschluss__Group_2__1__Impl ;
    public final void rule__BAnschluss__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2122:1: ( rule__BAnschluss__Group_2__1__Impl )
            // InternalRdsPP.g:2123:2: rule__BAnschluss__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BAnschluss__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group_2__1"


    // $ANTLR start "rule__BAnschluss__Group_2__1__Impl"
    // InternalRdsPP.g:2129:1: rule__BAnschluss__Group_2__1__Impl : ( ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 ) ) ;
    public final void rule__BAnschluss__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2133:1: ( ( ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 ) ) )
            // InternalRdsPP.g:2134:1: ( ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 ) )
            {
            // InternalRdsPP.g:2134:1: ( ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 ) )
            // InternalRdsPP.g:2135:2: ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 )
            {
             before(grammarAccess.getBAnschlussAccess().getKennzeichnung2Assignment_2_1()); 
            // InternalRdsPP.g:2136:2: ( rule__BAnschluss__Kennzeichnung2Assignment_2_1 )
            // InternalRdsPP.g:2136:3: rule__BAnschluss__Kennzeichnung2Assignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__BAnschluss__Kennzeichnung2Assignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getBAnschlussAccess().getKennzeichnung2Assignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Group_2__1__Impl"


    // $ANTLR start "rule__BDoku__Group__0"
    // InternalRdsPP.g:2145:1: rule__BDoku__Group__0 : rule__BDoku__Group__0__Impl rule__BDoku__Group__1 ;
    public final void rule__BDoku__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2149:1: ( rule__BDoku__Group__0__Impl rule__BDoku__Group__1 )
            // InternalRdsPP.g:2150:2: rule__BDoku__Group__0__Impl rule__BDoku__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__BDoku__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BDoku__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__0"


    // $ANTLR start "rule__BDoku__Group__0__Impl"
    // InternalRdsPP.g:2157:1: rule__BDoku__Group__0__Impl : ( '&' ) ;
    public final void rule__BDoku__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2161:1: ( ( '&' ) )
            // InternalRdsPP.g:2162:1: ( '&' )
            {
            // InternalRdsPP.g:2162:1: ( '&' )
            // InternalRdsPP.g:2163:2: '&'
            {
             before(grammarAccess.getBDokuAccess().getAmpersandKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getBDokuAccess().getAmpersandKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__0__Impl"


    // $ANTLR start "rule__BDoku__Group__1"
    // InternalRdsPP.g:2172:1: rule__BDoku__Group__1 : rule__BDoku__Group__1__Impl rule__BDoku__Group__2 ;
    public final void rule__BDoku__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2176:1: ( rule__BDoku__Group__1__Impl rule__BDoku__Group__2 )
            // InternalRdsPP.g:2177:2: rule__BDoku__Group__1__Impl rule__BDoku__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__BDoku__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BDoku__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__1"


    // $ANTLR start "rule__BDoku__Group__1__Impl"
    // InternalRdsPP.g:2184:1: rule__BDoku__Group__1__Impl : ( ( rule__BDoku__DokuAssignment_1 ) ) ;
    public final void rule__BDoku__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2188:1: ( ( ( rule__BDoku__DokuAssignment_1 ) ) )
            // InternalRdsPP.g:2189:1: ( ( rule__BDoku__DokuAssignment_1 ) )
            {
            // InternalRdsPP.g:2189:1: ( ( rule__BDoku__DokuAssignment_1 ) )
            // InternalRdsPP.g:2190:2: ( rule__BDoku__DokuAssignment_1 )
            {
             before(grammarAccess.getBDokuAccess().getDokuAssignment_1()); 
            // InternalRdsPP.g:2191:2: ( rule__BDoku__DokuAssignment_1 )
            // InternalRdsPP.g:2191:3: rule__BDoku__DokuAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BDoku__DokuAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBDokuAccess().getDokuAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__1__Impl"


    // $ANTLR start "rule__BDoku__Group__2"
    // InternalRdsPP.g:2199:1: rule__BDoku__Group__2 : rule__BDoku__Group__2__Impl ;
    public final void rule__BDoku__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2203:1: ( rule__BDoku__Group__2__Impl )
            // InternalRdsPP.g:2204:2: rule__BDoku__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BDoku__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__2"


    // $ANTLR start "rule__BDoku__Group__2__Impl"
    // InternalRdsPP.g:2210:1: rule__BDoku__Group__2__Impl : ( ( RULE_S )? ) ;
    public final void rule__BDoku__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2214:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:2215:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:2215:1: ( ( RULE_S )? )
            // InternalRdsPP.g:2216:2: ( RULE_S )?
            {
             before(grammarAccess.getBDokuAccess().getSTerminalRuleCall_2()); 
            // InternalRdsPP.g:2217:2: ( RULE_S )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_S) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalRdsPP.g:2217:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBDokuAccess().getSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__Group__2__Impl"


    // $ANTLR start "rule__BSignal__Group__0"
    // InternalRdsPP.g:2226:1: rule__BSignal__Group__0 : rule__BSignal__Group__0__Impl rule__BSignal__Group__1 ;
    public final void rule__BSignal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2230:1: ( rule__BSignal__Group__0__Impl rule__BSignal__Group__1 )
            // InternalRdsPP.g:2231:2: rule__BSignal__Group__0__Impl rule__BSignal__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__BSignal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BSignal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__0"


    // $ANTLR start "rule__BSignal__Group__0__Impl"
    // InternalRdsPP.g:2238:1: rule__BSignal__Group__0__Impl : ( ';' ) ;
    public final void rule__BSignal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2242:1: ( ( ';' ) )
            // InternalRdsPP.g:2243:1: ( ';' )
            {
            // InternalRdsPP.g:2243:1: ( ';' )
            // InternalRdsPP.g:2244:2: ';'
            {
             before(grammarAccess.getBSignalAccess().getSemicolonKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getBSignalAccess().getSemicolonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__0__Impl"


    // $ANTLR start "rule__BSignal__Group__1"
    // InternalRdsPP.g:2253:1: rule__BSignal__Group__1 : rule__BSignal__Group__1__Impl rule__BSignal__Group__2 ;
    public final void rule__BSignal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2257:1: ( rule__BSignal__Group__1__Impl rule__BSignal__Group__2 )
            // InternalRdsPP.g:2258:2: rule__BSignal__Group__1__Impl rule__BSignal__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__BSignal__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BSignal__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__1"


    // $ANTLR start "rule__BSignal__Group__1__Impl"
    // InternalRdsPP.g:2265:1: rule__BSignal__Group__1__Impl : ( ( rule__BSignal__KlassifizierungAssignment_1 ) ) ;
    public final void rule__BSignal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2269:1: ( ( ( rule__BSignal__KlassifizierungAssignment_1 ) ) )
            // InternalRdsPP.g:2270:1: ( ( rule__BSignal__KlassifizierungAssignment_1 ) )
            {
            // InternalRdsPP.g:2270:1: ( ( rule__BSignal__KlassifizierungAssignment_1 ) )
            // InternalRdsPP.g:2271:2: ( rule__BSignal__KlassifizierungAssignment_1 )
            {
             before(grammarAccess.getBSignalAccess().getKlassifizierungAssignment_1()); 
            // InternalRdsPP.g:2272:2: ( rule__BSignal__KlassifizierungAssignment_1 )
            // InternalRdsPP.g:2272:3: rule__BSignal__KlassifizierungAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BSignal__KlassifizierungAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBSignalAccess().getKlassifizierungAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__1__Impl"


    // $ANTLR start "rule__BSignal__Group__2"
    // InternalRdsPP.g:2280:1: rule__BSignal__Group__2 : rule__BSignal__Group__2__Impl rule__BSignal__Group__3 ;
    public final void rule__BSignal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2284:1: ( rule__BSignal__Group__2__Impl rule__BSignal__Group__3 )
            // InternalRdsPP.g:2285:2: rule__BSignal__Group__2__Impl rule__BSignal__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__BSignal__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BSignal__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__2"


    // $ANTLR start "rule__BSignal__Group__2__Impl"
    // InternalRdsPP.g:2292:1: rule__BSignal__Group__2__Impl : ( '.' ) ;
    public final void rule__BSignal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2296:1: ( ( '.' ) )
            // InternalRdsPP.g:2297:1: ( '.' )
            {
            // InternalRdsPP.g:2297:1: ( '.' )
            // InternalRdsPP.g:2298:2: '.'
            {
             before(grammarAccess.getBSignalAccess().getFullStopKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBSignalAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__2__Impl"


    // $ANTLR start "rule__BSignal__Group__3"
    // InternalRdsPP.g:2307:1: rule__BSignal__Group__3 : rule__BSignal__Group__3__Impl rule__BSignal__Group__4 ;
    public final void rule__BSignal__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2311:1: ( rule__BSignal__Group__3__Impl rule__BSignal__Group__4 )
            // InternalRdsPP.g:2312:2: rule__BSignal__Group__3__Impl rule__BSignal__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__BSignal__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BSignal__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__3"


    // $ANTLR start "rule__BSignal__Group__3__Impl"
    // InternalRdsPP.g:2319:1: rule__BSignal__Group__3__Impl : ( ( rule__BSignal__KennzeichnungAssignment_3 ) ) ;
    public final void rule__BSignal__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2323:1: ( ( ( rule__BSignal__KennzeichnungAssignment_3 ) ) )
            // InternalRdsPP.g:2324:1: ( ( rule__BSignal__KennzeichnungAssignment_3 ) )
            {
            // InternalRdsPP.g:2324:1: ( ( rule__BSignal__KennzeichnungAssignment_3 ) )
            // InternalRdsPP.g:2325:2: ( rule__BSignal__KennzeichnungAssignment_3 )
            {
             before(grammarAccess.getBSignalAccess().getKennzeichnungAssignment_3()); 
            // InternalRdsPP.g:2326:2: ( rule__BSignal__KennzeichnungAssignment_3 )
            // InternalRdsPP.g:2326:3: rule__BSignal__KennzeichnungAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BSignal__KennzeichnungAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBSignalAccess().getKennzeichnungAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__3__Impl"


    // $ANTLR start "rule__BSignal__Group__4"
    // InternalRdsPP.g:2334:1: rule__BSignal__Group__4 : rule__BSignal__Group__4__Impl ;
    public final void rule__BSignal__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2338:1: ( rule__BSignal__Group__4__Impl )
            // InternalRdsPP.g:2339:2: rule__BSignal__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BSignal__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__4"


    // $ANTLR start "rule__BSignal__Group__4__Impl"
    // InternalRdsPP.g:2345:1: rule__BSignal__Group__4__Impl : ( ( RULE_S )? ) ;
    public final void rule__BSignal__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2349:1: ( ( ( RULE_S )? ) )
            // InternalRdsPP.g:2350:1: ( ( RULE_S )? )
            {
            // InternalRdsPP.g:2350:1: ( ( RULE_S )? )
            // InternalRdsPP.g:2351:2: ( RULE_S )?
            {
             before(grammarAccess.getBSignalAccess().getSTerminalRuleCall_4()); 
            // InternalRdsPP.g:2352:2: ( RULE_S )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==RULE_S) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalRdsPP.g:2352:3: RULE_S
                    {
                    match(input,RULE_S,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getBSignalAccess().getSTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__Group__4__Impl"


    // $ANTLR start "rule__Domainmodel__KennzeichnungAssignment"
    // InternalRdsPP.g:2361:1: rule__Domainmodel__KennzeichnungAssignment : ( ruleKennzeichnung ) ;
    public final void rule__Domainmodel__KennzeichnungAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2365:1: ( ( ruleKennzeichnung ) )
            // InternalRdsPP.g:2366:2: ( ruleKennzeichnung )
            {
            // InternalRdsPP.g:2366:2: ( ruleKennzeichnung )
            // InternalRdsPP.g:2367:3: ruleKennzeichnung
            {
             before(grammarAccess.getDomainmodelAccess().getKennzeichnungKennzeichnungParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleKennzeichnung();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getKennzeichnungKennzeichnungParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__KennzeichnungAssignment"


    // $ANTLR start "rule__Kennzeichnung__FunktionAssignment_0"
    // InternalRdsPP.g:2376:1: rule__Kennzeichnung__FunktionAssignment_0 : ( ruleBFunktion ) ;
    public final void rule__Kennzeichnung__FunktionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2380:1: ( ( ruleBFunktion ) )
            // InternalRdsPP.g:2381:2: ( ruleBFunktion )
            {
            // InternalRdsPP.g:2381:2: ( ruleBFunktion )
            // InternalRdsPP.g:2382:3: ruleBFunktion
            {
             before(grammarAccess.getKennzeichnungAccess().getFunktionBFunktionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleBFunktion();

            state._fsp--;

             after(grammarAccess.getKennzeichnungAccess().getFunktionBFunktionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__FunktionAssignment_0"


    // $ANTLR start "rule__Kennzeichnung__FunkZuordnungAssignment_1"
    // InternalRdsPP.g:2391:1: rule__Kennzeichnung__FunkZuordnungAssignment_1 : ( ruleBFunkZuordnung ) ;
    public final void rule__Kennzeichnung__FunkZuordnungAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2395:1: ( ( ruleBFunkZuordnung ) )
            // InternalRdsPP.g:2396:2: ( ruleBFunkZuordnung )
            {
            // InternalRdsPP.g:2396:2: ( ruleBFunkZuordnung )
            // InternalRdsPP.g:2397:3: ruleBFunkZuordnung
            {
             before(grammarAccess.getKennzeichnungAccess().getFunkZuordnungBFunkZuordnungParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBFunkZuordnung();

            state._fsp--;

             after(grammarAccess.getKennzeichnungAccess().getFunkZuordnungBFunkZuordnungParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__FunkZuordnungAssignment_1"


    // $ANTLR start "rule__Kennzeichnung__ProduktAssignment_2"
    // InternalRdsPP.g:2406:1: rule__Kennzeichnung__ProduktAssignment_2 : ( ruleBProdukt ) ;
    public final void rule__Kennzeichnung__ProduktAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2410:1: ( ( ruleBProdukt ) )
            // InternalRdsPP.g:2411:2: ( ruleBProdukt )
            {
            // InternalRdsPP.g:2411:2: ( ruleBProdukt )
            // InternalRdsPP.g:2412:3: ruleBProdukt
            {
             before(grammarAccess.getKennzeichnungAccess().getProduktBProduktParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleBProdukt();

            state._fsp--;

             after(grammarAccess.getKennzeichnungAccess().getProduktBProduktParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__ProduktAssignment_2"


    // $ANTLR start "rule__Kennzeichnung__EinbauortAssignment_3"
    // InternalRdsPP.g:2421:1: rule__Kennzeichnung__EinbauortAssignment_3 : ( ruleBEinbauort ) ;
    public final void rule__Kennzeichnung__EinbauortAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2425:1: ( ( ruleBEinbauort ) )
            // InternalRdsPP.g:2426:2: ( ruleBEinbauort )
            {
            // InternalRdsPP.g:2426:2: ( ruleBEinbauort )
            // InternalRdsPP.g:2427:3: ruleBEinbauort
            {
             before(grammarAccess.getKennzeichnungAccess().getEinbauortBEinbauortParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleBEinbauort();

            state._fsp--;

             after(grammarAccess.getKennzeichnungAccess().getEinbauortBEinbauortParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__EinbauortAssignment_3"


    // $ANTLR start "rule__Kennzeichnung__AufstellungsortAssignment_4"
    // InternalRdsPP.g:2436:1: rule__Kennzeichnung__AufstellungsortAssignment_4 : ( ruleBAufstellungsort ) ;
    public final void rule__Kennzeichnung__AufstellungsortAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2440:1: ( ( ruleBAufstellungsort ) )
            // InternalRdsPP.g:2441:2: ( ruleBAufstellungsort )
            {
            // InternalRdsPP.g:2441:2: ( ruleBAufstellungsort )
            // InternalRdsPP.g:2442:3: ruleBAufstellungsort
            {
             before(grammarAccess.getKennzeichnungAccess().getAufstellungsortBAufstellungsortParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleBAufstellungsort();

            state._fsp--;

             after(grammarAccess.getKennzeichnungAccess().getAufstellungsortBAufstellungsortParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kennzeichnung__AufstellungsortAssignment_4"


    // $ANTLR start "rule__BFunktion__HauptsystemAssignment_1"
    // InternalRdsPP.g:2451:1: rule__BFunktion__HauptsystemAssignment_1 : ( RULE_ANN ) ;
    public final void rule__BFunktion__HauptsystemAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2455:1: ( ( RULE_ANN ) )
            // InternalRdsPP.g:2456:2: ( RULE_ANN )
            {
            // InternalRdsPP.g:2456:2: ( RULE_ANN )
            // InternalRdsPP.g:2457:3: RULE_ANN
            {
             before(grammarAccess.getBFunktionAccess().getHauptsystemANNTerminalRuleCall_1_0()); 
            match(input,RULE_ANN,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getHauptsystemANNTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__HauptsystemAssignment_1"


    // $ANTLR start "rule__BFunktion__Abschnitt1Assignment_3"
    // InternalRdsPP.g:2466:1: rule__BFunktion__Abschnitt1Assignment_3 : ( RULE_AAANN ) ;
    public final void rule__BFunktion__Abschnitt1Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2470:1: ( ( RULE_AAANN ) )
            // InternalRdsPP.g:2471:2: ( RULE_AAANN )
            {
            // InternalRdsPP.g:2471:2: ( RULE_AAANN )
            // InternalRdsPP.g:2472:3: RULE_AAANN
            {
             before(grammarAccess.getBFunktionAccess().getAbschnitt1AAANNTerminalRuleCall_3_0()); 
            match(input,RULE_AAANN,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getAbschnitt1AAANNTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Abschnitt1Assignment_3"


    // $ANTLR start "rule__BFunktion__Abschnitt2Assignment_5"
    // InternalRdsPP.g:2481:1: rule__BFunktion__Abschnitt2Assignment_5 : ( RULE_AANNN ) ;
    public final void rule__BFunktion__Abschnitt2Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2485:1: ( ( RULE_AANNN ) )
            // InternalRdsPP.g:2486:2: ( RULE_AANNN )
            {
            // InternalRdsPP.g:2486:2: ( RULE_AANNN )
            // InternalRdsPP.g:2487:3: RULE_AANNN
            {
             before(grammarAccess.getBFunktionAccess().getAbschnitt2AANNNTerminalRuleCall_5_0()); 
            match(input,RULE_AANNN,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getAbschnitt2AANNNTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__Abschnitt2Assignment_5"


    // $ANTLR start "rule__BFunktion__LeitfunktionAssignment_6_1"
    // InternalRdsPP.g:2496:1: rule__BFunktion__LeitfunktionAssignment_6_1 : ( RULE_AANN ) ;
    public final void rule__BFunktion__LeitfunktionAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2500:1: ( ( RULE_AANN ) )
            // InternalRdsPP.g:2501:2: ( RULE_AANN )
            {
            // InternalRdsPP.g:2501:2: ( RULE_AANN )
            // InternalRdsPP.g:2502:3: RULE_AANN
            {
             before(grammarAccess.getBFunktionAccess().getLeitfunktionAANNTerminalRuleCall_6_1_0()); 
            match(input,RULE_AANN,FOLLOW_2); 
             after(grammarAccess.getBFunktionAccess().getLeitfunktionAANNTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__LeitfunktionAssignment_6_1"


    // $ANTLR start "rule__BFunktion__ProduktAssignment_7_0_1"
    // InternalRdsPP.g:2511:1: rule__BFunktion__ProduktAssignment_7_0_1 : ( ruleBProdukt ) ;
    public final void rule__BFunktion__ProduktAssignment_7_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2515:1: ( ( ruleBProdukt ) )
            // InternalRdsPP.g:2516:2: ( ruleBProdukt )
            {
            // InternalRdsPP.g:2516:2: ( ruleBProdukt )
            // InternalRdsPP.g:2517:3: ruleBProdukt
            {
             before(grammarAccess.getBFunktionAccess().getProduktBProduktParserRuleCall_7_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBProdukt();

            state._fsp--;

             after(grammarAccess.getBFunktionAccess().getProduktBProduktParserRuleCall_7_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__ProduktAssignment_7_0_1"


    // $ANTLR start "rule__BFunktion__AnschlussAssignment_7_0_2_0_1"
    // InternalRdsPP.g:2526:1: rule__BFunktion__AnschlussAssignment_7_0_2_0_1 : ( ruleBAnschluss ) ;
    public final void rule__BFunktion__AnschlussAssignment_7_0_2_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2530:1: ( ( ruleBAnschluss ) )
            // InternalRdsPP.g:2531:2: ( ruleBAnschluss )
            {
            // InternalRdsPP.g:2531:2: ( ruleBAnschluss )
            // InternalRdsPP.g:2532:3: ruleBAnschluss
            {
             before(grammarAccess.getBFunktionAccess().getAnschlussBAnschlussParserRuleCall_7_0_2_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBAnschluss();

            state._fsp--;

             after(grammarAccess.getBFunktionAccess().getAnschlussBAnschlussParserRuleCall_7_0_2_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__AnschlussAssignment_7_0_2_0_1"


    // $ANTLR start "rule__BFunktion__DokumentationAssignment_7_0_2_1_1"
    // InternalRdsPP.g:2541:1: rule__BFunktion__DokumentationAssignment_7_0_2_1_1 : ( ruleBDoku ) ;
    public final void rule__BFunktion__DokumentationAssignment_7_0_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2545:1: ( ( ruleBDoku ) )
            // InternalRdsPP.g:2546:2: ( ruleBDoku )
            {
            // InternalRdsPP.g:2546:2: ( ruleBDoku )
            // InternalRdsPP.g:2547:3: ruleBDoku
            {
             before(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_0_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_0_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__DokumentationAssignment_7_0_2_1_1"


    // $ANTLR start "rule__BFunktion__SignalAssignment_7_1_1"
    // InternalRdsPP.g:2556:1: rule__BFunktion__SignalAssignment_7_1_1 : ( ruleBSignal ) ;
    public final void rule__BFunktion__SignalAssignment_7_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2560:1: ( ( ruleBSignal ) )
            // InternalRdsPP.g:2561:2: ( ruleBSignal )
            {
            // InternalRdsPP.g:2561:2: ( ruleBSignal )
            // InternalRdsPP.g:2562:3: ruleBSignal
            {
             before(grammarAccess.getBFunktionAccess().getSignalBSignalParserRuleCall_7_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBSignal();

            state._fsp--;

             after(grammarAccess.getBFunktionAccess().getSignalBSignalParserRuleCall_7_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__SignalAssignment_7_1_1"


    // $ANTLR start "rule__BFunktion__DokumentationAssignment_7_2_1"
    // InternalRdsPP.g:2571:1: rule__BFunktion__DokumentationAssignment_7_2_1 : ( ruleBDoku ) ;
    public final void rule__BFunktion__DokumentationAssignment_7_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2575:1: ( ( ruleBDoku ) )
            // InternalRdsPP.g:2576:2: ( ruleBDoku )
            {
            // InternalRdsPP.g:2576:2: ( ruleBDoku )
            // InternalRdsPP.g:2577:3: ruleBDoku
            {
             before(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBFunktionAccess().getDokumentationBDokuParserRuleCall_7_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunktion__DokumentationAssignment_7_2_1"


    // $ANTLR start "rule__BFunkZuordnung__FunktionsbereicheAssignment_1"
    // InternalRdsPP.g:2586:1: rule__BFunkZuordnung__FunktionsbereicheAssignment_1 : ( RULE_ANN ) ;
    public final void rule__BFunkZuordnung__FunktionsbereicheAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2590:1: ( ( RULE_ANN ) )
            // InternalRdsPP.g:2591:2: ( RULE_ANN )
            {
            // InternalRdsPP.g:2591:2: ( RULE_ANN )
            // InternalRdsPP.g:2592:3: RULE_ANN
            {
             before(grammarAccess.getBFunkZuordnungAccess().getFunktionsbereicheANNTerminalRuleCall_1_0()); 
            match(input,RULE_ANN,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getFunktionsbereicheANNTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__FunktionsbereicheAssignment_1"


    // $ANTLR start "rule__BFunkZuordnung__Abschnitt1Assignment_3"
    // InternalRdsPP.g:2601:1: rule__BFunkZuordnung__Abschnitt1Assignment_3 : ( RULE_AAANN ) ;
    public final void rule__BFunkZuordnung__Abschnitt1Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2605:1: ( ( RULE_AAANN ) )
            // InternalRdsPP.g:2606:2: ( RULE_AAANN )
            {
            // InternalRdsPP.g:2606:2: ( RULE_AAANN )
            // InternalRdsPP.g:2607:3: RULE_AAANN
            {
             before(grammarAccess.getBFunkZuordnungAccess().getAbschnitt1AAANNTerminalRuleCall_3_0()); 
            match(input,RULE_AAANN,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getAbschnitt1AAANNTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__Abschnitt1Assignment_3"


    // $ANTLR start "rule__BFunkZuordnung__LeitfunktionAssignment_5"
    // InternalRdsPP.g:2616:1: rule__BFunkZuordnung__LeitfunktionAssignment_5 : ( RULE_AANN ) ;
    public final void rule__BFunkZuordnung__LeitfunktionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2620:1: ( ( RULE_AANN ) )
            // InternalRdsPP.g:2621:2: ( RULE_AANN )
            {
            // InternalRdsPP.g:2621:2: ( RULE_AANN )
            // InternalRdsPP.g:2622:3: RULE_AANN
            {
             before(grammarAccess.getBFunkZuordnungAccess().getLeitfunktionAANNTerminalRuleCall_5_0()); 
            match(input,RULE_AANN,FOLLOW_2); 
             after(grammarAccess.getBFunkZuordnungAccess().getLeitfunktionAANNTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__LeitfunktionAssignment_5"


    // $ANTLR start "rule__BFunkZuordnung__SignalAssignment_6_0_1"
    // InternalRdsPP.g:2631:1: rule__BFunkZuordnung__SignalAssignment_6_0_1 : ( ruleBSignal ) ;
    public final void rule__BFunkZuordnung__SignalAssignment_6_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2635:1: ( ( ruleBSignal ) )
            // InternalRdsPP.g:2636:2: ( ruleBSignal )
            {
            // InternalRdsPP.g:2636:2: ( ruleBSignal )
            // InternalRdsPP.g:2637:3: ruleBSignal
            {
             before(grammarAccess.getBFunkZuordnungAccess().getSignalBSignalParserRuleCall_6_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBSignal();

            state._fsp--;

             after(grammarAccess.getBFunkZuordnungAccess().getSignalBSignalParserRuleCall_6_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__SignalAssignment_6_0_1"


    // $ANTLR start "rule__BFunkZuordnung__DokumentationAssignment_6_1_1"
    // InternalRdsPP.g:2646:1: rule__BFunkZuordnung__DokumentationAssignment_6_1_1 : ( ruleBDoku ) ;
    public final void rule__BFunkZuordnung__DokumentationAssignment_6_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2650:1: ( ( ruleBDoku ) )
            // InternalRdsPP.g:2651:2: ( ruleBDoku )
            {
            // InternalRdsPP.g:2651:2: ( ruleBDoku )
            // InternalRdsPP.g:2652:3: ruleBDoku
            {
             before(grammarAccess.getBFunkZuordnungAccess().getDokumentationBDokuParserRuleCall_6_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBFunkZuordnungAccess().getDokumentationBDokuParserRuleCall_6_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BFunkZuordnung__DokumentationAssignment_6_1_1"


    // $ANTLR start "rule__BEinbauort__HauptsystemAssignment_1"
    // InternalRdsPP.g:2661:1: rule__BEinbauort__HauptsystemAssignment_1 : ( RULE_ANN ) ;
    public final void rule__BEinbauort__HauptsystemAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2665:1: ( ( RULE_ANN ) )
            // InternalRdsPP.g:2666:2: ( RULE_ANN )
            {
            // InternalRdsPP.g:2666:2: ( RULE_ANN )
            // InternalRdsPP.g:2667:3: RULE_ANN
            {
             before(grammarAccess.getBEinbauortAccess().getHauptsystemANNTerminalRuleCall_1_0()); 
            match(input,RULE_ANN,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getHauptsystemANNTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__HauptsystemAssignment_1"


    // $ANTLR start "rule__BEinbauort__EinbaueinheitAssignment_3"
    // InternalRdsPP.g:2676:1: rule__BEinbauort__EinbaueinheitAssignment_3 : ( RULE_AAANN ) ;
    public final void rule__BEinbauort__EinbaueinheitAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2680:1: ( ( RULE_AAANN ) )
            // InternalRdsPP.g:2681:2: ( RULE_AAANN )
            {
            // InternalRdsPP.g:2681:2: ( RULE_AAANN )
            // InternalRdsPP.g:2682:3: RULE_AAANN
            {
             before(grammarAccess.getBEinbauortAccess().getEinbaueinheitAAANNTerminalRuleCall_3_0()); 
            match(input,RULE_AAANN,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getEinbaueinheitAAANNTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__EinbaueinheitAssignment_3"


    // $ANTLR start "rule__BEinbauort__KlassifizierungAssignment_4_1"
    // InternalRdsPP.g:2691:1: rule__BEinbauort__KlassifizierungAssignment_4_1 : ( RULE_AANNN ) ;
    public final void rule__BEinbauort__KlassifizierungAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2695:1: ( ( RULE_AANNN ) )
            // InternalRdsPP.g:2696:2: ( RULE_AANNN )
            {
            // InternalRdsPP.g:2696:2: ( RULE_AANNN )
            // InternalRdsPP.g:2697:3: RULE_AANNN
            {
             before(grammarAccess.getBEinbauortAccess().getKlassifizierungAANNNTerminalRuleCall_4_1_0()); 
            match(input,RULE_AANNN,FOLLOW_2); 
             after(grammarAccess.getBEinbauortAccess().getKlassifizierungAANNNTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__KlassifizierungAssignment_4_1"


    // $ANTLR start "rule__BEinbauort__EinbauplatzAssignment_6"
    // InternalRdsPP.g:2706:1: rule__BEinbauort__EinbauplatzAssignment_6 : ( ruleNA ) ;
    public final void rule__BEinbauort__EinbauplatzAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2710:1: ( ( ruleNA ) )
            // InternalRdsPP.g:2711:2: ( ruleNA )
            {
            // InternalRdsPP.g:2711:2: ( ruleNA )
            // InternalRdsPP.g:2712:3: ruleNA
            {
             before(grammarAccess.getBEinbauortAccess().getEinbauplatzNAParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleNA();

            state._fsp--;

             after(grammarAccess.getBEinbauortAccess().getEinbauplatzNAParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__EinbauplatzAssignment_6"


    // $ANTLR start "rule__BEinbauort__DokumentationAssignment_7_1"
    // InternalRdsPP.g:2721:1: rule__BEinbauort__DokumentationAssignment_7_1 : ( ruleBDoku ) ;
    public final void rule__BEinbauort__DokumentationAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2725:1: ( ( ruleBDoku ) )
            // InternalRdsPP.g:2726:2: ( ruleBDoku )
            {
            // InternalRdsPP.g:2726:2: ( ruleBDoku )
            // InternalRdsPP.g:2727:3: ruleBDoku
            {
             before(grammarAccess.getBEinbauortAccess().getDokumentationBDokuParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBEinbauortAccess().getDokumentationBDokuParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BEinbauort__DokumentationAssignment_7_1"


    // $ANTLR start "rule__BAufstellungsort__HauptsystemAssignment_1"
    // InternalRdsPP.g:2736:1: rule__BAufstellungsort__HauptsystemAssignment_1 : ( RULE_ANN ) ;
    public final void rule__BAufstellungsort__HauptsystemAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2740:1: ( ( RULE_ANN ) )
            // InternalRdsPP.g:2741:2: ( RULE_ANN )
            {
            // InternalRdsPP.g:2741:2: ( RULE_ANN )
            // InternalRdsPP.g:2742:3: RULE_ANN
            {
             before(grammarAccess.getBAufstellungsortAccess().getHauptsystemANNTerminalRuleCall_1_0()); 
            match(input,RULE_ANN,FOLLOW_2); 
             after(grammarAccess.getBAufstellungsortAccess().getHauptsystemANNTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__HauptsystemAssignment_1"


    // $ANTLR start "rule__BAufstellungsort__OrtAssignment_3"
    // InternalRdsPP.g:2751:1: rule__BAufstellungsort__OrtAssignment_3 : ( RULE_AAANN ) ;
    public final void rule__BAufstellungsort__OrtAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2755:1: ( ( RULE_AAANN ) )
            // InternalRdsPP.g:2756:2: ( RULE_AAANN )
            {
            // InternalRdsPP.g:2756:2: ( RULE_AAANN )
            // InternalRdsPP.g:2757:3: RULE_AAANN
            {
             before(grammarAccess.getBAufstellungsortAccess().getOrtAAANNTerminalRuleCall_3_0()); 
            match(input,RULE_AAANN,FOLLOW_2); 
             after(grammarAccess.getBAufstellungsortAccess().getOrtAAANNTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__OrtAssignment_3"


    // $ANTLR start "rule__BAufstellungsort__BereichAssignment_5"
    // InternalRdsPP.g:2766:1: rule__BAufstellungsort__BereichAssignment_5 : ( ruleNA ) ;
    public final void rule__BAufstellungsort__BereichAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2770:1: ( ( ruleNA ) )
            // InternalRdsPP.g:2771:2: ( ruleNA )
            {
            // InternalRdsPP.g:2771:2: ( ruleNA )
            // InternalRdsPP.g:2772:3: ruleNA
            {
             before(grammarAccess.getBAufstellungsortAccess().getBereichNAParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleNA();

            state._fsp--;

             after(grammarAccess.getBAufstellungsortAccess().getBereichNAParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__BereichAssignment_5"


    // $ANTLR start "rule__BAufstellungsort__DokumentationAssignment_6_1"
    // InternalRdsPP.g:2781:1: rule__BAufstellungsort__DokumentationAssignment_6_1 : ( ruleBDoku ) ;
    public final void rule__BAufstellungsort__DokumentationAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2785:1: ( ( ruleBDoku ) )
            // InternalRdsPP.g:2786:2: ( ruleBDoku )
            {
            // InternalRdsPP.g:2786:2: ( ruleBDoku )
            // InternalRdsPP.g:2787:3: ruleBDoku
            {
             before(grammarAccess.getBAufstellungsortAccess().getDokumentationBDokuParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBDoku();

            state._fsp--;

             after(grammarAccess.getBAufstellungsortAccess().getDokumentationBDokuParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAufstellungsort__DokumentationAssignment_6_1"


    // $ANTLR start "rule__BProdukt__ProduktAssignment_1"
    // InternalRdsPP.g:2796:1: rule__BProdukt__ProduktAssignment_1 : ( ( rule__BProdukt__ProduktAlternatives_1_0 ) ) ;
    public final void rule__BProdukt__ProduktAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2800:1: ( ( ( rule__BProdukt__ProduktAlternatives_1_0 ) ) )
            // InternalRdsPP.g:2801:2: ( ( rule__BProdukt__ProduktAlternatives_1_0 ) )
            {
            // InternalRdsPP.g:2801:2: ( ( rule__BProdukt__ProduktAlternatives_1_0 ) )
            // InternalRdsPP.g:2802:3: ( rule__BProdukt__ProduktAlternatives_1_0 )
            {
             before(grammarAccess.getBProduktAccess().getProduktAlternatives_1_0()); 
            // InternalRdsPP.g:2803:3: ( rule__BProdukt__ProduktAlternatives_1_0 )
            // InternalRdsPP.g:2803:4: rule__BProdukt__ProduktAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__BProdukt__ProduktAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getBProduktAccess().getProduktAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BProdukt__ProduktAssignment_1"


    // $ANTLR start "rule__BAnschluss__Kennzeichnung1Assignment_1"
    // InternalRdsPP.g:2811:1: rule__BAnschluss__Kennzeichnung1Assignment_1 : ( RULE_STRING ) ;
    public final void rule__BAnschluss__Kennzeichnung1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2815:1: ( ( RULE_STRING ) )
            // InternalRdsPP.g:2816:2: ( RULE_STRING )
            {
            // InternalRdsPP.g:2816:2: ( RULE_STRING )
            // InternalRdsPP.g:2817:3: RULE_STRING
            {
             before(grammarAccess.getBAnschlussAccess().getKennzeichnung1STRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getBAnschlussAccess().getKennzeichnung1STRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Kennzeichnung1Assignment_1"


    // $ANTLR start "rule__BAnschluss__Kennzeichnung2Assignment_2_1"
    // InternalRdsPP.g:2826:1: rule__BAnschluss__Kennzeichnung2Assignment_2_1 : ( RULE_STRING ) ;
    public final void rule__BAnschluss__Kennzeichnung2Assignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2830:1: ( ( RULE_STRING ) )
            // InternalRdsPP.g:2831:2: ( RULE_STRING )
            {
            // InternalRdsPP.g:2831:2: ( RULE_STRING )
            // InternalRdsPP.g:2832:3: RULE_STRING
            {
             before(grammarAccess.getBAnschlussAccess().getKennzeichnung2STRINGTerminalRuleCall_2_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getBAnschlussAccess().getKennzeichnung2STRINGTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BAnschluss__Kennzeichnung2Assignment_2_1"


    // $ANTLR start "rule__BDoku__DokuAssignment_1"
    // InternalRdsPP.g:2841:1: rule__BDoku__DokuAssignment_1 : ( RULE_AAANNN ) ;
    public final void rule__BDoku__DokuAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2845:1: ( ( RULE_AAANNN ) )
            // InternalRdsPP.g:2846:2: ( RULE_AAANNN )
            {
            // InternalRdsPP.g:2846:2: ( RULE_AAANNN )
            // InternalRdsPP.g:2847:3: RULE_AAANNN
            {
             before(grammarAccess.getBDokuAccess().getDokuAAANNNTerminalRuleCall_1_0()); 
            match(input,RULE_AAANNN,FOLLOW_2); 
             after(grammarAccess.getBDokuAccess().getDokuAAANNNTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BDoku__DokuAssignment_1"


    // $ANTLR start "rule__BSignal__KlassifizierungAssignment_1"
    // InternalRdsPP.g:2856:1: rule__BSignal__KlassifizierungAssignment_1 : ( ( rule__BSignal__KlassifizierungAlternatives_1_0 ) ) ;
    public final void rule__BSignal__KlassifizierungAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2860:1: ( ( ( rule__BSignal__KlassifizierungAlternatives_1_0 ) ) )
            // InternalRdsPP.g:2861:2: ( ( rule__BSignal__KlassifizierungAlternatives_1_0 ) )
            {
            // InternalRdsPP.g:2861:2: ( ( rule__BSignal__KlassifizierungAlternatives_1_0 ) )
            // InternalRdsPP.g:2862:3: ( rule__BSignal__KlassifizierungAlternatives_1_0 )
            {
             before(grammarAccess.getBSignalAccess().getKlassifizierungAlternatives_1_0()); 
            // InternalRdsPP.g:2863:3: ( rule__BSignal__KlassifizierungAlternatives_1_0 )
            // InternalRdsPP.g:2863:4: rule__BSignal__KlassifizierungAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__BSignal__KlassifizierungAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getBSignalAccess().getKlassifizierungAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__KlassifizierungAssignment_1"


    // $ANTLR start "rule__BSignal__KennzeichnungAssignment_3"
    // InternalRdsPP.g:2871:1: rule__BSignal__KennzeichnungAssignment_3 : ( ruleNA ) ;
    public final void rule__BSignal__KennzeichnungAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRdsPP.g:2875:1: ( ( ruleNA ) )
            // InternalRdsPP.g:2876:2: ( ruleNA )
            {
            // InternalRdsPP.g:2876:2: ( ruleNA )
            // InternalRdsPP.g:2877:3: ruleNA
            {
             before(grammarAccess.getBSignalAccess().getKennzeichnungNAParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleNA();

            state._fsp--;

             after(grammarAccess.getBSignalAccess().getKennzeichnungNAParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BSignal__KennzeichnungAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000500L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000100100L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000000000C0L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000001000L});

}