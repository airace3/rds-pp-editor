/*
 * generated by Xtext 2.15.0
 */
package org.xtext.rdsserver.ui;

import com.google.inject.Injector;
import org.eclipse.core.runtime.Platform;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.xtext.rdsserver.ui.internal.RdsserverActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class RdsPPExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return Platform.getBundle(RdsserverActivator.PLUGIN_ID);
	}
	
	@Override
	protected Injector getInjector() {
		RdsserverActivator activator = RdsserverActivator.getInstance();
		return activator != null ? activator.getInjector(RdsserverActivator.ORG_XTEXT_RDSSERVER_RDSPP) : null;
	}

}
